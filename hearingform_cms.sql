/*
SQLyog Ultimate v11.33 (64 bit)
MySQL - 10.1.13-MariaDB : Database - hearingform_cms
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`china_dating` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `china_dating`;

/*Table structure for table `activations` */

DROP TABLE IF EXISTS `activations`;

CREATE TABLE `activations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `completed` tinyint(1) NOT NULL DEFAULT '0',
  `completed_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `activations` */

insert  into `activations`(`id`,`user_id`,`code`,`completed`,`completed_at`,`created_at`,`updated_at`) values (1,1,'DPpnA4ZQN6DvFNzKfO2QALMZhRrlVRV7',1,'2016-12-15 09:18:50','2016-12-15 09:18:50','2016-12-15 09:18:50'),(2,2,'USMbOXMSwZTmI9ThPPQCNgENalrGzpoZ',1,'2016-12-15 13:36:22','2016-12-15 13:36:22','2016-12-15 13:36:22'),(3,3,'ywW8Ew23f5Y1c8m9Qdv1vZ58SMCisldX',1,'2016-12-15 14:09:49','2016-12-15 14:09:49','2016-12-15 14:09:49'),(4,4,'y42jzR0f7Mjx4UgBQ0JlXKnys3SMrlhj',1,'2016-12-15 14:11:28','2016-12-15 14:11:28','2016-12-15 14:11:28');

/*Table structure for table `client_abouts` */

DROP TABLE IF EXISTS `client_abouts`;

CREATE TABLE `client_abouts` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `uid` int(10) NOT NULL DEFAULT '0',
  `item1` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `item2` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `item3` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `item4` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `item5` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `item6` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `item7` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `item8` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `item9` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `item10` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `item11` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `item12` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `item13` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `itemfile` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `client_abouts` */

insert  into `client_abouts`(`id`,`uid`,`item1`,`item2`,`item3`,`item4`,`item5`,`item6`,`item7`,`item8`,`item9`,`item10`,`item11`,`item12`,`item13`,`itemfile`,`created_at`,`updated_at`) values (1,0,'','','','','','','','','','sdfsdf','','','','','2016-08-31 07:01:12','2016-12-13 00:25:17'),(2,2,'aa','','','','','','','','','','','','','','2016-12-16 00:05:55','2016-12-16 01:07:04');

/*Table structure for table `client_homes` */

DROP TABLE IF EXISTS `client_homes`;

CREATE TABLE `client_homes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `uid` int(10) NOT NULL DEFAULT '0',
  `item1` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `item2` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `item3` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `item4` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `item5` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `item6` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `item7` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `item8` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `item9` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `item10` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `item11` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `item12` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `item13` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `itemfile4` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `itemfile1` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `itemfile2` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `itemfile3` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `client_homes` */

insert  into `client_homes`(`id`,`uid`,`item1`,`item2`,`item3`,`item4`,`item5`,`item6`,`item7`,`item8`,`item9`,`item10`,`item11`,`item12`,`item13`,`itemfile4`,`itemfile1`,`itemfile2`,`itemfile3`,`created_at`,`updated_at`) values (20,0,'#ff0000','aaaaa','dddddd','sdsdf','','ttt','tttt','ttttt','aaaaaaaa','ssssss','asdasd','dfgdfgdfgdfg','','22.jpg','22.jpg','','','2016-08-30 23:35:03','2016-12-16 00:49:48'),(21,2,'#ffcc99','aaaaaa','','','','','','','','','','','','','','','','2016-12-15 23:59:37','2016-12-16 01:05:45');

/*Table structure for table `client_option_items` */

DROP TABLE IF EXISTS `client_option_items`;

CREATE TABLE `client_option_items` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `uid` int(10) NOT NULL,
  `item1` int(3) NOT NULL DEFAULT '0',
  `item2` int(3) NOT NULL DEFAULT '0',
  `item3` int(3) NOT NULL DEFAULT '0',
  `item4` int(3) NOT NULL DEFAULT '0',
  `item5` int(3) NOT NULL DEFAULT '0',
  `item6` int(3) NOT NULL DEFAULT '0',
  `item7` int(3) NOT NULL DEFAULT '0',
  `item8` int(3) NOT NULL DEFAULT '0',
  `item9` int(3) NOT NULL DEFAULT '0',
  `item10` int(3) NOT NULL DEFAULT '0',
  `item11` int(3) NOT NULL DEFAULT '0',
  `item12` int(3) NOT NULL DEFAULT '0',
  `item13` int(3) NOT NULL DEFAULT '0',
  `item14` int(3) NOT NULL DEFAULT '0',
  `item15` int(3) NOT NULL DEFAULT '0',
  `item16` int(3) NOT NULL DEFAULT '0',
  `item_1` int(3) NOT NULL DEFAULT '0',
  `item_2` int(3) NOT NULL DEFAULT '0',
  `item_3` int(3) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `client_option_items` */

insert  into `client_option_items`(`id`,`uid`,`item1`,`item2`,`item3`,`item4`,`item5`,`item6`,`item7`,`item8`,`item9`,`item10`,`item11`,`item12`,`item13`,`item14`,`item15`,`item16`,`item_1`,`item_2`,`item_3`,`created_at`,`updated_at`) values (9,0,1,1,1,0,0,0,0,0,1,1,1,1,1,0,0,0,1,0,0,'2016-08-31 06:31:38','2016-12-12 23:03:35'),(15,2,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,'2016-12-16 00:08:24','2016-12-16 01:08:38');

/*Table structure for table `client_services_1` */

DROP TABLE IF EXISTS `client_services_1`;

CREATE TABLE `client_services_1` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `uid` int(10) NOT NULL DEFAULT '0',
  `item1` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `item2` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `item3` text COLLATE utf8_unicode_ci NOT NULL,
  `item4` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `item5` text COLLATE utf8_unicode_ci NOT NULL,
  `item6` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `item7` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `item8` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `item9` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `item10` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `item11` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `item12` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `item13` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `itemfile1` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `itemfile2` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `itemfile3` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `client_services_1` */

insert  into `client_services_1`(`id`,`uid`,`item1`,`item2`,`item3`,`item4`,`item5`,`item6`,`item7`,`item8`,`item9`,`item10`,`item11`,`item12`,`item13`,`itemfile1`,`itemfile2`,`itemfile3`,`created_at`,`updated_at`) values (1,0,'','','','','','','','','','','','','','','','aaa.jpg','2016-09-01 22:28:27','2016-12-15 18:09:02'),(2,2,'','','','','','','','','','','','','','','','','2016-12-16 00:07:10','0000-00-00 00:00:00');

/*Table structure for table `client_services_2` */

DROP TABLE IF EXISTS `client_services_2`;

CREATE TABLE `client_services_2` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `uid` int(10) NOT NULL DEFAULT '0',
  `item1` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `item2` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `item3` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `item4` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `item5` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `item6` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `item7` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `item8` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `item9` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `item10` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `item11` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `item12` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `item13` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `itemfile` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `client_services_2` */

insert  into `client_services_2`(`id`,`uid`,`item1`,`item2`,`item3`,`item4`,`item5`,`item6`,`item7`,`item8`,`item9`,`item10`,`item11`,`item12`,`item13`,`itemfile`,`created_at`,`updated_at`) values (1,0,'bbbbssssaa','','','','','','','','','','','','','nga_icon_clouds_png_2_2.jpeg,qq.jpg','2016-08-31 18:29:42','2016-12-12 23:38:44'),(2,2,'','','','','','','','','','','','','','','2016-12-16 00:07:53','0000-00-00 00:00:00');

/*Table structure for table `datatables` */

DROP TABLE IF EXISTS `datatables`;

CREATE TABLE `datatables` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fullname` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `points` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `notes` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `datatables` */

insert  into `datatables`(`id`,`username`,`fullname`,`email`,`points`,`notes`,`created_at`,`updated_at`) values (1,'Jayden','Cormier','satterfield.christine@yahoo.com','185','Laborum fugiat ex qui doloribus. Soluta libero vel facere. Ea assumenda nisi in voluptatem.','2016-12-15 09:18:50','2016-12-15 09:18:50'),(2,'Vada','Schmitt','stroman.elliott@hotmail.com','150','Vitae consectetur dolorum nam. Occaecati veniam inventore et.','2016-12-15 09:18:50','2016-12-15 09:18:50'),(3,'Clara','Mante','lfisher@gmail.com','963','Corrupti enim et itaque tenetur repellendus aut. In rerum qui quasi perferendis nemo doloribus.','2016-12-15 09:18:50','2016-12-15 09:18:50'),(4,'Grover','Ortiz','queenie63@hotmail.com','264','Iure nesciunt ratione molestiae est et quia. Commodi sed maxime iure quaerat.','2016-12-15 09:18:50','2016-12-15 09:18:50'),(5,'Alize','Murazik','zgreenfelder@gmail.com','869','Porro quia deleniti rerum natus doloribus. Ipsam ut voluptates dolorem quis sit repellat.','2016-12-15 09:18:50','2016-12-15 09:18:50'),(6,'Brigitte','Bernhard','mbrakus@oconnell.com','925','Repellat quibusdam blanditiis non quae. Ipsum sed deleniti ad maxime. Ut unde illum cum.','2016-12-15 09:18:50','2016-12-15 09:18:50'),(7,'Bennie','Lebsack','erna.waelchi@konopelski.com','832','Amet doloremque molestias qui repellendus quos. Eos assumenda voluptates adipisci.','2016-12-15 09:18:50','2016-12-15 09:18:50'),(8,'Glenda','Kuvalis','cmccullough@monahan.com','459','Eveniet impedit iusto impedit maiores. Ut eveniet et accusamus aperiam est aliquid.','2016-12-15 09:18:50','2016-12-15 09:18:50'),(9,'Pauline','Prosacco','jennyfer03@lesch.net','74','Autem at ut consectetur voluptatibus est fugiat minus. Voluptates fugit dicta non quidem sint sint.','2016-12-15 09:18:50','2016-12-15 09:18:50'),(10,'Marjory','Roberts','haag.ethan@ondricka.com','571','Velit sint exercitationem animi minima id. Sit ut voluptas beatae sed sequi corporis.','2016-12-15 09:18:50','2016-12-15 09:18:50');

/*Table structure for table `migrations` */

DROP TABLE IF EXISTS `migrations`;

CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `migrations` */

insert  into `migrations`(`migration`,`batch`) values ('2014_07_02_230147_migration_cartalyst_sentinel',1),('2014_10_04_174350_soft_delete_users',1),('2014_12_10_011106_add_fields_to_user_table',1),('2015_08_09_200015_create_blog_module_table',1),('2015_08_11_064636_add_slug_to_blogs_table',1),('2015_08_19_073929_create_taggable_table',1),('2015_11_10_140011_create_files_table',1),('2016_01_02_062647_create_tasks_table',1),('2016_04_26_054601_create_datatables_table',1);

/*Table structure for table `persistences` */

DROP TABLE IF EXISTS `persistences`;

CREATE TABLE `persistences` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `persistences_code_unique` (`code`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `persistences` */

insert  into `persistences`(`id`,`user_id`,`code`,`created_at`,`updated_at`) values (13,2,'ygYWSAXSWeCReQfedO0nrcTNeMfcYbB0','2016-12-15 13:50:09','2016-12-15 13:50:09'),(14,2,'TNOYIdJh4FZcbpOm8jbGveCM2QoFecqL','2016-12-15 13:50:54','2016-12-15 13:50:54'),(15,2,'RAJnPlWhsNDwo4fDqPcVrFdp6LhSq7Vo','2016-12-15 13:51:08','2016-12-15 13:51:08'),(22,1,'EjapJbbjawMbDJ5gvfmoBvKrRRjDzjUn','2016-12-15 16:27:19','2016-12-15 16:27:19');

/*Table structure for table `reminders` */

DROP TABLE IF EXISTS `reminders`;

CREATE TABLE `reminders` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `completed` tinyint(1) NOT NULL DEFAULT '0',
  `completed_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `reminders` */

insert  into `reminders`(`id`,`user_id`,`code`,`completed`,`completed_at`,`created_at`,`updated_at`) values (1,2,'PjinGURR2l5oIQh97pNsiZAYpUwls95X',0,NULL,'2016-12-15 13:37:06','2016-12-15 13:37:06');

/*Table structure for table `role_users` */

DROP TABLE IF EXISTS `role_users`;

CREATE TABLE `role_users` (
  `user_id` int(10) unsigned NOT NULL,
  `role_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`user_id`,`role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `role_users` */

insert  into `role_users`(`user_id`,`role_id`,`created_at`,`updated_at`) values (1,1,'2016-12-15 09:18:50','2016-12-15 09:18:50'),(2,2,'2016-12-15 13:36:22','2016-12-15 13:36:22'),(3,2,'2016-12-15 14:09:49','2016-12-15 14:09:49'),(4,2,'2016-12-15 14:11:28','2016-12-15 14:11:28');

/*Table structure for table `roles` */

DROP TABLE IF EXISTS `roles`;

CREATE TABLE `roles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `permissions` text COLLATE utf8_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `roles_slug_unique` (`slug`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `roles` */

insert  into `roles`(`id`,`slug`,`name`,`permissions`,`created_at`,`updated_at`) values (1,'admin','Admin','{\"admin\":1}','2016-12-15 09:18:50','2016-12-15 09:18:50'),(2,'user','User',NULL,'2016-12-15 09:18:50','2016-12-15 09:18:50');

/*Table structure for table `tasks` */

DROP TABLE IF EXISTS `tasks`;

CREATE TABLE `tasks` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `finished` tinyint(4) NOT NULL DEFAULT '0',
  `task_description` text COLLATE utf8_unicode_ci NOT NULL,
  `task_deadline` date NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `tasks` */

/*Table structure for table `throttle` */

DROP TABLE IF EXISTS `throttle`;

CREATE TABLE `throttle` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned DEFAULT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ip` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `throttle_user_id_index` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `throttle` */

insert  into `throttle`(`id`,`user_id`,`type`,`ip`,`created_at`,`updated_at`) values (1,NULL,'global',NULL,'2016-12-15 10:20:12','2016-12-15 10:20:12'),(2,NULL,'ip','192.168.1.252','2016-12-15 10:20:12','2016-12-15 10:20:12'),(3,1,'user',NULL,'2016-12-15 10:20:12','2016-12-15 10:20:12'),(4,NULL,'global',NULL,'2016-12-15 10:20:16','2016-12-15 10:20:16'),(5,NULL,'ip','192.168.1.252','2016-12-15 10:20:16','2016-12-15 10:20:16'),(6,1,'user',NULL,'2016-12-15 10:20:16','2016-12-15 10:20:16'),(7,NULL,'global',NULL,'2016-12-15 10:21:37','2016-12-15 10:21:37'),(8,NULL,'ip','192.168.1.252','2016-12-15 10:21:37','2016-12-15 10:21:37'),(9,1,'user',NULL,'2016-12-15 10:21:37','2016-12-15 10:21:37'),(10,NULL,'global',NULL,'2016-12-15 13:39:14','2016-12-15 13:39:14'),(11,NULL,'ip','192.168.1.252','2016-12-15 13:39:14','2016-12-15 13:39:14');

/*Table structure for table `users` */

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `permissions` text COLLATE utf8_unicode_ci,
  `last_login` timestamp NULL DEFAULT NULL,
  `first_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `company_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `bio` text COLLATE utf8_unicode_ci,
  `gender` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `dob` date DEFAULT NULL,
  `pic` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `country` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `state` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `city` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `postal` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `users` */

insert  into `users`(`id`,`email`,`password`,`permissions`,`last_login`,`first_name`,`last_name`,`company_name`,`created_at`,`updated_at`,`deleted_at`,`bio`,`gender`,`dob`,`pic`,`country`,`state`,`city`,`address`,`postal`) values (1,'admin@admin.com','$2y$10$EmetDrC2rdsdVoVPmm/AYOn8z.Kit/bdcNUvobJQQT59S5fEXoPna',NULL,'2016-12-15 16:27:19','Admin','',NULL,'2016-12-15 09:18:50','2016-12-15 16:27:19',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(2,'user1@gmail.com','$2y$10$xBYdBHRPRnbk4nCg7OzHdemwUa5Mfazl8HwevcDq4513Y8IqNyx4y',NULL,'2016-12-15 15:48:48','user1','user',NULL,'2016-12-15 13:36:22','2016-12-15 15:48:48',NULL,NULL,'female',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(3,'user2@gmail.com','$2y$10$AWlbu1nR3CfVBvgAqA3VFu3i5SpdI9q40ktd6vGrpe.40vi9HDXua',NULL,'2016-12-15 14:09:49','user2','user',NULL,'2016-12-15 14:09:49','2016-12-15 14:09:49',NULL,NULL,'female',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(4,'user3@gmail.com','$2y$10$K4rlsV0EZi7PgNQCqcgWgumUAvZIMiA4w1/ND9OnVa2cGCQFLtPLW',NULL,'2016-12-15 14:11:28','user3','user','user3 company','2016-12-15 14:11:28','2016-12-15 14:11:28',NULL,NULL,'female',NULL,NULL,NULL,NULL,NULL,NULL,NULL);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
