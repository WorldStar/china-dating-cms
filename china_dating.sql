/*
SQLyog Ultimate v11.33 (64 bit)
MySQL - 10.1.13-MariaDB : Database - china_dating
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`ihusigro_chinadating` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci */;

USE `ihusigro_chinadating`;

/*Table structure for table `activations` */

DROP TABLE IF EXISTS `activations`;

CREATE TABLE `activations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `completed` tinyint(1) NOT NULL DEFAULT '0',
  `completed_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `activations` */

insert  into `activations`(`id`,`user_id`,`code`,`completed`,`completed_at`,`created_at`,`updated_at`) values (1,1,'DPpnA4ZQN6DvFNzKfO2QALMZhRrlVRV7',1,'2016-12-15 09:18:50','2016-12-15 09:18:50','2016-12-15 09:18:50'),(2,2,'USMbOXMSwZTmI9ThPPQCNgENalrGzpoZ',1,'2016-12-15 13:36:22','2016-12-15 13:36:22','2016-12-15 13:36:22'),(3,3,'ywW8Ew23f5Y1c8m9Qdv1vZ58SMCisldX',1,'2016-12-15 14:09:49','2016-12-15 14:09:49','2016-12-15 14:09:49'),(4,4,'y42jzR0f7Mjx4UgBQ0JlXKnys3SMrlhj',1,'2016-12-15 14:11:28','2016-12-15 14:11:28','2016-12-15 14:11:28'),(5,5,'A6ihUF9TS5UuCeHT7q0dWltfggWmfPoE',1,'2016-12-20 09:18:08','2016-12-20 09:18:08','2016-12-20 09:18:08');

/*Table structure for table `date_advertises` */

DROP TABLE IF EXISTS `date_advertises`;

CREATE TABLE `date_advertises` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `photo` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `weburl` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `expired` date NOT NULL,
  `type` int(3) NOT NULL COMMENT '1-top, 2-middle, 3-bottom',
  `status` int(3) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `date_advertises` */

insert  into `date_advertises`(`id`,`title`,`description`,`photo`,`weburl`,`expired`,`type`,`status`,`created_at`,`updated_at`) values (9,'上鱼泡泡','<p>一元约战LOL</p>\r\n','GQKWYHppIw.jpg','http://baidu.com','2017-01-26',1,0,'2016-12-26 11:33:24','2016-12-26 11:33:24'),(10,'点亮圣诞树','<p>点亮圣诞树</p>\r\n','rt2umA58Y2.jpg','http://baidu.com','2017-01-26',2,0,'2016-12-26 11:34:14','2016-12-26 11:34:14'),(11,'12月星大神','<p>12月星大神</p>\r\n','TjqXiCnNoa.jpg','http://baidu.com','2017-01-26',3,0,'2016-12-26 11:36:01','2016-12-26 11:36:01');

/*Table structure for table `date_business` */

DROP TABLE IF EXISTS `date_business`;

CREATE TABLE `date_business` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` int(3) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `date_business` */

insert  into `date_business`(`id`,`name`,`status`,`created_at`,`updated_at`) values (4,'计算机/互联网/通信',0,'2016-12-26 11:48:33','2016-12-26 11:48:33'),(5,'生产/工业/制造',0,'2016-12-26 11:49:01','2016-12-26 11:49:01');

/*Table structure for table `date_cat_classics` */

DROP TABLE IF EXISTS `date_cat_classics`;

CREATE TABLE `date_cat_classics` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `cat_id` int(10) NOT NULL DEFAULT '0',
  `subcat_id` int(10) NOT NULL DEFAULT '0',
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` int(3) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `date_cat_classics` */

insert  into `date_cat_classics`(`id`,`cat_id`,`subcat_id`,`name`,`status`,`created_at`,`updated_at`) values (6,4,6,'新货',0,'2016-12-26 11:09:03','2016-12-26 11:09:03'),(7,4,6,'正式',0,'2016-12-26 11:09:30','2016-12-26 11:09:30'),(8,4,6,'音诱',0,'2016-12-26 11:10:35','2016-12-26 11:10:35'),(9,4,7,'实习脸萌',0,'2016-12-26 11:11:20','2016-12-26 11:11:20'),(10,4,7,'颜值担当',0,'2016-12-26 11:11:58','2016-12-26 11:11:58'),(11,3,8,'黄铜',0,'2016-12-26 11:13:03','2016-12-26 11:13:03'),(12,3,8,'白银',0,'2016-12-26 11:13:14','2016-12-26 11:13:14'),(13,3,9,'传说以下',0,'2016-12-26 11:13:49','2016-12-26 11:13:49'),(14,3,9,'传说以上',0,'2016-12-26 11:13:59','2016-12-26 11:13:59'),(15,2,10,'黄铜',0,'2016-12-26 11:14:22','2016-12-26 11:14:22'),(16,2,10,'白银',0,'2016-12-26 11:14:30','2016-12-26 11:14:30'),(17,2,11,'青铜',0,'2016-12-26 11:15:09','2016-12-26 11:15:09'),(18,2,11,'白银',0,'2016-12-26 11:15:17','2016-12-26 11:15:17'),(19,5,12,'实习',0,'2016-12-26 11:15:35','2016-12-26 11:15:35'),(20,5,12,'正式',0,'2016-12-26 11:15:42','2016-12-26 11:15:42'),(21,5,13,'小影迷',0,'2016-12-26 11:16:23','2016-12-26 11:16:23'),(22,5,13,'影评师',0,'2016-12-26 11:16:46','2016-12-26 11:16:46');

/*Table structure for table `date_cat_favorites` */

DROP TABLE IF EXISTS `date_cat_favorites`;

CREATE TABLE `date_cat_favorites` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL DEFAULT '0',
  `cat_id` int(10) NOT NULL DEFAULT '0',
  `subcat_id` int(10) NOT NULL DEFAULT '0',
  `favorite` int(3) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `user id` (`user_id`),
  CONSTRAINT `user id` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `date_cat_favorites` */

insert  into `date_cat_favorites`(`id`,`user_id`,`cat_id`,`subcat_id`,`favorite`,`created_at`,`updated_at`) values (1,4,1,0,2,'2016-12-20 10:26:50','0000-00-00 00:00:00'),(2,4,1,1,3,'2016-12-20 11:02:41','0000-00-00 00:00:00');

/*Table structure for table `date_cat_prices` */

DROP TABLE IF EXISTS `date_cat_prices`;

CREATE TABLE `date_cat_prices` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `cat_id` int(10) NOT NULL DEFAULT '0',
  `subcat_id` int(10) NOT NULL DEFAULT '0',
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` int(3) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `date_cat_prices` */

insert  into `date_cat_prices`(`id`,`cat_id`,`subcat_id`,`name`,`status`,`created_at`,`updated_at`) values (1,1,3,'60',0,'2016-12-20 14:50:58','2016-12-20 06:49:58'),(2,1,1,'30',0,'2016-12-20 06:51:10','2016-12-20 06:51:10'),(3,1,1,'40',0,'2016-12-20 06:51:19','2016-12-20 06:51:19'),(4,1,1,'20',0,'2016-12-20 06:51:58','2016-12-20 06:51:58'),(5,4,6,'25',0,'2016-12-26 11:17:28','2016-12-26 11:17:28'),(6,4,6,'30',0,'2016-12-26 11:17:34','2016-12-26 11:17:34'),(7,4,6,'35',0,'2016-12-26 11:17:41','2016-12-26 11:17:41'),(8,4,7,'80',0,'2016-12-26 11:17:57','2016-12-26 11:17:57'),(9,4,7,'120',0,'2016-12-26 11:18:01','2016-12-26 11:18:01'),(10,4,7,'150',0,'2016-12-26 11:18:06','2016-12-26 11:18:06'),(11,3,8,'20',0,'2016-12-26 11:18:24','2016-12-26 11:18:24'),(12,3,8,'30',0,'2016-12-26 11:18:28','2016-12-26 11:18:28'),(13,3,8,'35',0,'2016-12-26 11:18:33','2016-12-26 11:18:33'),(14,2,9,'25',0,'2016-12-26 11:18:47','2016-12-26 11:18:47'),(15,3,9,'25',0,'2016-12-26 11:18:57','2016-12-26 11:18:57'),(16,3,9,'30',0,'2016-12-26 11:19:03','2016-12-26 11:19:03'),(17,2,10,'39',0,'2016-12-26 11:19:19','2016-12-26 11:19:19'),(18,2,10,'59',0,'2016-12-26 11:19:24','2016-12-26 11:19:24'),(19,2,10,'99',0,'2016-12-26 11:19:28','2016-12-26 11:19:28'),(20,2,11,'59',0,'2016-12-26 11:19:43','2016-12-26 11:19:43'),(21,2,11,'99',0,'2016-12-26 11:19:47','2016-12-26 11:19:47'),(22,2,11,'129',0,'2016-12-26 11:19:52','2016-12-26 11:19:52'),(23,5,12,'12',0,'2016-12-26 11:20:03','2016-12-26 11:20:03'),(24,5,12,'15',0,'2016-12-26 11:20:08','2016-12-26 11:20:08'),(25,5,12,'18',0,'2016-12-26 11:20:13','2016-12-26 11:20:13'),(26,5,13,'15',0,'2016-12-26 11:20:22','2016-12-26 11:20:22'),(27,5,13,'30',0,'2016-12-26 11:20:26','2016-12-26 11:20:26'),(28,5,13,'50',0,'2016-12-26 11:20:31','2016-12-26 11:20:31');

/*Table structure for table `date_category` */

DROP TABLE IF EXISTS `date_category`;

CREATE TABLE `date_category` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `icon` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` int(3) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `date_category` */

insert  into `date_category`(`id`,`name`,`icon`,`status`,`created_at`,`updated_at`) values (2,'线下游戏','D5JOYUgLh7.jpg',0,'2016-12-26 19:03:28','2016-12-26 11:03:28'),(3,'线上游戏','5x462Ch1y1.jpeg',0,'2016-12-26 19:02:47','2016-12-26 11:02:47'),(4,'情感咨询','1BDRDRgdrQ.jpg',0,'2016-12-26 19:02:28','2016-12-26 11:02:28'),(5,'线上娱乐','42YCFcIX7t.jpg',0,'2016-12-26 11:03:50','2016-12-26 11:03:50');

/*Table structure for table `date_chatting_group_masters` */

DROP TABLE IF EXISTS `date_chatting_group_masters`;

CREATE TABLE `date_chatting_group_masters` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` int(3) NOT NULL DEFAULT '0',
  `cat_id` int(10) NOT NULL DEFAULT '0' COMMENT '0',
  `subcat_id` int(10) NOT NULL DEFAULT '0' COMMENT '0 - chatting group',
  `status` int(3) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `date_chatting_group_masters` */

insert  into `date_chatting_group_masters`(`id`,`user_id`,`title`,`description`,`type`,`cat_id`,`subcat_id`,`status`,`created_at`,`updated_at`) values (1,5,'大神群组1','大神群组1',0,2,4,0,'2016-12-26 21:48:33','0000-00-00 00:00:00'),(4,6,'聊天群组1','聊天群组1',3,0,0,0,'2016-12-26 21:47:21','0000-00-00 00:00:00'),(5,7,'大神群组2','大神群组2',0,5,12,0,'2016-12-26 21:48:42','0000-00-00 00:00:00'),(6,6,'聊天群组2','聊天群组2',0,0,0,0,'2016-12-26 21:47:33','0000-00-00 00:00:00');

/*Table structure for table `date_chatting_groups` */

DROP TABLE IF EXISTS `date_chatting_groups`;

CREATE TABLE `date_chatting_groups` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `group_id` int(10) NOT NULL,
  `user_id` int(10) NOT NULL,
  `status` int(3) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `date_chatting_groups` */

insert  into `date_chatting_groups`(`id`,`group_id`,`user_id`,`status`,`created_at`,`updated_at`) values (1,1,5,0,'2016-12-24 01:29:19','0000-00-00 00:00:00'),(2,1,6,0,'2016-12-24 01:29:23','0000-00-00 00:00:00'),(6,4,6,0,'2016-12-24 01:29:36','0000-00-00 00:00:00'),(7,5,7,0,'2016-12-26 21:48:58','0000-00-00 00:00:00'),(8,5,5,0,'2016-12-26 21:49:03','0000-00-00 00:00:00'),(9,6,7,0,'2016-12-26 21:49:10','0000-00-00 00:00:00');

/*Table structure for table `date_cities` */

DROP TABLE IF EXISTS `date_cities`;

CREATE TABLE `date_cities` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` int(3) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `date_cities` */

insert  into `date_cities`(`id`,`name`,`status`,`created_at`,`updated_at`) values (6,'北京',0,'2016-12-26 11:43:04','2016-12-26 11:43:04'),(7,'长沙',0,'2016-12-26 11:43:16','2016-12-26 11:43:16'),(8,'成都',0,'2016-12-26 11:43:29','2016-12-26 11:43:29'),(9,'重庆',0,'2016-12-26 11:43:37','2016-12-26 11:43:37'),(10,'常州',0,'2016-12-26 11:43:49','2016-12-26 11:43:49');

/*Table structure for table `date_group_types` */

DROP TABLE IF EXISTS `date_group_types`;

CREATE TABLE `date_group_types` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` int(3) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `date_group_types` */

insert  into `date_group_types`(`id`,`name`,`status`,`created_at`,`updated_at`) values (1,'商用楼',0,'2016-12-26 19:50:57','0000-00-00 00:00:00'),(2,'小区',0,'2016-12-26 19:51:02','0000-00-00 00:00:00'),(3,'学校',0,'2016-12-26 19:51:12','0000-00-00 00:00:00');

/*Table structure for table `date_news` */

DROP TABLE IF EXISTS `date_news`;

CREATE TABLE `date_news` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) NOT NULL DEFAULT '0',
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `photo` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` int(3) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `date_news` */

insert  into `date_news`(`id`,`user_id`,`title`,`description`,`photo`,`status`,`created_at`,`updated_at`) values (2,5,'鱼泡泡官方客服','【年度盛典】恭喜小哥哥获得年度盛典邀请函一份，','news1.jpg',0,'2016-12-26 21:46:07','0000-00-00 00:00:00'),(3,6,'招冠','越是价钱的东西越容易被替换。','news2.jpg',0,'2016-12-26 21:46:12','0000-00-00 00:00:00'),(4,7,'招冠','越是价钱的东西越容易被替换。','news2.jpg',0,'2016-12-26 22:12:31','0000-00-00 00:00:00');

/*Table structure for table `date_news_comments` */

DROP TABLE IF EXISTS `date_news_comments`;

CREATE TABLE `date_news_comments` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `news_id` int(10) NOT NULL DEFAULT '0',
  `user_id` int(10) NOT NULL DEFAULT '0',
  `comment` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` int(3) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `date_news_comments` */

insert  into `date_news_comments`(`id`,`news_id`,`user_id`,`comment`,`status`,`created_at`,`updated_at`) values (5,2,5,'咬肩',0,'2016-12-26 21:46:26','0000-00-00 00:00:00'),(6,3,5,'前排',0,'2016-12-26 19:58:48','0000-00-00 00:00:00'),(7,3,6,'板凳',0,'2016-12-26 21:46:31','0000-00-00 00:00:00');

/*Table structure for table `date_news_favorites` */

DROP TABLE IF EXISTS `date_news_favorites`;

CREATE TABLE `date_news_favorites` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `news_id` int(10) NOT NULL DEFAULT '0',
  `user_id` int(10) NOT NULL DEFAULT '0',
  `favo_num` int(10) NOT NULL DEFAULT '0',
  `status` int(3) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `date_news_favorites` */

insert  into `date_news_favorites`(`id`,`news_id`,`user_id`,`favo_num`,`status`,`created_at`,`updated_at`) values (1,2,5,5,0,'2016-12-26 21:46:38','0000-00-00 00:00:00'),(2,3,6,3,0,'2016-12-26 21:46:54','0000-00-00 00:00:00');

/*Table structure for table `date_notifications` */

DROP TABLE IF EXISTS `date_notifications`;

CREATE TABLE `date_notifications` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `expired` date NOT NULL,
  `status` int(3) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `date_notifications` */

insert  into `date_notifications`(`id`,`title`,`description`,`expired`,`status`,`created_at`,`updated_at`) values (1,'年度盛典邀请名单已出炉...','<p>点击【首页】-鱼泡泡2016年度盛典活动页面，查看邀请名单！</p>\r\n','2017-01-30',0,'2016-12-26 11:27:48','2016-12-26 11:27:48'),(2,'圣诞节到了，萌萌的女主...','<p>LOL女神来了！鱼泡泡圣诞福利，让你一元就能约女神，陪你打LOL！知名电竞解说郭MINI，台湾性感女玩家，美艳的砖石玩家，等着你来约！</p>\r\n','2017-01-26',0,'2016-12-26 11:31:09','2016-12-26 11:31:09');

/*Table structure for table `date_payment_method` */

DROP TABLE IF EXISTS `date_payment_method`;

CREATE TABLE `date_payment_method` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` int(3) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `date_payment_method` */

insert  into `date_payment_method`(`id`,`name`,`status`,`created_at`,`updated_at`) values (1,'支付宝',0,'2016-12-26 19:59:19','0000-00-00 00:00:00'),(2,'微信',0,'2016-12-26 19:59:23','0000-00-00 00:00:00'),(3,'QQ',0,'2016-12-21 00:59:20','0000-00-00 00:00:00');

/*Table structure for table `date_payment_tickets` */

DROP TABLE IF EXISTS `date_payment_tickets`;

CREATE TABLE `date_payment_tickets` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) NOT NULL,
  `pay_method` int(10) NOT NULL DEFAULT '0',
  `transaction_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `subscribe_id` int(255) NOT NULL,
  `transaction_fee` float NOT NULL,
  `amount` float NOT NULL,
  `invoice_number` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `payer_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `payer_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `paid_date` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` int(3) NOT NULL DEFAULT '0' COMMENT '0-input 1-output',
  `status` int(3) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `date_payment_tickets` */

insert  into `date_payment_tickets`(`id`,`user_id`,`pay_method`,`transaction_id`,`subscribe_id`,`transaction_fee`,`amount`,`invoice_number`,`payer_id`,`payer_name`,`paid_date`,`type`,`status`,`created_at`,`updated_at`) values (1,5,1,'A12345678',5,5,50,'I123454555','P12345566','star','2016-12-25',0,0,'2016-12-26 21:43:09','0000-00-00 00:00:00'),(2,5,2,'A12345679',4,3,30,'I123454556','P12345567','star','2016-12-26',1,0,'2016-12-26 21:44:01','0000-00-00 00:00:00');

/*Table structure for table `date_point_transactions` */

DROP TABLE IF EXISTS `date_point_transactions`;

CREATE TABLE `date_point_transactions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) NOT NULL DEFAULT '0',
  `receive_user_id` int(10) NOT NULL DEFAULT '0',
  `cat_id` int(10) NOT NULL DEFAULT '0',
  `subcat_id` int(10) NOT NULL DEFAULT '0',
  `pointtype_id` int(10) NOT NULL DEFAULT '0',
  `point_amount` int(10) NOT NULL DEFAULT '0',
  `content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` int(3) DEFAULT '0' COMMENT '0-input 1 - output',
  `status` int(3) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `date_point_transactions` */

insert  into `date_point_transactions`(`id`,`user_id`,`receive_user_id`,`cat_id`,`subcat_id`,`pointtype_id`,`point_amount`,`content`,`type`,`status`,`created_at`,`updated_at`) values (1,5,6,5,12,3,30,'test',0,0,'2016-12-26 21:39:24','0000-00-00 00:00:00'),(2,5,7,2,10,2,40,'test',1,0,'2016-12-26 21:39:34','0000-00-00 00:00:00');

/*Table structure for table `date_point_types` */

DROP TABLE IF EXISTS `date_point_types`;

CREATE TABLE `date_point_types` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` int(3) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `date_point_types` */

insert  into `date_point_types`(`id`,`name`,`status`,`created_at`,`updated_at`) values (1,'鱼泡泡账户',0,'2016-12-26 20:00:02','2016-12-20 14:59:32'),(2,'积分',0,'2016-12-26 20:00:07','0000-00-00 00:00:00'),(3,'优惠卷',0,'2016-12-26 20:00:22','0000-00-00 00:00:00'),(4,'魅力值',0,'2016-12-26 20:00:53','0000-00-00 00:00:00');

/*Table structure for table `date_subcategory` */

DROP TABLE IF EXISTS `date_subcategory`;

CREATE TABLE `date_subcategory` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `cat_id` int(10) unsigned NOT NULL DEFAULT '0',
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `icon` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` int(3) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `catid1` (`cat_id`),
  CONSTRAINT `catid1` FOREIGN KEY (`cat_id`) REFERENCES `date_category` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `date_subcategory` */

insert  into `date_subcategory`(`id`,`cat_id`,`name`,`icon`,`status`,`created_at`,`updated_at`) values (6,4,'声优聊天','GwPk3K6sxg.png',0,'2016-12-26 11:05:21','2016-12-26 11:05:21'),(7,4,'视频聊天','Fg9fS2HaKC.png',0,'2016-12-26 11:05:37','2016-12-26 11:05:37'),(8,3,'线上LOL','sosbZRhvpI.png',0,'2016-12-26 11:06:18','2016-12-26 11:06:18'),(9,3,'线上炉石','oCZ5Vu1sYu.png',0,'2016-12-26 11:06:40','2016-12-26 11:06:40'),(10,2,'线下LOL','uh084LAu79.png',0,'2016-12-26 11:07:09','2016-12-26 11:07:09'),(11,2,'线下守望','xkwb8zLTw4.png',0,'2016-12-26 11:07:28','2016-12-26 11:07:28'),(12,5,'线上歌手','iGlXy8cZCQ.png',0,'2016-12-26 11:07:47','2016-12-26 11:07:47'),(13,5,'线上观影','VcFYchzyyj.png',0,'2016-12-26 11:08:07','2016-12-26 11:08:07');

/*Table structure for table `date_subscribe_prices` */

DROP TABLE IF EXISTS `date_subscribe_prices`;

CREATE TABLE `date_subscribe_prices` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` int(3) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `date_subscribe_prices` */

insert  into `date_subscribe_prices`(`id`,`name`,`status`,`created_at`,`updated_at`) values (4,'30',0,'2016-12-26 11:46:47','2016-12-26 11:46:47'),(5,'50',0,'2016-12-26 11:46:53','2016-12-26 11:46:53'),(6,'100',0,'2016-12-26 11:46:57','2016-12-26 11:46:57');

/*Table structure for table `date_tags` */

DROP TABLE IF EXISTS `date_tags`;

CREATE TABLE `date_tags` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` int(3) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `date_tags` */

insert  into `date_tags`(`id`,`name`,`status`,`created_at`,`updated_at`) values (3,'星座',0,'2016-12-26 18:59:59','2016-12-19 16:52:00'),(4,'颜控',0,'2016-12-26 19:00:23','0000-00-00 00:00:00'),(5,'足球',0,'2016-12-26 19:00:35','0000-00-00 00:00:00'),(6,'桌游',0,'2016-12-26 19:00:45','0000-00-00 00:00:00'),(7,'声控',0,'2016-12-26 19:00:52','0000-00-00 00:00:00'),(8,'王者荣耀',0,'2016-12-26 19:01:13','0000-00-00 00:00:00');

/*Table structure for table `date_user_books` */

DROP TABLE IF EXISTS `date_user_books`;

CREATE TABLE `date_user_books` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) NOT NULL DEFAULT '0',
  `book_user_id` int(10) NOT NULL DEFAULT '0',
  `status` int(3) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `date_user_books` */

insert  into `date_user_books`(`id`,`user_id`,`book_user_id`,`status`,`created_at`,`updated_at`) values (2,5,6,0,'2016-12-26 21:37:07','0000-00-00 00:00:00');

/*Table structure for table `date_user_chats` */

DROP TABLE IF EXISTS `date_user_chats`;

CREATE TABLE `date_user_chats` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) NOT NULL DEFAULT '0',
  `chat_user_id` int(10) NOT NULL DEFAULT '0',
  `status` int(3) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `date_user_chats` */

insert  into `date_user_chats`(`id`,`user_id`,`chat_user_id`,`status`,`created_at`,`updated_at`) values (1,5,6,0,'2016-12-23 15:33:16','0000-00-00 00:00:00'),(2,5,7,0,'2016-12-26 21:36:57','0000-00-00 00:00:00');

/*Table structure for table `date_user_details` */

DROP TABLE IF EXISTS `date_user_details`;

CREATE TABLE `date_user_details` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) NOT NULL DEFAULT '0',
  `vip_id` int(10) NOT NULL DEFAULT '0',
  `user_card` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'user auth card image',
  `car_card` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `comment` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `lat` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lon` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` int(3) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `date_user_details` */

insert  into `date_user_details`(`id`,`user_id`,`vip_id`,`user_card`,`car_card`,`comment`,`lat`,`lon`,`status`,`created_at`,`updated_at`) values (1,5,3,'33333.jpg','11111.jpg','我喜欢此app','40.044503','116.419793',0,'2016-12-26 21:28:12','0000-00-00 00:00:00'),(2,6,4,'44444.jpg','22222.jpg','我喜欢此app','28.174634','113.227809',0,'2016-12-26 21:28:44','0000-00-00 00:00:00'),(3,7,5,'66666.jpg','','我喜欢此app','29.597239','106.449668',0,'2016-12-26 21:30:57','0000-00-00 00:00:00');

/*Table structure for table `date_user_masters` */

DROP TABLE IF EXISTS `date_user_masters`;

CREATE TABLE `date_user_masters` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) NOT NULL DEFAULT '0',
  `cat_id` int(10) NOT NULL DEFAULT '0',
  `subcat_id` int(10) NOT NULL DEFAULT '0',
  `price` int(10) NOT NULL DEFAULT '0' COMMENT 'price / hour',
  `video` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `audio` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `comment` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `action` int(3) NOT NULL DEFAULT '0' COMMENT '1-master, 0-remove mater',
  `high_action` int(3) NOT NULL DEFAULT '0' COMMENT '1-book, 2-confirmed',
  `status` int(3) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `date_user_masters` */

insert  into `date_user_masters`(`id`,`user_id`,`cat_id`,`subcat_id`,`price`,`video`,`audio`,`comment`,`action`,`high_action`,`status`,`created_at`,`updated_at`) values (1,5,2,4,20,'video1.mp4','audio1.mp3','我具有资格在线上LOL',0,0,0,'2016-12-26 21:41:08','2016-12-24 14:55:14'),(3,5,3,5,40,'video2.mp4','audio2.mp3','我具有资格在线上炉石',1,2,0,'2016-12-26 21:41:11','2016-12-24 15:57:07'),(4,6,5,12,50,'video2.mp4','audio2.mp3','我具有资格在线上炉石',0,0,0,'2016-12-26 21:41:17','0000-00-00 00:00:00');

/*Table structure for table `date_user_pockets` */

DROP TABLE IF EXISTS `date_user_pockets`;

CREATE TABLE `date_user_pockets` (
  `id` int(10) unsigned NOT NULL DEFAULT '0',
  `user_id` int(10) NOT NULL,
  `stock_amount` float NOT NULL,
  `app_amount` float NOT NULL,
  `integration_amount` float NOT NULL,
  `coupon_amount` int(11) NOT NULL,
  `favorite_amount` float NOT NULL,
  `status` int(3) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `date_user_pockets` */

/*Table structure for table `date_vips` */

DROP TABLE IF EXISTS `date_vips`;

CREATE TABLE `date_vips` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `low` int(10) NOT NULL DEFAULT '0',
  `high` int(10) NOT NULL DEFAULT '0',
  `status` int(3) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `date_vips` */

insert  into `date_vips`(`id`,`name`,`low`,`high`,`status`,`created_at`,`updated_at`) values (2,'VIP0',1,99,0,'2016-12-26 18:58:37','2016-12-19 17:53:59'),(3,'VIP1',100,999,0,'2016-12-26 18:58:41','0000-00-00 00:00:00'),(4,'VIP2',1000,2999,0,'2016-12-26 18:58:43','0000-00-00 00:00:00'),(5,'VIP3',3000,9999,0,'2016-12-26 18:58:51','0000-00-00 00:00:00');

/*Table structure for table `migrations` */

DROP TABLE IF EXISTS `migrations`;

CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `migrations` */

insert  into `migrations`(`migration`,`batch`) values ('2014_07_02_230147_migration_cartalyst_sentinel',1),('2014_10_04_174350_soft_delete_users',1),('2014_12_10_011106_add_fields_to_user_table',1),('2015_08_09_200015_create_blog_module_table',1),('2015_08_11_064636_add_slug_to_blogs_table',1),('2015_08_19_073929_create_taggable_table',1),('2015_11_10_140011_create_files_table',1),('2016_01_02_062647_create_tasks_table',1),('2016_04_26_054601_create_datatables_table',1);

/*Table structure for table `notifications` */

DROP TABLE IF EXISTS `notifications`;

CREATE TABLE `notifications` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) NOT NULL,
  `content` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `opt_val` int(3) NOT NULL DEFAULT '0',
  `status` int(3) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `notifications` */

insert  into `notifications`(`id`,`user_id`,`content`,`opt_val`,`status`,`created_at`) values (10,3,'notification 1',1,1,'2016-12-19 18:33:55'),(11,3,'notification 2',2,1,'2016-12-19 18:33:59'),(12,3,'notification 3',3,1,'2016-12-19 18:34:03');

/*Table structure for table `persistences` */

DROP TABLE IF EXISTS `persistences`;

CREATE TABLE `persistences` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `persistences_code_unique` (`code`)
) ENGINE=InnoDB AUTO_INCREMENT=66 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `persistences` */

insert  into `persistences`(`id`,`user_id`,`code`,`created_at`,`updated_at`) values (13,2,'ygYWSAXSWeCReQfedO0nrcTNeMfcYbB0','2016-12-15 13:50:09','2016-12-15 13:50:09'),(14,2,'TNOYIdJh4FZcbpOm8jbGveCM2QoFecqL','2016-12-15 13:50:54','2016-12-15 13:50:54'),(15,2,'RAJnPlWhsNDwo4fDqPcVrFdp6LhSq7Vo','2016-12-15 13:51:08','2016-12-15 13:51:08'),(22,1,'EjapJbbjawMbDJ5gvfmoBvKrRRjDzjUn','2016-12-15 16:27:19','2016-12-15 16:27:19'),(27,3,'W2HTDaK0nhytUUzdvZeb4c75K6BaYoRj','2016-12-19 13:57:05','2016-12-19 13:57:05'),(29,1,'QzBwMpIbY9EQcz3fl9PH9DGRJz9msCVu','2016-12-19 15:37:20','2016-12-19 15:37:20'),(30,1,'AVXF1CuOTi2BKC04zKpxjWlaDAlVIBef','2016-12-20 02:23:34','2016-12-20 02:23:34'),(31,1,'mOT28iKf6sLOwKbb5RzuJPZbuoGfyrkO','2016-12-20 03:36:47','2016-12-20 03:36:47'),(32,1,'BucfzhTpZt4zU16sshznKLp5bDuogf0k','2016-12-20 03:44:22','2016-12-20 03:44:22'),(33,1,'ZJjsnUqieB8Dii8Psn3sYVTDCucPUCG9','2016-12-20 03:59:58','2016-12-20 03:59:58'),(34,1,'mAkZ9DQiwxDJaqBfOKcpWylfms3YeOTT','2016-12-20 04:50:32','2016-12-20 04:50:32'),(35,1,'9nmTuy7a5NpA1JG0D5NZLPpNJZR7XKOs','2016-12-20 07:05:54','2016-12-20 07:05:54'),(36,1,'jkPIByS5x9mAVBnlcVAkmGnJtKcrVGS1','2016-12-20 07:29:58','2016-12-20 07:29:58'),(37,1,'5J6tK8KgSwByQJSujuzHbT7pc0c0Jqx6','2016-12-20 10:05:32','2016-12-20 10:05:32'),(38,1,'O31piGHe9ruq9bzxzJkqomwv7mEpPhV2','2016-12-20 13:21:02','2016-12-20 13:21:02'),(39,1,'QSuJJyBDQLMSEu32wwD45wsqK3GFKSJa','2016-12-20 13:24:26','2016-12-20 13:24:26'),(40,1,'G8jE9VgqqNVCan8WyY9gqqrdeoKPHk42','2016-12-20 14:58:48','2016-12-20 14:58:48'),(41,1,'SgtINMeWNBG29fm4GXKF5qrr4jgnQEnP','2016-12-22 02:21:45','2016-12-22 02:21:45'),(42,1,'zAThwVNujYVOQwsOD0YIRoaB4rnDDrJj','2016-12-22 02:57:06','2016-12-22 02:57:06'),(43,1,'44Om8f3VBv6cd3lDJCw9vOeKILOlSCEF','2016-12-22 07:38:25','2016-12-22 07:38:25'),(44,1,'UiSsyx23Lzy4kkSRbrjXBCLnHHVHrI88','2016-12-22 08:45:21','2016-12-22 08:45:21'),(45,1,'EBLUuJoT8GCR6Bjkru1nLBgEIhMzyEps','2016-12-22 09:52:46','2016-12-22 09:52:46'),(46,1,'sLnIrW9S47i84IbPyXPCggQYIobmrCmB','2016-12-22 11:49:56','2016-12-22 11:49:56'),(47,1,'oidG5uqGvfPfNlwkTe8h9ZaqMTxBeAMS','2016-12-22 11:50:33','2016-12-22 11:50:33'),(48,1,'u0zCoYhCFEtBkCwnzydWWGxXAfxC0huZ','2016-12-22 11:51:08','2016-12-22 11:51:08'),(49,1,'drpvzL1om5ioiZgeydGJMxJJb6usfikH','2016-12-22 14:29:32','2016-12-22 14:29:32'),(50,1,'2WJC23deH4Xb2smsl9RU3ZiYXkfeQ5zO','2016-12-22 15:29:31','2016-12-22 15:29:31'),(51,1,'j7w4DKkbHap3CP9NLx5A77hIbLyTCb6Z','2016-12-22 15:29:39','2016-12-22 15:29:39'),(52,1,'y05UmS6JMBzmh5JOI3VlVDfljoMZAKgf','2016-12-22 18:14:47','2016-12-22 18:14:47'),(53,1,'LzU0tiXPiHQZYHw1NlU63gQmIMENTBqa','2016-12-23 01:16:05','2016-12-23 01:16:05'),(54,1,'5vmY46cNUhjR1sR6JFV2eERfgCK3xhtC','2016-12-23 01:17:13','2016-12-23 01:17:13'),(55,1,'iXMn6z3SyWJLtZst1V4x3VaWdOHE717Z','2016-12-23 01:23:11','2016-12-23 01:23:11'),(56,1,'8hjC38mJcxIWOV2SccwkZfMAcmf4jliW','2016-12-23 01:24:49','2016-12-23 01:24:49'),(57,1,'CSYr8izp1FEZhcHqfqMYGjFmz19rW37i','2016-12-23 04:35:09','2016-12-23 04:35:09'),(58,1,'kT5ZBzjHBa2vs0UoZgzumPHmQFZkhDkZ','2016-12-23 07:14:04','2016-12-23 07:14:04'),(59,1,'Ycm9xgAAx1Ykx1YYAxuOVbj4M3w8vPYH','2016-12-23 14:43:56','2016-12-23 14:43:56'),(60,1,'LTorMH38ukhktb4DYwPplL8Odlvq6xmi','2016-12-24 13:55:04','2016-12-24 13:55:04'),(62,1,'dkGXQKTHwwvMnbnzE5zZ2vUUzYnlicSL','2016-12-24 15:54:53','2016-12-24 15:54:53'),(63,1,'TEpyoEzOlei6fjT1LetQqblpsuJ8KnTz','2016-12-24 15:56:14','2016-12-24 15:56:14'),(64,1,'WQ9gAujNUGvkiLrejjuoQEFbrY8EsINB','2016-12-26 00:43:52','2016-12-26 00:43:52'),(65,1,'X5RcMAdDHcgJvlVrDlYrRb7C4gyRAVLT','2016-12-26 13:09:13','2016-12-26 13:09:13');

/*Table structure for table `reminders` */

DROP TABLE IF EXISTS `reminders`;

CREATE TABLE `reminders` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `completed` tinyint(1) NOT NULL DEFAULT '0',
  `completed_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `reminders` */

/*Table structure for table `role_users` */

DROP TABLE IF EXISTS `role_users`;

CREATE TABLE `role_users` (
  `user_id` int(10) unsigned NOT NULL,
  `role_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`user_id`,`role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `role_users` */

insert  into `role_users`(`user_id`,`role_id`,`created_at`,`updated_at`) values (1,1,'2016-12-15 09:18:50','2016-12-15 09:18:50'),(2,2,'2016-12-15 13:36:22','2016-12-15 13:36:22'),(3,2,'2016-12-15 14:09:49','2016-12-15 14:09:49'),(4,2,'2016-12-15 14:11:28','2016-12-15 14:11:28'),(5,3,'2016-12-20 09:18:08','2016-12-20 09:18:08'),(6,4,'2016-12-20 09:18:08','2016-12-20 09:18:08'),(7,4,'2016-12-20 09:18:08','2016-12-20 09:18:08');

/*Table structure for table `roles` */

DROP TABLE IF EXISTS `roles`;

CREATE TABLE `roles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `permissions` text COLLATE utf8_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `roles_slug_unique` (`slug`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `roles` */

insert  into `roles`(`id`,`slug`,`name`,`permissions`,`created_at`,`updated_at`) values (1,'admin','Admin','{\"admin\":1}','2016-12-15 09:18:50','2016-12-15 09:18:50'),(2,'Sub Admin','Sub Admin','{\"sub admin\":2}','2016-12-15 09:18:50','2016-12-15 09:18:50'),(3,'Master','Master','{\"masters\":3}','2016-12-15 09:18:50','2016-12-15 09:18:50'),(4,'Customer','Customer','{\"customer\":4}','2016-12-15 09:18:50','2016-12-15 09:18:50');

/*Table structure for table `tasks` */

DROP TABLE IF EXISTS `tasks`;

CREATE TABLE `tasks` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `finished` tinyint(4) NOT NULL DEFAULT '0',
  `task_description` text COLLATE utf8_unicode_ci NOT NULL,
  `task_deadline` date NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `tasks` */

/*Table structure for table `throttle` */

DROP TABLE IF EXISTS `throttle`;

CREATE TABLE `throttle` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned DEFAULT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ip` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `throttle_user_id_index` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `throttle` */

insert  into `throttle`(`id`,`user_id`,`type`,`ip`,`created_at`,`updated_at`) values (1,NULL,'global',NULL,'2016-12-15 10:20:12','2016-12-15 10:20:12'),(2,NULL,'ip','192.168.1.252','2016-12-15 10:20:12','2016-12-15 10:20:12'),(3,1,'user',NULL,'2016-12-15 10:20:12','2016-12-15 10:20:12'),(4,NULL,'global',NULL,'2016-12-15 10:20:16','2016-12-15 10:20:16'),(5,NULL,'ip','192.168.1.252','2016-12-15 10:20:16','2016-12-15 10:20:16'),(6,1,'user',NULL,'2016-12-15 10:20:16','2016-12-15 10:20:16'),(7,NULL,'global',NULL,'2016-12-15 10:21:37','2016-12-15 10:21:37'),(8,NULL,'ip','192.168.1.252','2016-12-15 10:21:37','2016-12-15 10:21:37'),(9,1,'user',NULL,'2016-12-15 10:21:37','2016-12-15 10:21:37'),(10,NULL,'global',NULL,'2016-12-15 13:39:14','2016-12-15 13:39:14'),(11,NULL,'ip','192.168.1.252','2016-12-15 13:39:14','2016-12-15 13:39:14');

/*Table structure for table `users` */

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `permissions` text COLLATE utf8_unicode_ci,
  `last_login` timestamp NULL DEFAULT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `first_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `company_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `bio` text COLLATE utf8_unicode_ci,
  `gender` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `dob` date DEFAULT NULL,
  `pic` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `country` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `state` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `city` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `postal` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `business` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `school` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `tags` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email1` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `phone1` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `weisin_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `qq_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `online` int(3) NOT NULL DEFAULT '0',
  `userno` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `users` */

insert  into `users`(`id`,`email`,`password`,`permissions`,`last_login`,`username`,`first_name`,`last_name`,`company_name`,`created_at`,`updated_at`,`deleted_at`,`bio`,`gender`,`dob`,`pic`,`country`,`state`,`city`,`address`,`postal`,`business`,`school`,`tags`,`email1`,`phone1`,`weisin_id`,`qq_id`,`online`,`userno`) values (1,'admin@admin.com','$2y$10$EmetDrC2rdsdVoVPmm/AYOn8z.Kit/bdcNUvobJQQT59S5fEXoPna',NULL,'2016-12-26 13:09:13','admin','Admin','',NULL,'2016-12-15 09:18:50','2016-12-26 13:09:13',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'','','','','','','',0,''),(2,'user1@gmail.com','$2y$10$xBYdBHRPRnbk4nCg7OzHdemwUa5Mfazl8HwevcDq4513Y8IqNyx4y',NULL,'2016-12-15 15:48:48','subadmin1','Sub Admin1','',NULL,'2016-12-15 13:36:22','2016-12-15 15:48:48',NULL,NULL,'female',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'','','','','','','',0,''),(3,'user2@gmail.com','$2y$10$AWlbu1nR3CfVBvgAqA3VFu3i5SpdI9q40ktd6vGrpe.40vi9HDXua',NULL,'2016-12-19 13:57:05','subadmin2','Sub Admin2','',NULL,'2016-12-15 14:09:49','2016-12-19 13:57:05',NULL,NULL,'female',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'','','','','','','',1,''),(4,'user3@gmail.com','$2y$10$K4rlsV0EZi7PgNQCqcgWgumUAvZIMiA4w1/ND9OnVa2cGCQFLtPLW',NULL,'2016-12-15 14:11:28','subadmin3','Sub Admin3','','user3 company','2016-12-15 14:11:28','2016-12-20 10:12:33',NULL,'','female','2016-12-20',NULL,'','','','','','','','','','','','',1,'23408974'),(5,'future.syg1118@gmail.com','$2y$10$nSujhQdFI7gBojTMFGRkxeieUNBN4psNXqemq4Wo.pt9cmzBL4Nfa',NULL,NULL,'star1','star1','',NULL,'2016-12-20 09:18:08','2016-12-20 09:51:45',NULL,'','male','2016-08-15',NULL,'JP','Alabama','北京','sdfsdf','12312','计算机/互联网/通信','school1','星座/颜控/足球','future.syg1118@gmail.com','12345678','1234556','',1,'23408975'),(6,'pearl.syg1118@gmail.com','$2y$10$Zqx.xp0xJzo/GXhzK81byOzbxyyzCLdF1M6umrD9u19ZYrllqXViq',NULL,NULL,'customer1','customer1','',NULL,'2016-12-20 09:18:08','2016-12-20 18:18:44',NULL,'','male','2016-08-15',NULL,'JP','Alabama','长沙','sdfsdf','12312','生产/工业/制造','school2','桌游/声控/王者荣耀','pearl.syg1118@gmail.com','23456789','','2345676',1,'23408976'),(7,'pearl.syg108@gmail.com','$2y$10$Zqx.xp0xJzo/GXhzK81byOzbxyyzCLdF1M6umrD9u19ZYrllqXViq',NULL,NULL,'customer1','customer1','',NULL,'2016-12-20 09:18:08','2016-12-20 18:18:44',NULL,'','female','2016-08-15',NULL,'JP','Alabama','重庆','sdfsdf','12312','生产/工业/制造','school2','星座/颜控/足球','pearl.syg108@gmail.com','12345678','1234556','2345676',1,'23408976');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
