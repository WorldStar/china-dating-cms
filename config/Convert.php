<?php

return [
		'convert_item' => array('EN', 'CN'),

		// layouts/default.blade.php
		'cms_name' => array('鱼泡泡 Admin Panel', '鱼泡泡 管理面板'),

		// _left_menu.blade.php
		'dashboard' => array('Dashboard', '仪表盘'),
		'admins' => array('Admins', '管理员'),
		'add_admins' => array('Add Admins', '添加管理员'),
		'delete_admins' => array('Delete Admins', '删除管理员'),
		'masters' => array('Masters', '大神'),
		'delete_masters' => array('Delete Masters', '删除大神'),
		'customers' => array('Customers', '用户'),
		'delete_customers' => array('Delete Customers', '删除用户'),
		'categories' => array('Categories', '项目'),
		'sub_categories' => array('Sub Categories', '子项目'),
		'classices' => array('Classices', '等级'),
		'prices' => array('Prices', '价格'),
		'settings' => array('Settings', '设置'),
		'cities' => array('Cities', '城市'),
		'vips' => array('VIPs', 'VIPs'),
		'tags' => array('Tags', '兴趣'),
		'subscribe_prices' => array('Subscribe Prices', '充值'),
		'business_options' => array('Business Options', '职业'),
		'reports' => array('Reports', '报告'),
		'sales_report' => array('Sales Report', '销售报告'),
		'user_report' => array('User Report', '用户报告'),
		'activity_report' => array('Activity Report', '活动报告'),
		'notifications' => array('Notifications', '公告'),
		'site_notifications' => array('Site Notifications', '网站公告'),
		'groups' => array('Groups', '群组'),
		'category_groups' => array('Category Groups', '项目群组'),
		'chat_groups' => array('Chat Groups', '聊天群组'),

		// users/index.blade.php
		'admins_list' => array('Admin List', '管理员目录'),
		'id' => array('ID', 'ID'),
		'first_name' => array('Real Name', '姓名'),
		'email' => array('E-mail', '电子邮箱'),
		'gender' => array('Gender', '性别'),
		'postal_code' => array('Postal Code', '邮编'),
		'status' => array('Status', '状态'),
		'create_date' => array('Created Date', '创建日期'),
		'actions' => array('Actions', '活动'),

		// users/show.blade.php
		'admin_detail_name' => array('View Admin Details', '管理员资料'),
		'admin_profile' => array('Admin Profile', '管理员资料'),
		'change_password' => array('Change Password', '修改密码'),
		'birthday' => array('Birthday', '生日'),
		'country' => array('Country', '国家'),
		'state' => array('State', '省'),
		'address' => array('Address', '地址'),

		// users/create.blade.php
		'bio' => array('Bio', '我的照片'),
		'user_group' => array('Group', '群组'),
		'password' => array('Password', '密码'),
		'submit' => array('Submit', '提交'),
		'confirm_password' => array('Confirm Password', '确认密码'),
		'profile_picture' => array('Profile picture', '上传图片'),
		'select_image' => array('Select Image', '选择图片'),
		'change' => array('Change', '修改'),
		'remove' => array('Remove', '删除'),
		'brief_intro' => array('brief intro', '简介'),
		'select' => array('Select', '选择'),
		'male' => array('MALE', '男'),
		'female' => array('FEMALE', '女'),
		'other' => array('OTHER', '其他'),
		'activate_user' => array('Activate User', '激活用户'),
		'previous' => array('Previous', '前一个'),
		'next' => array('Next', '下一个'),
		'finish' => array('Finish', '结尾'),
		'active_message' => array('If Admin is not activated, mail will be sent to user with activation link', '如果管理员还没有激活，邮件跟激活连接一起将被发送到他的邮箱。'),
		'admin_access_message' => array('Be careful with group selection, if you give admin access.. they can access admin section', '注意，如果您允许管理员权限，用户们可以接近以管理员。'),

		// users/deleted_users.blade.php


		//users/edit.blade.php
		'edit_admins' => array('Edit Admin', '编辑管理员'),
		'password_leave_message' => array('If you don\'t want to change password... please leave them empty', '如果管理员不想修改密码，请把下面留着空白。'),
		//users/create.blade.php에 존재하는 변수들을 리용


		// masters/index.blade.php
		'masters_list' => array('Master List', '大神目录'),
		'customer_name' => array('User Name', '昵称'),
		'phone' => array('Phone', '手机号'),
		'online' => array('Online', '上线'),


		// masters/show.blade.php
		'master_profile' => array('Master Profile', '大神资料'),
		'payment_transactions' => array('Payment Transactions', '支付交易'),
		'point_transactions' => array('Point Transactions', '订单交易'),
		'update_to_customer' => array('Update to Customer', '更新到用户'),
		'master_photo' => array('Master Photo', '大神 照片'),
		'master_id' => array('Master ID', '大神 ID'),
		'master_user_name' => array('Master User Name', '大神 昵称'),
		'master_real_name' => array('Master Real Name', '大神 姓名'),
		'weixin_id' => array('Weisin ID', '微信 ID'),
		'qq_id' => array('QQ ID', 'QQ ID'),
		'master_phone' => array('Master Phone', '大神 手机号'),
		'master_email' => array('Master Email', '大神 邮箱'),
		'master_stock' => array('Master Stock', '大神 余额'),
		'vip_type' => array('VIP Type', 'VIP 等级'),
		'stock_amount' => array('Stock Amount', '余额'),
		'app_amount' => array('Stock Amount', '余额'),
		'coupon_amount' => array('Coupon Amount', '优惠卷'),
		'favorite_amount' => array('Favorite Amount', '赞同'),
		'integration_amount' => array('Integration Amount', '积分'),
		'authorization_photos' => array('Authorization Photos', '认证照片'),
		'master_card' => array('Master\' Card', '大神身份证'),
		'car_card' => array('Car Card', '大神驾驶证'),
		'master_video_audio' => array('Master Video/Audio', '大神视频/音频'),
		'master_video' => array('Master Video', '大神视频'),
		'master_audio' => array('Master Audio', '大神音频'),
		'others' => array('Others', '其他'),
		'business_type' => array('Business Type', '职业'),
		'school' => array('School', '学校'),
		'master_comment' => array('Master Comment', '大神评论'),
		'no' => array('NO', '编号'),
		'payer_id' => array('Payer ID', '付款人 ID'),
		'payer_name' => array('Payer Name', '付款人姓名'),
		'payment_method' => array('Payment Method', '支付种类'),
		'transaction_id' => array('Transaction ID', '交易 ID'),
		'subscribe_type' => array('Subscribe Type', '充值种类'),
		'payment_amount' => array('Payment Amount', '支付额数'),
		'transaction_fee' => array('Transaction Fee', '交易手续费'),
		'invoice_number' => array('Invoice Number', '订单编号'),
		'payment_type' => array('Payment Type', '支付 出/入'),
		'paid_date' => array('Paid Date', '支付日期'),
		'receiver_sender_id' => array('Receiver/Sender ID', '收款员/打款员 ID'),
		'point_type' => array('Point Type', '赞同种类'),
		'point_amount' => array('Point Amount', '赞同数量'),
		'content' => array('Content', '内容'),
		'input_output' => array('Input/Output', '输入/输出'),
		'point_date' => array('Point Date', '赞同日期'),
		'master_status' => array('Master Status', '大神状态'),
		'master_active_message' => array('If master is not activated, mail will be sent to master with activation link', '如果大神还没有激活，邮件跟激活连接一起将被发送到他的邮箱。'),
		'book_contacts' => array('Book Contacts', '预订客服'),
		'chatting_contacts' => array('Chatting Contacts', '聊天客服'),
		'master_update_message' => array('Do you really agree to update to master?', '您真同意更新到大神？'),
		'master_release_message' => array('Do you really agree to release from master?', '您真同意释放从大神？'),
		'master_update_to_high_message' => array('Do you really agree to update to high master?', '您真同意更新到高级大神？'),
		'book_contact_delete_message' => array('Do you really want to delete this booked Contact?', '您真同意删除此预订客服？'),
		'chat_contact_delete_message' => array('Do you really want to delete this chatting contact?', '您真同意删除此聊天客服？'),
		'contact_photo' => array('Contact Photo', '客服照片'),
		'contact_name' => array('Contact Name', '客服名字'),
		'contact_info' => array('Contact Info', '客服信息'),

		// masters/show.blade.php
		'requests_master' => array('Requests Master', '申请大神'),

		// masters/usermaster.blade.php
		'request_delete_message' => array('Do you really want to delete this request?', '您真想把此项目要删除？'),
		'request_inpute_message' => array('Please input a request.', '请输入项目名称。'),
		'request_active_message' => array('Do you really want to active this request?', '您真想把此项目要激活？'),
		'request_inactive_message' => array('Do you really want to inactive this request?', '您真想把此项目要不激活？'),

		// masters/preshow.blade.php
		'requests_master_detail' => array('Requests Master Detail', '申请大神详细'),

		//category/category.blade.php
		'category_name' => array('Category Name', '项目名称'),
		'edit' => array('Edit', '编辑'),
		'delete' => array('Delete', '删除'),
		'add_category' => array('Add Category', '添加项目'),
		'category_icon' => array('Category Icon', '项目小图片'),
		'add' => array('Add', '添加'),
		'cancel' => array('Cancel', '取消'),
		'save' => array('Save', '保存'),
		'ok' => array('OK', '确认'),
		'category_delete_message' => array('Do you really want to delete this category?', '您真想把此项目要删除？'),
		'category_inpute_message' => array('Please input a category name.', '请输入项目名称。'),
		'category_active_message' => array('Do you really want to active this category?', '您真想把此项目要激活？'),
		'category_inactive_message' => array('Do you really want to inactive this category?', '您真想把此项目要不激活？'),

		//category/subcategory.blade.php
		'subcategory_name' => array('Sub Category Name', '子项目名称'),
		'subcategory_icon' => array('Sub Category Icon', '子项目小图片'),
		'add_subcategory' => array('Add Sub Category', '添加子项目'),
		'subcategory_delete_message' => array('Do you really want to delete this sub category?', '您真想把此子项目要删除？'),
		'subcategory_inpute_message' => array('Please input a sub category name.', '请输入子项目名称。'),
		'subcategory_active_message' => array('Do you really want to active this sub category?', '您真想把此子项目要激活？'),
		'subcategory_inactive_message' => array('Do you really want to inactive this sub category?', '您真想把此子项目要不激活？'),

		//category/classic.blade.php
		'add_classic' => array('Add Classic', '添加等级'),
		'classic_delete_message' => array('Do you really want to delete this classic?', '您真想把此等级要删除？'),
		'classic_inpute_message' => array('Please input a classic name.', '请输入等级名称。'),
		'classic_active_message' => array('Do you really want to active this classic?', '您真想把此等级要激活？'),
		'classic_inactive_message' => array('Do you really want to inactive this classic?', '您真想把此等级要不激活？'),

		//category/create.blade.php

		//category/edit.blade.php
		'edit_category' => array('Edit Category', '编辑项目'),

		//category/sub_edit.blade.php
		'subedit_category' => array('Sub Edit Category', '编辑子项目'),
		'subcategory' => array('Sub Category', '子项目'),

		//category/price.blade.php
		'add_price' => array('Add Price', '添加价格'),
		'price_delete_message' => array('Do you really want to delete this price?', '您真想把此价格要删除？'),
		'price_inpute_message' => array('Please input a price name.', '请输入价格名称。'),
		'price_active_message' => array('Do you really want to active this price?', '您真想把此价格要激活？'),
		'price_inactive_message' => array('Do you really want to inactive this price?', '您真想把此价格要不激活？'),

		//index.blade.php   dashboard
		'view_today' => array('Views Today', '今天的观点'),
		'last_week' => array('Last Week', '上礼拜'),
		'last_month' => array('Last Month', '上个月'),
		'todays_sales' => array('Today\'s Sales', '今天的销售'),
		'news' => array('News', '动态'),
		'registered_users' => array('Registered Users', '注册用户'),
		'communication_load' => array('Communication Load', '通信负荷'),
		'load_on_server' => array('Load on the Server', '服务器上的负载'),
		'users_distributions' => array('Users Distributions', '用户分布'),
		'sales_distributions' => array('Sales Distributions', '销售分布'),
		'visitors_map' => array('Visitors Map', '游客地图'),
		'collapse' => array('Collapse', '折叠'),
		'refresh' => array('Refresh', '刷新'),
		'configurations' => array('Configurations', '配置'),
		'fullscreen' => array('Fullscreen', '全屏'),

		//customers/index.blade.php
		'customer_list' => array('Customer List', '用户目录'),

		//customers/show.blade.php
		'customer_profile' => array('Customer Profile', '用户资料'),
		'comments' => array('Comments', '评论'),
		'update_to_master' => array('Update to Master', '更新到大神'),
		'customer_photo' => array('Customer Photo', '大神 照片'),
		'customer_id' => array('Customer ID', '大神 ID'),
		'customer_user_name' => array('Customer User Name', '用户 昵称'),
		'customer_real_name' => array('Customer Real Name', '用户 姓名'),
		'customer_phone' => array('Customer Phone', '用户 手机号'),
		'customer_email' => array('Customer Email', '用户 邮箱'),
		'customer_stock' => array('Customer Stock', '用户 余额'),
		'customer_card' => array('Customer\' Card', '用户身份证'),
		'customer_video_audio' => array('Customer Video/Audio', '用户视频/音频'),
		'customer_video' => array('Customer Video', '用户视频'),
		'customer_audio' => array('Customer Audio', '用户音频'),
		'customer_location' => array('Customer Location', '用户地点'),
		'customer_comment' => array('Customer Comment', '用户评论'),
		'title' => array('Title', '标题'),
		'description' => array('Description', '描述'),
		'news_image' => array('News Image', '动态图片'),
		'favorites' => array('Favorites', '赞同'),
		'news_delete_message' => array('Do you really want to delete this news?', '您真想把此动态要删除？'),
		'comments_delete_message' => array('Do you really want to delete this comment?', '您真想把此评论要删除？'),
		'customer_status' => array('Customer Status', '用户状态'),
		'customer_active_message' => array('If user is not activated, mail will be sent to user with activation link', '如果用户还没有激活，邮件跟激活连接一起将被发送到他的邮箱。'),

		//advertise/advertise.blade.php
		'advertises' => array('Advertises', '广告'),
		'top_advertises' => array('Top Advertises', '顶部广告'),
		'middle_advertises' => array('Middle Advertises', '中部广告'),
		'bottom_advertises' => array('Bottom Advertises', '底部广告'),
		'advertise_image' => array('Advertise Image', '广告图片'),
		'advertise_title' => array('Advertise Title', '广告标题'),
		'advertise_weburl' => array('Advertise WebURL', '广告连接'),
		'advertise_expired_date' => array('Expired Date', '期满日期'),
		'advertise_delete_message' => array('Do you really want to delete this advertise?', '您真想把此广告要删除？'),
		'advertise_active_message' => array('Do you really want to active this advertise?', '您真想把此广告要激活？'),
		'advertise_inactive_message' => array('Do you really want to inactive this advertise?', '您真想把此广告要不激活？'),

		//advertise/create.blade.php
		'add_advertises' => array('Add Advertises', '添加广告'),
		'advertise_description' => array('Advertise Description', '广告描述'),
		'advertise_photo' => array('Advertise Photo', '广告图片'),

		//advertise/show.blade.php
		'advertise_detail' => array('Advertise Detail', '广告资料'),

		//advertise/edit.blade.php
		'edit_advertise' => array('Edit Advertise', '广告编辑'),
		'update_advertise' => array('Update Advertise', '广告编辑'),

		//news/news.blade.php
		'user_name' => array('User Name', '用户名称'),
		'news_delete_message' => array('Do you really want to delete this news?', '您真想把此动态要删除？'),
		'news_inpute_message' => array('Please input a news name.', '请输入动态名称。'),
		'news_active_message' => array('Do you really want to active this news?', '您真想把此动态要激活？'),
		'news_inactive_message' => array('Do you really want to inactive this news?', '您真想把此动态要不激活？'),

		//news/show.blade.php
		'news_detail' => array('News Detail', '动态明细'),
		'news_comments' => array('News Comments', '动态评论'),
		'news_title' => array('News Title', '动态标题'),
		'news_description' => array('News Description', '动态描述'),
		'news_photo' => array('News Photo', '动态照片'),
		'user_photo' => array('User Photo', '动态照片'),
		'comments_delete_message' => array('Do you really want to delete this comments?', '您真想把此评论要删除？'),

		//public/assets/vendors/datatables/js/jquery.dataTables.js
		'showing' => array('Showing', '显示'),


		//notification/notification.blade.php
		'notification_title' => array('Notification Title', '公告标题'),
		'notification_description' => array('Notification Description', '公告描述'),
		'notification_delete_message' => array('Do you really want to delete this notification?', '您真想把此公告要删除？'),
		'notification_inpute_message' => array('Please input a notification.', '请输入公告。'),
		'notification_active_message' => array('Do you really want to active this notification?', '您真想把此公告要激活？'),
		'notification_inactive_message' => array('Do you really want to inactive this notification?', '您真想把此公告要不激活？'),

		//notification/notification.blade.php
		'add_notification' => array('Add Notification', '添加公告'),

		//notification/edit.blade.php
		'edit_notification' => array('Edit Notification', '编辑公告'),

		//notification/show.blade.php
		'notification_detail' => array('Notification Detail', '公告详细'),

		//groups/catgroups.blade.php
		'group_master' => array('Group Master', '群组创建人'),
		'group_title' => array('Group Title', '群组标题'),
		'group_description' => array('Group Description', '群组描述'),
		'group_staffs' => array('Group Staffs', '群组成员'),
		'category_group_delete_message' => array('Do you really want to delete this category group?', '您真想把此项目群组要删除？'),

		//groups/chatgroups.blade.php
		'group_type' => array('Group Type', '群组种类'),
		'chat_group_delete_message' => array('Do you really want to delete this chat group?', '您真想把此聊天群组要删除？'),

		//city/city.blade.php
		'city_name' => array('City Name', '城市名称'),
		'add_city' => array('Add City', '添加城市'),
		'city_delete_message' => array('Do you really want to delete this city?', '您真想把此城市要删除？'),
		'city_inpute_message' => array('Please input a city.', '请输入城市。'),
		'city_active_message' => array('Do you really want to active this city?', '您真想把此城市要激活？'),
		'city_inactive_message' => array('Do you really want to inactive this city?', '您真想把此城市要不激活？'),
		'notice' => array('Notice!!!', '注意!!!'),

		//vip/vip.blade.php
		'vip_list' => array('VIP List', 'VIP 目录'),
		'vip_name' => array('VIP Name', 'VIP 名字'),
		'low_point' => array('Low Point', 'VIP 经验值(从)'),
		'high_point' => array('High Point', 'VIP 经验值(到)'),
		'add_vip' => array('Add VIP', '添加 VIP'),
		'vip_delete_message' => array('Do you really want to delete this VIP?', '您真想把此VIP要删除？'),
		'vip_inpute_message' => array('Please input a VIP Name.', '请输入VIP 名字。'),
		'vip_active_message' => array('Do you really want to active this VIP?', '您真想把此VIP要激活？'),
		'vip_inactive_message' => array('Do you really want to inactive this VIP?', '您真想把此VIP要不激活？'),

		//vip/subprice.blade.php
		'add_subscribe_price' => array('Add Subscribe Price', '添加充值'),
		'subscribe_price_delete_message' => array('Do you really want to delete this subscribe_price?', '您真想把此充值要删除？'),
		'subscribe_price_inpute_message' => array('Please input a subscribe_price.', '请输入充值名称。'),
		'subscribe_price_active_message' => array('Do you really want to active this subscribe_price?', '您真想把此充值要激活？'),
		'subscribe_price_inactive_message' => array('Do you really want to inactive this subscribe_price?', '您真想把此充值要不激活？'),


		//tag/tag.blade.php
		'tag_name' => array('Tag Name', '兴趣名称'),
		'add_tag' => array('Add Tag', '添加兴趣'),
		'tag_delete_message' => array('Do you really want to delete this tag?', '您真想把此兴趣要删除？'),
		'tag_inpute_message' => array('Please input a tag name.', '请输入兴趣名称。'),
		'tag_active_message' => array('Do you really want to active this tag?', '您真想把此兴趣要激活？'),
		'tag_inactive_message' => array('Do you really want to inactive this tag?', '您真想把此兴趣要不激活？'),

		//business/business.blade.php
		'business_option_name' => array('Business Option Name', '职业名称'),
		'add_business_option' => array('Add Business Option', '添加职业'),
		'business_option_delete_message' => array('Do you really want to delete this business_option?', '您真想把此职业要删除？'),
		'business_option_inpute_message' => array('Please input a business_option.', '请输入职业名称。'),
		'business_option_active_message' => array('Do you really want to active this business_option?', '您真想把此职业要激活？'),
		'business_option_inactive_message' => array('Do you really want to inactive this business_option?', '您真想把此职业要不激活？'),

		//Http/Controllers/UserMasterController.php
		'to_master' => array('to Master', '到大神'),
		'to_customer' => array('to Customer', '到用户'),
		'inactive' => array('inactive', '不激活'),
		'active' => array('active', '激活'),
		'activated' => array('Activated', '已激活'),
		'pending' => array('Pending', '在等待'),
		'high_master' => array('high Master', '高级大神'),
		'reset' => array('Reset', '重置'),
		'back' => array('Back', '返回'),
		// login page

		//
	
];
