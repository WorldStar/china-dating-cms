<ul id="menu" class="page-sidebar-menu">
    <li {!! (Request::is('admin') ? 'class="active"' : '') !!}>
        <a href="{{ route('dashboard') }}">
            <i class="livicon" data-name="home" data-size="18" data-c="#418BCA" data-hc="#418BCA"
               data-loop="true"></i>
            <span class="title">{{ config('Convert.dashboard')[$_SESSION['lang']] }}</span>
        </a>
    </li>
    <?php
    $role_id = DB::table('role_users')->where('user_id', Sentinel::getuser()->id)->first()->role_id;
    if($role_id == 1){
    ?>
    <li {!! (Request::is('admin/users') || Request::is('admin/users/create') || Request::is('admin/user_profile') || Request::is('admin/users/*') || Request::is('admin/deleted_users') ? 'class="active"' : '') !!}>
        <a href="#">
            <i class="livicon" data-name="user" data-size="18" data-c="#e9573f" data-hc="#e9573f"
               data-loop="true"></i>
            <span class="title">{{ config('Convert.admins')[$_SESSION['lang']] }}</span>
            <span class="fa arrow"></span>
        </a>
        <ul class="sub-menu">
            <li {!! (Request::is('admin/users') ? 'class="active" id="active"' : '') !!}>
                <a href="{{ URL::to('admin/users') }}">
                    <i class="fa fa-angle-double-right"></i>
                    {{ config('Convert.admins')[$_SESSION['lang']] }}
                </a>
            </li>
            <li {!! (Request::is('admin/users/create') ? 'class="active" id="active"' : '') !!}>
                <a href="{{ URL::to('admin/users/create') }}">
                    <i class="fa fa-angle-double-right"></i>
                    {{ config('Convert.add_admins')[$_SESSION['lang']] }}
                </a>
            </li>
            <li {!! (Request::is('admin/deleted_users') ? 'class="active" id="active"' : '') !!}>
                <a href="{{ URL::to('admin/deleted_users') }}">
                    <i class="fa fa-angle-double-right"></i>
                    {{ config('Convert.delete_admins')[$_SESSION['lang']] }}
                </a>
            </li>
        </ul>
    </li>
    <?php } ?>
    <li {!! (Request::is('admin/category') || Request::is('admin/subcategory') || Request::is('admin/classic') || Request::is('admin/price') ? 'class="active"' : '') !!}>
        <a href="#">
            <i class="livicon" data-c="#F89A14" data-hc="#F89A14" data-name="list" data-size="18"
               data-loop="true"></i>
            {{ config('Convert.categories')[$_SESSION['lang']] }}
            <span class="fa arrow"></span>
        </a>
        <ul class="sub-menu">
            <li {!! (Request::is('admin/category') ? 'class="active" id="active"' : '') !!}>
                <a href="{{ URL::to('/admin/category') }}">
                    <i class="fa fa-angle-double-right"></i>
                    {{ config('Convert.categories')[$_SESSION['lang']] }}
                </a>
            </li>
            <li {!! (Request::is('admin/subcategory') ? 'class="active" id="active"' : '') !!}>
                <a href="{{ URL::to('/admin/subcategory') }}">
                    <i class="fa fa-angle-double-right"></i>
                    {{ config('Convert.sub_categories')[$_SESSION['lang']] }}<!--this follow category.-->
                </a>
            </li>
            <li {!! (Request::is('admin/classic') ? 'class="active" id="active"' : '') !!}>
                <a href="{{ URL::to('admin/classic') }}">
                    <i class="fa fa-angle-double-right"></i>
                    {{ config('Convert.classices')[$_SESSION['lang']] }}  <!--this follow subcategory.-->
                </a>
            </li>
            <li {!! (Request::is('admin/price') ? 'class="active" id="active"' : '') !!}>
                <a href="{{ URL::to('admin/price') }}">
                    <i class="fa fa-angle-double-right"></i>
                    {{ config('Convert.prices')[$_SESSION['lang']] }} <!--this follow subcategory.-->
                </a>
            </li>
        </ul>
    </li>
    <li {!! (Request::is('admin/masters') || Request::is('admin/masters/create') || Request::is('admin/master_profile') || Request::is('admin/masters/*') || Request::is('admin/deleted_masters') || Request::is('admin/usermaster') ? 'class="active"' : '') !!}>
        <a href="#">
            <i class="livicon" data-name="user" data-size="18" data-c="#6CC66C" data-hc="#6CC66C"
               data-loop="true"></i>
            <span class="title">{{ config('Convert.masters')[$_SESSION['lang']] }}</span>
            <span class="fa arrow"></span>
        </a>
        <ul class="sub-menu">
            <li {!! (Request::is('admin/masters') ? 'class="active" id="active"' : '') !!}>
                <a href="{{ URL::to('admin/masters') }}">
                    <i class="fa fa-angle-double-right"></i>
                    {{ config('Convert.masters')[$_SESSION['lang']] }}
                </a>
            </li>
            <li {!! (Request::is('admin/deleted_masters') ? 'class="active" id="active"' : '') !!}>
                <a href="{{ URL::to('admin/deleted_masters') }}">
                    <i class="fa fa-angle-double-right"></i>
                    {{ config('Convert.delete_masters')[$_SESSION['lang']] }}
                </a>
            </li>
        </ul>
    </li>
    <li {!! (Request::is('admin/usermaster') ? 'class="active"' : '') !!}>
        <a href="{{ URL::to('admin/usermaster') }}">
            <i class="livicon" data-name="address-book" data-size="18" data-c="#418BCA" data-hc="#418BCA"
               data-loop="true"></i>
            <span class="title">{{ config('Convert.requests_master')[$_SESSION['lang']] }}</span>
        </a>
    </li>
    <li {!! (Request::is('admin/customers') || Request::is('admin/customers/create') || Request::is('admin/customer_profile') || Request::is('admin/customers/*') || Request::is('admin/deleted_customers') ? 'class="active"' : '') !!}>
        <a href="#">
            <i class="livicon" data-name="user" data-size="18" data-c="#418BCA" data-hc="#418BCA"
               data-loop="true"></i>
            <span class="title">{{ config('Convert.customers')[$_SESSION['lang']] }}</span>
            <span class="fa arrow"></span>
        </a>
        <ul class="sub-menu">
            <li {!! (Request::is('admin/customers') ? 'class="active" id="active"' : '') !!}>
                <a href="{{ URL::to('admin/customers') }}">
                    <i class="fa fa-angle-double-right"></i>
                    {{ config('Convert.customers')[$_SESSION['lang']] }}
                </a>
            </li>
            <li {!! (Request::is('admin/deleted_customers') ? 'class="active" id="active"' : '') !!}>
                <a href="{{ URL::to('admin/deleted_customers') }}">
                    <i class="fa fa-angle-double-right"></i>
                    {{ config('Convert.delete_customers')[$_SESSION['lang']] }}
                </a>
            </li>
        </ul>
    </li>
    <li {!! (Request::is('admin/advertise') || Request::is('admin/advertise/create') || Request::is('admin/advertise/*') ? 'class="active"' : '') !!}>
        <a href="#">
            <i class="livicon" data-name="flag" data-size="18" data-c="#e9573f" data-hc="#e9573f"
               data-loop="true"></i>
            <span class="title">{{ config('Convert.advertises')[$_SESSION['lang']] }}</span>
            <span class="fa arrow"></span>
        </a>
        <ul class="sub-menu">
            <li {!! (Request::is('admin/advertise/1') ? 'class="active" id="active"' : '') !!}>
                <a href="{{ URL::to('admin/advertise/1') }}">
                    <i class="fa fa-angle-double-right"></i>
                    {{ config('Convert.top_advertises')[$_SESSION['lang']] }}
                </a>
            </li>
            <li {!! (Request::is('admin/advertise/2') ? 'class="active" id="active"' : '') !!}>
                <a href="{{ URL::to('admin/advertise/2') }}">
                    <i class="fa fa-angle-double-right"></i>
                    {{ config('Convert.middle_advertises')[$_SESSION['lang']] }}
                </a>
            </li>
            <li {!! (Request::is('admin/advertise/3') ? 'class="active" id="active"' : '') !!}>
                <a href="{{ URL::to('admin/advertise/3') }}">
                    <i class="fa fa-angle-double-right"></i>
                    {{ config('Convert.bottom_advertises')[$_SESSION['lang']] }}
                </a>
            </li>
        </ul>
    </li>
    <li {!! (Request::is('admin/notification') ? 'class="active"' : '') !!}>
        <a href="#">
            <i class="livicon" data-name="wifi-alt" data-size="18" data-c="#F89A14" data-hc="#F89A14"
               data-loop="true"></i>
            <span class="title">{{ config('Convert.notifications')[$_SESSION['lang']] }}</span>
            <span class="fa arrow"></span>
        </a>
        <ul class="sub-menu">
            <li {!! (Request::is('admin/notification') ? 'class="active" id="active"' : '') !!}>
                <a href="{{ URL::to('admin/notification') }}">
                    <i class="fa fa-angle-double-right"></i>
                    {{ config('Convert.site_notifications')[$_SESSION['lang']] }}
                </a>
            </li>
        </ul>
    </li>
    <li {!! (Request::is('admin/catgroups') || Request::is('admin/chatgroups')? 'class="active"' : '') !!}>
        <a href="#">
            <i class="livicon" data-name="sitemap" data-size="18" data-c="#6CC66C" data-hc="#6CC66C"
               data-loop="true"></i>
            <span class="title">{{ config('Convert.groups')[$_SESSION['lang']] }}</span>
            <span class="fa arrow"></span>
        </a>
        <ul class="sub-menu">
            <li {!! (Request::is('admin/catgroups') ? 'class="active" id="active"' : '') !!}>
                <a href="{{ URL::to('admin/catgroups') }}">
                    <i class="fa fa-angle-double-right"></i>
                    {{ config('Convert.category_groups')[$_SESSION['lang']] }}
                </a>
            </li>
        </ul>
        <ul class="sub-menu">
            <li {!! (Request::is('admin/chatgroups') ? 'class="active" id="active"' : '') !!}>
                <a href="{{ URL::to('admin/chatgroups') }}">
                    <i class="fa fa-angle-double-right"></i>
                    {{ config('Convert.chat_groups')[$_SESSION['lang']] }}
                </a>
            </li>
        </ul>
    </li>
    <li {!! (Request::is('admin/news') || Request::is('admin/advertise/create') || Request::is('admin/advertise/*') ? 'class="active"' : '') !!}>
        <a href="#">
            <i class="livicon" data-name="address-book" data-size="18" data-c="#F89A14" data-hc="#F89A14"
               data-loop="true"></i>
            <span class="title">{{ config('Convert.news')[$_SESSION['lang']] }}</span>
            <span class="fa arrow"></span>
        </a>
        <ul class="sub-menu">
            <li {!! (Request::is('admin/news') ? 'class="active" id="active"' : '') !!}>
                <a href="{{ URL::to('admin/news') }}">
                    <i class="fa fa-angle-double-right"></i>
                    {{ config('Convert.news')[$_SESSION['lang']] }}
                </a>
            </li>
            <li {!! (Request::is('admin/comments') ? 'class="active" id="active"' : '') !!}>
                <a href="{{ URL::to('admin/comments/0') }}">
                    <i class="fa fa-angle-double-right"></i>
                    {{ config('Convert.comments')[$_SESSION['lang']] }}
                </a>
            </li>
        </ul>
    </li>
    <li {!! (Request::is('admin/filter') || Request::is('admin/filter/*') ? 'class="active"' : '') !!}>
        <a href="#">
            <i class="livicon" data-name="sun" data-size="18" data-c="#e9573f" data-hc="#e9573f"
               data-loop="true"></i>
            <span class="title">Filters</span>
            <span class="fa arrow"></span>
        </a>
        <ul class="sub-menu">
            <li {!! (Request::is('admin/filter/1') ? 'class="active" id="active"' : '') !!}>
                <a href="{{ URL::to('admin/filter/1') }}">
                    <i class="fa fa-angle-double-right"></i>
                    Master Filter
                </a>
            </li>
            <li {!! (Request::is('admin/filter/2') ? 'class="active" id="active"' : '') !!}>
                <a href="{{ URL::to('admin/filter/2') }}">
                    <i class="fa fa-angle-double-right"></i>
                    Customer Filter
                </a>
            </li>
            <li {!! (Request::is('admin/filter/3') ? 'class="active" id="active"' : '') !!}>
                <a href="{{ URL::to('admin/filter/3') }}">
                    <i class="fa fa-angle-double-right"></i>
                    Payment Filter
                </a>
            </li>
            <li {!! (Request::is('admin/filter/4') ? 'class="active" id="active"' : '') !!}>
                <a href="{{ URL::to('admin/filter/4') }}">
                    <i class="fa fa-angle-double-right"></i>
                    Transaction Filter
                </a>
            </li>
            <li {!! (Request::is('admin/filter/5') ? 'class="active" id="active"' : '') !!}>
                <a href="{{ URL::to('admin/filter/5') }}">
                    <i class="fa fa-angle-double-right"></i>
                    Live Filter
                </a>
            </li>
            <li {!! (Request::is('admin/filter/6') ? 'class="active" id="active"' : '') !!}>
                <a href="{{ URL::to('admin/filter/6') }}">
                    <i class="fa fa-angle-double-right"></i>
                    Chat Filter
                </a>
            </li>
        </ul>
    </li>
    <li {!! (Request::is('admin/activities') ? 'class="active"' : '') !!}>
        <a href="{{ URL::to('admin/activities') }}">
            <i class="livicon" data-name="alarm" data-size="18" data-c="#418BCA" data-hc="#418BCA"
               data-loop="true"></i>
            <span class="title">Activities</span>
        </a>
    </li>

    <li {!! (Request::is('admin/charts') || Request::is('admin/piecharts') || Request::is('admin/charts_animation') || Request::is('admin/jscharts') || Request::is('admin/sparklinecharts') ? 'class="active"' : '') !!}>
        <a href="#">
            <i class="livicon" data-name="barchart" data-size="18" data-c="#6CC66C" data-hc="#6CC66C"
               data-loop="true"></i>
            <span class="title">{{ config('Convert.reports')[$_SESSION['lang']] }}</span>
            <span class="fa arrow"></span>
        </a>
        <ul class="sub-menu">
            <li {!! (Request::is('admin/charts') ? 'class="active"' : '') !!}>
                <a href="{{ URL::to('admin/reports/charts') }}">
                    <i class="fa fa-angle-double-right"></i>
                    {{ config('Convert.sales_report')[$_SESSION['lang']] }}
                </a>
            </li>
            <li {!! (Request::is('admin/piecharts') ? 'class="active"' : '') !!}>
                <a href="{{ URL::to('admin/reports/piecharts') }}">
                    <i class="fa fa-angle-double-right"></i>
                    {{ config('Convert.user_report')[$_SESSION['lang']] }}
                </a>
            </li>
            <li {!! (Request::is('admin/charts_animation') ? 'class="active"' : '') !!}>
                <a href="{{ URL::to('admin/reports/charts_animation') }}">
                    <i class="fa fa-angle-double-right"></i>
                    {{ config('Convert.activity_report')[$_SESSION['lang']] }}
                </a>
            </li>
        </ul>
    </li>

    <li {!! (Request::is('admin/vip') || Request::is('admin/tag') || Request::is('admin/city') || Request::is('admin/subprice') ? 'class="active"' : '') !!}>
        <a href="#">
            <i class="livicon" data-name="gear" data-size="18" data-c="#418BCA" data-hc="#6CC66C"
               data-loop="true"></i>
            <span class="title">{{ config('Convert.settings')[$_SESSION['lang']] }}</span>
            <span class="fa arrow"></span>
        </a>
        <ul class="sub-menu">
            <li {!! (Request::is('admin/city') ? 'class="active" id="active"' : '') !!}>
                <a href="{{ URL::to('admin/city') }}">
                    <i class="fa fa-angle-double-right"></i>
                    {{ config('Convert.cities')[$_SESSION['lang']] }}
                </a>
            </li>
            <li {!! (Request::is('admin/vip') ? 'class="active" id="active"' : '') !!}>
                <a href="{{ URL::to('admin/vip') }}">
                    <i class="fa fa-angle-double-right"></i>
                    {{ config('Convert.vips')[$_SESSION['lang']] }}
                </a>
            </li>
            <li {!! (Request::is('admin/tag') ? 'class="active" id="active"' : '') !!}>
                <a href="{{ URL::to('admin/tag') }}">
                    <i class="fa fa-angle-double-right"></i>
                    {{ config('Convert.tags')[$_SESSION['lang']] }}
                </a>
            </li>
            <li {!! (Request::is('admin/subprice') ? 'class="active" id="active"' : '') !!}>
                <a href="{{ URL::to('admin/subprice') }}">
                    <i class="fa fa-angle-double-right"></i>
                    {{ config('Convert.subscribe_prices')[$_SESSION['lang']] }}
                </a>
            </li>
            <li {!! (Request::is('admin/business') ? 'class="active" id="active"' : '') !!}>
                <a href="{{ URL::to('admin/business') }}">
                    <i class="fa fa-angle-double-right"></i>
                    {{ config('Convert.business_options')[$_SESSION['lang']] }}
                </a>
            </li>
            <li {!! (Request::is('admin/thirdparty') ? 'class="active" id="active"' : '') !!}>
                <a href="{{ URL::to('admin/thirdparty') }}">
                    <i class="fa fa-angle-double-right"></i>
                    Third Party
                </a>
            </li>
            <!--<li {!! (Request::is('admin/business') ? 'class="active" id="active"' : '') !!}>
                <a href="{{ URL::to('admin/business') }}">
                    <i class="fa fa-angle-double-right"></i>
                    Business Options
                </a>
            </li>
            <li {!! (Request::is('admin/pointtype') ? 'class="active" id="active"' : '') !!}>
                <a href="{{ URL::to('admin/pointtype') }}">
                    <i class="fa fa-angle-double-right"></i>
                    Point Types
                </a>
            </li>-->
        </ul>
    </li>
</ul>