@extends('admin/layouts/default')

{{-- Page title --}}
@section('title')
    Activities
    @parent
@stop

{{-- page level styles --}}
@section('header_styles')
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/dataTables.bootstrap.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/buttons.bootstrap.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/colReorder.bootstrap.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/dataTables.bootstrap.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/rowReorder.bootstrap.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/buttons.bootstrap.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/scroller.bootstrap.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/pages/tables.css') }}" />
@stop


{{-- Page content --}}
@section('content')
    <section class="content-header">
        <h1>Activities</h1>
        <ol class="breadcrumb">
            <li>
                <a href="{{ route('dashboard') }}">
                    <i class="livicon" data-name="home" data-size="14" data-color="#000"></i>
                    {{ config('Convert.dashboard')[$_SESSION['lang']] }}
                </a>
            </li>
            <li><a href="#"> Activity</a></li>
            <li class="active">Activities</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content paddingleft_right15">
        <div class="row">
            <div class="panel panel-primary ">
                <div class="panel-heading">
                    <h4 class="panel-title"> <i class="livicon" data-name="user" data-size="16" data-loop="true" data-c="#fff" data-hc="white"></i>
                        Activities
                    </h4>
                </div>
                <br />
                <div class="panel-body">
                    <div class="row">
                        <div class="col-lg-3">
                            &nbsp;
                        </div>
                        <div class="col-lg-9">
                            {!! Form::open(['url'=>url('/admin/activities'),'class'=>'masterfrom', 'id'=>'masterfrom']) !!}
                            <?php echo Form::select('option_id', $options, $option_id, array('class'=>'form-control', 'style'=>'width:150px;float:left;margin-right:10px;', 'onchange'=>'onChange()', 'name'=>'option_id', 'size'=>'1', 'id' => 'option_id')); ?>
                            <?php echo Form::select('cat_id', $categories, $cat_id, array('class'=>'form-control', 'style'=>'width:150px;float:left;margin-right:10px;', 'onchange'=>'onChange()', 'name'=>'cat_id', 'size'=>'1', 'id' => 'cat_id')); ?>
                            <?php echo Form::select('subcat_id', $subcategories, $subcat_id, array('class'=>'form-control', 'style'=>'width:150px;float:left;margin-right:10px;', 'onchange'=>'onChange()', 'name'=>'subcat_id', 'size'=>'1', 'id' => 'subcat_id')); ?>
                            <?php echo Form::select('vip_id', $vips, $vip_id, array('class'=>'form-control', 'style'=>'width:150px;float:left;margin-right:10px;', 'onchange'=>'onChange()', 'name'=>'vip_id', 'size'=>'1', 'id' => 'vip_id')); ?>
                            {!! Form::close() !!}
                        </div>
                    </div>
                    <div class="panel panel-primary filterable" style="background-color: transparent !important;">

                        <div class="panel-body table-responsive">
                            <table class="table table-striped table-bordered" id="table1">
                                <thead>
                                <tr>
                                    <th>NO</th>
                                    <th>User ID</th>
                                    <th>User Photo</th>
                                    <th>User Name</th>
                                    <th>User VIP</th>
                                    <th>User Category</th>
                                    <th>Content</th>
                                    <th>Date/Time</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td>1</td>
                                    <td>2345678</td>
                                    <td>
                                        <img src="/uploads/users/noimage.png" style="max-height:50px;border-radius:50%;">
                                    </td>
                                    <td>star1</td>
                                    <td>VIP1</td>
                                    <td><img src="/uploads/categories/GwPk3K6sxg.png" style="max-height:20px;margin-right:10px;border-radius:50%;">20/时<br><img src="/uploads/categories/Fg9fS2HaKC.png" style="max-height:20px;margin-right:10px;border-radius:50%;">40/时<br></td>
                                    <td>Open Live Broadcast on 情感咨询∙声优聊天</td>
                                    <td>2016-12-26 11:10:10</td>
                                </tr>
                                <tr>
                                    <td>2</td>
                                    <td>2345678</td>
                                    <td>
                                        <img src="/uploads/users/noimage.png" style="max-height:50px;border-radius:50%;">
                                    </td>
                                    <td>star1</td>
                                    <td>VIP1</td>
                                    <td><img src="/uploads/categories/GwPk3K6sxg.png" style="max-height:20px;margin-right:10px;border-radius:50%;">20/时<br><img src="/uploads/categories/Fg9fS2HaKC.png" style="max-height:20px;margin-right:10px;border-radius:50%;">40/时<br></td>
                                    <td>Updated own profile</td>
                                    <td>2016-12-26 11:10:10</td>
                                </tr>
                                <tr>
                                    <td>3</td>
                                    <td>2345678</td>
                                    <td>
                                        <img src="/uploads/users/noimage.png" style="max-height:50px;border-radius:50%;">
                                    </td>
                                    <td>star1</td>
                                    <td>VIP1</td>
                                    <td><img src="/uploads/categories/GwPk3K6sxg.png" style="max-height:20px;margin-right:10px;border-radius:50%;">20/时<br><img src="/uploads/categories/Fg9fS2HaKC.png" style="max-height:20px;margin-right:10px;border-radius:50%;">40/时<br></td>
                                    <td>Tried to log in</td>
                                    <td>2016-12-26 11:10:10</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>    <!-- row-->
    </section>
@stop

{{-- page level scripts --}}
@section('footer_scripts')
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/jquery.dataTables.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.bootstrap.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.buttons.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.colReorder.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.responsive.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.rowReorder.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/buttons.colVis.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/buttons.html5.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/buttons.print.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/buttons.bootstrap.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/buttons.print.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/pdfmake.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/vfs_fonts.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.scroller.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/js/pages/table-advanced.js') }}" ></script>


    <script>
        $(function () {

            $('body').on('hidden.bs.modal', '.modal', function () {
                $(this).removeData('bs.modal');
            });
        });
        function onChange(){
            console.log('aaaaa');
            $('#masterfrom').submit();
        }
    </script>
@stop
