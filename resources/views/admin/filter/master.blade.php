@extends('admin/layouts/default')

{{-- Page title --}}
@section('title')
    Master Filter
    @parent
@stop

{{-- page level styles --}}
@section('header_styles')
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/dataTables.bootstrap.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/buttons.bootstrap.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/colReorder.bootstrap.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/dataTables.bootstrap.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/rowReorder.bootstrap.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/buttons.bootstrap.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/scroller.bootstrap.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/pages/tables.css') }}" />
@stop


{{-- Page content --}}
@section('content')
    <section class="content-header">
        <h1>Master Filter</h1>
        <ol class="breadcrumb">
            <li>
                <a href="{{ route('dashboard') }}">
                    <i class="livicon" data-name="home" data-size="14" data-color="#000"></i>
                    {{ config('Convert.dashboard')[$_SESSION['lang']] }}
                </a>
            </li>
            <li><a href="#"> Master Filter</a></li>
            <li class="active">Master Filter</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content paddingleft_right15">
        <div class="row">
            <div class="panel panel-primary ">
                <div class="panel-heading">
                    <h4 class="panel-title"> <i class="livicon" data-name="user" data-size="16" data-loop="true" data-c="#fff" data-hc="white"></i>
                        Master Filter
                    </h4>
                </div>
                <br />
                <div class="panel-body">
                    <div class="row">
                        <div class="col-lg-6">
                            &nbsp;
                        </div>
                        <div class="col-lg-6">
                            {!! Form::open(['url'=>url('/admin/filter/1'),'class'=>'masterfrom', 'id'=>'masterfrom']) !!}
                            <?php echo Form::select('cat_id', $categories, $cat_id, array('class'=>'form-control', 'style'=>'width:150px;float:left;margin-right:10px;', 'onchange'=>'onChange()', 'name'=>'cat_id', 'size'=>'1', 'id' => 'cat_id')); ?>
                            <?php echo Form::select('subcat_id', $subcategories, $subcat_id, array('class'=>'form-control', 'style'=>'width:150px;float:left;margin-right:10px;', 'onchange'=>'onChange()', 'name'=>'subcat_id', 'size'=>'1', 'id' => 'subcat_id')); ?>
                            <?php echo Form::select('vip_id', $vips, $vip_id, array('class'=>'form-control', 'style'=>'width:150px;float:left;margin-right:10px;', 'onchange'=>'onChange()', 'name'=>'vip_id', 'size'=>'1', 'id' => 'vip_id')); ?>
                            {!! Form::close() !!}
                        </div>
                    </div>
                    <div class="panel panel-primary filterable" style="background-color: transparent !important;">

                        <div class="panel-body table-responsive">
                            <table class="table table-striped table-bordered" id="table1">
                                <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>User Photo</th>
                                    <th>User Name</th>
                                    <th>Real Name</th>
                                    <th>Phone</th>
                                    <th>VIP</th>
                                    <th>Category</th>
                                    <th>Stock Amount</th>
                                    <th>Integration Amount</th>
                                    <th>Coupon Amount</th>
                                    <th>News</th>
                                    <th>Favorite</th>
                                    <th>Comment</th>
                                    <th>City</th>
                                    <th>Email</th>
                                    <th>Gender</th>
                                    <th>Birthday</th>
                                    <th>Online</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                //print_r($customers);
                                $i = 0;

                                $users = DB::table('users')
                                        ->join('role_users', 'role_users.user_id', '=', 'users.id')
                                        ->leftJoin('date_user_details', 'date_user_details.user_id', '=', 'users.id')
                                        ->leftJoin('date_user_pockets', 'date_user_pockets.user_id', '=', 'users.id')
                                        ->select(['date_user_pockets.stock_amount', 'date_user_pockets.coupon_amount', 'date_user_pockets.integration_amount', 'date_user_details.vip_id', 'users.id', 'users.userno', 'users.first_name', 'users.username', 'users.email1', 'users.pic', 'users.gender', 'users.city', 'users.online', 'users.dob', 'users.phone1', 'users.weisin_id', 'users.qq_id', 'users.postal','users.created_at']);
                                if($vip_id != 0)
                                    $users = $users->where('date_user_details.vip_id', $vip_id);
                                $users = $users->where('role_users.role_id', 3)->orderby('id', 'desc')->get();
                                foreach($users as $user){
                                    $pic = '';
                                    if($user->pic == null || $user->pic == ''){
                                        $pic = 'noimage.png';
                                    }else{
                                        $pic = $user->pic;
                                    }
                                    $vip = DB::table('date_vips')->where('id', $user->vip_id)->first();
                                    $vipname = '';
                                    if(!empty($vip))$vipname = $vip->name;
                                    $catflg = 0;
                                    $masters = DB::table('date_user_masters')->where('user_id', $user->id)->get();
                                        $categoryname = '';
                                        foreach($masters as $master){
                                            $str = '';
                                            $cat_id1 = $master->cat_id;
                                            $subcat_id1 = $master->subcat_id;
                                            echo $cat_id1.":";
                                            if($subcat_id == 0){
                                                if($cat_id1 == $cat_id){
                                                    $catflg = 1;
                                                }
                                            }else{
                                                if($cat_id1 == $cat_id && $subcat_id1 == $subcat_id){
                                                    $catflg = 1;
                                                }
                                            }
                                            $price = $master->price;
                                            $category = DB::table('date_category')->where('id', $cat_id1)->first();
                                            $catname = '';
                                            if(!empty($category))$catname = $category->name;
                                            $subcategory = DB::table('date_subcategory')->where('cat_id', $cat_id1)->where('id', $subcat_id1)->first();
                                            $subcatname = '';
                                            $icon = '';
                                            if(!empty($subcategory)){
                                                $subcatname = $subcategory->name;
                                                $icon = $subcategory->icon;
                                                if($icon != ''){
                                                    $str .= '<img src="/uploads/categories/'.$icon.'" style="max-height:20px;margin-right:10px;border-radius:50%;">';
                                                }
                                                $str .= $price.'/时<br>';
                                            }
                                            $categoryname .= $str;
                                        }
                                        if(($subcat_id != 0 || $cat_id != 0) && $catflg == 0) continue;
                                        $newsnum = DB::table('date_news')->where('user_id', $user->id)->where('status', 0)->count();
                                        $favonum = DB::table('date_news_favorites')->where('user_id', $user->id)->where('status', 0)->sum('favo_num');
                                        $commentnum = DB::table('date_news_comments')->where('user_id', $user->id)->where('status', 0)->count();
                                        $online = '';
                                        if($user->online == 1){
                                            $online = '/img/online.png';
                                            $alt = 'Online';
                                        }else{
                                            $online = '/img/offline.png';
                                            $alt = 'Offline';
                                        }

                                ?>
                                <tr>
                                    <td><a href="/admin/masters/{{ $user->id }}" style="text-decoration: none">{{ $user->userno }}</a></td>
                                    <td><img src="/uploads/users/{{ $pic }}" style="max-height:50px;border-radius:50%;"></td>
                                    <td>{{ $user->username }}</td>
                                    <td>{{ $user->first_name }}</td>
                                    <td>{{ $user->phone1 }}</td>
                                    <td>{{ $vipname }}</td>
                                    <td>{!! $categoryname !!}</td>
                                    <td>{!! $user->stock_amount !!}元</td>
                                    <td>{!! $user->integration_amount !!}</td>
                                    <td>{!! $user->coupon_amount !!}</td>
                                    <td>{!! $newsnum !!}</td>
                                    <td>{!! $favonum !!}</td>
                                    <td>{!! $commentnum !!}</td>
                                    <td>{!! $user->city !!}</td>
                                    <td>{!! $user->email1 !!}</td>
                                    <td>{!! $user->gender !!}</td>
                                    <td>{!! $user->dob !!}</td>
                                    <td>
                                        <img src ="{{ $online }}" style="width:20px;padding-bottom:10px;" title="{{ $alt }}">
                                    </td>

                                </tr>
                                <?php
                                }
                                ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>    <!-- row-->
    </section>
@stop

{{-- page level scripts --}}
@section('footer_scripts')
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/jquery.dataTables.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.bootstrap.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.buttons.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.colReorder.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.responsive.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.rowReorder.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/buttons.colVis.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/buttons.html5.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/buttons.print.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/buttons.bootstrap.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/buttons.print.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/pdfmake.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/vfs_fonts.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.scroller.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/js/pages/table-advanced.js') }}" ></script>


    <script>
        $(function () {

            $('body').on('hidden.bs.modal', '.modal', function () {
                $(this).removeData('bs.modal');
            });
        });
        function onChange(){
            console.log('aaaaa');
            $('#masterfrom').submit();
        }
    </script>
@stop
