@extends('admin/layouts/default')

{{-- Page title --}}
@section('title')
    {{ config('Convert.masters')[$_SESSION['lang']] }}
@parent
@stop

{{-- page level styles --}}
@section('header_styles')
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/dataTables.bootstrap.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/buttons.bootstrap.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/colReorder.bootstrap.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/dataTables.bootstrap.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/rowReorder.bootstrap.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/buttons.bootstrap.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/scroller.bootstrap.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/pages/tables.css') }}" />
@stop


{{-- Page content --}}
@section('content')
<section class="content-header">
    <h1>{{ config('Convert.masters')[$_SESSION['lang']] }}</h1>
    <ol class="breadcrumb">
        <li>
            <a href="{{ route('dashboard') }}">
                <i class="livicon" data-name="home" data-size="14" data-color="#000"></i>
                {{ config('Convert.dashboard')[$_SESSION['lang']] }}
            </a>
        </li>
        <li><a href="#"> {{ config('Convert.masters')[$_SESSION['lang']] }}</a></li>
        <li class="active">{{ config('Convert.masters_list')[$_SESSION['lang']] }}</li>
    </ol>
</section>

<!-- Main content -->
<section class="content paddingleft_right15">
    <div class="row">
        <div class="panel panel-primary ">
            <div class="panel-heading">
                <h4 class="panel-title"> <i class="livicon" data-name="user" data-size="16" data-loop="true" data-c="#fff" data-hc="white"></i>
                    {{ config('Convert.masters_list')[$_SESSION['lang']] }}
                </h4>
            </div>
            <br />
            <div class="panel-body">
                <table class="table table-striped table-bordered table-hover dataTable no-footer sample_editable"
                       id="table" role="grid">
                    <thead>
                    <tr role="row">
                        <th class="sorting_asc"  tabindex="0" aria-controls="paidtable" rowspan="1" te
                            colspan="1"  aria-label="
                                                   ID
                                            : activate to sort column ascending" style="width: 30px;">{{ config('Convert.id')[$_SESSION['lang']] }}
                        </th>
                        <th class="sorting_asc" tabindex="0" aria-controls="paidtable" rowspan="1"
                            colspan="1"  aria-label="
                                                   User Name
                                            : activate to sort column ascending" style="width: 100px;"> {{ config('Convert.customer_name')[$_SESSION['lang']] }}
                        </th>
                        <th class="sorting_asc" tabindex="0" aria-controls="paidtable" rowspan="1"
                            colspan="1"  aria-label="
                                                   First Name
                                            : activate to sort column ascending" style="width: 100px;"> {{ config('Convert.first_name')[$_SESSION['lang']] }}
                        </th>
                        <th class="sorting" tabindex="0" aria-controls="paidtable" rowspan="1"
                            colspan="1" aria-label="
                                                Wisin ID
                                            : activate to sort column ascending" style="width: 100px;">{{ config('Convert.weixin_id')[$_SESSION['lang']] }}
                        </th>
                        <th class="sorting" tabindex="0" aria-controls="paidtable" rowspan="1"
                            colspan="1" aria-label="
                                                QQ ID
                                            : activate to sort column ascending" style="width: 100px;">{{ config('Convert.qq_id')[$_SESSION['lang']] }}
                        </th>
                        <th class="sorting" tabindex="0" aria-controls="paidtable" rowspan="1"
                            colspan="1" aria-label="
                                                Phone
                                            : activate to sort column ascending" style="width: 100px;">{{ config('Convert.phone')[$_SESSION['lang']] }}
                        </th>
                        <th class="sorting" tabindex="0" aria-controls="paidtable" rowspan="1"
                            colspan="1" aria-label="
                                                User E-mail
                                            : activate to sort column ascending" style="width: 100px;">{{ config('Convert.email')[$_SESSION['lang']] }}
                        </th>
                        <th class="sorting" tabindex="0" aria-controls="paidtable" rowspan="1"
                            colspan="1" aria-label="
                                                User Gender
                                            : activate to sort column ascending" style="width: 100px;">{{ config('Convert.gender')[$_SESSION['lang']] }}
                        </th>
                        <th class="sorting" tabindex="0" aria-controls="paidtable" rowspan="1"
                            colspan="1" aria-label="
                                                User Birthday
                                            : activate to sort column ascending" style="width: 100px;">{{ config('Convert.birthday')[$_SESSION['lang']] }}
                        </th>
                        <th class="sorting_asc" tabindex="0" aria-controls="paidtable" rowspan="1"
                            colspan="1"  aria-label="
                                                   Online
                                            : activate to sort column ascending" style="width: 100px;">{{ config('Convert.online')[$_SESSION['lang']] }}
                        </th>
                        <th class="sorting_asc" tabindex="0" aria-controls="paidtable" rowspan="1"
                            colspan="1"  aria-label="
                                                   Status
                                            : activate to sort column ascending" style="width: 100px;">{{ config('Convert.status')[$_SESSION['lang']] }}
                        </th>
                        <th class="sorting" tabindex="0" aria-controls="paidtable" rowspan="1"
                            colspan="1" aria-label="
                                                Created At
                                            : activate to sort column ascending" style="width: 80px;">{{ config('Convert.create_date')[$_SESSION['lang']] }}
                        </th>
                        <th class="sorting_asc" tabindex="0" aria-controls="paidtable" rowspan="1"
                            colspan="1"  aria-label="
                                                   Actions
                                            : activate to sort column ascending" style="width:100px;">{{ config('Convert.actions')[$_SESSION['lang']] }}
                        </th>
                    </tr>
                    </thead>
                    <tbody>

                    </tbody>
                </table>
            </div>
        </div>
    </div>    <!-- row-->
</section>
@stop

{{-- page level scripts --}}
@section('footer_scripts')
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/jquery.dataTables.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.bootstrap.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.buttons.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.colReorder.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.responsive.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.rowReorder.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/buttons.colVis.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/buttons.html5.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/buttons.print.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/buttons.bootstrap.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/buttons.print.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/pdfmake.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/vfs_fonts.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.scroller.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/select2/js/select2.js') }}"></script>

<script>
    $(function() {
        var nEditing = null;
        var table = $('#table').DataTable({
            processing: true,
            serverSide: true,
            ajax: '{!! route('masters.data') !!}',
            columns: [
                { data: 'userno', name: 'userno' },
                { data: 'username', name: 'username' },
                { data: 'first_name', name: 'first_name' },
                { data: 'weisin_id', name: 'weisin_id' },
                { data: 'qq_id', name: 'qq_id' },
                { data: 'phone1', name: 'phone1' },
                { data: 'email1', name: 'email1' },
                { data: 'gender', name: 'gender' },
                { data: 'dob', name: 'dob' },
                { data: 'online', name: 'online' , searchable: false},
                { data: 'status', name: 'status', orderable: false, searchable: false},
                { data: 'created_at', name:'created_at', searchable: false},
                { data: 'actions', name: 'actions', orderable: false, searchable: false }
            ]
        });
        table.on( 'draw', function () {
            $('.livicon').each(function(){
                $(this).updateLivicon();
            });
        } );

    });

</script>

<div class="modal fade" id="delete_confirm" tabindex="-1" role="dialog" aria-labelledby="user_delete_confirm_title" aria-hidden="true">
	<div class="modal-dialog">
    	<div class="modal-content"></div>
  </div>
</div>
<script>
$(function () {
	$('body').on('hidden.bs.modal', '.modal', function () {
		$(this).removeData('bs.modal');
	});
});
</script>
@stop
