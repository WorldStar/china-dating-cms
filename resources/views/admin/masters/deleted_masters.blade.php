@extends('admin.layouts.default')

{{-- Page title --}}
@section('title')
    {{ config('Convert.delete_masters')[$_SESSION['lang']] }}
@parent
@stop

{{-- page level styles --}}
@section('header_styles')

    <link rel="stylesheet" href="{{ asset('assets/vendors/datatables/css/dataTables.bootstrap.css') }}" />
    <link href="{{ asset('assets/css/pages/tables.css') }}" rel="stylesheet" type="text/css" />
    <!-- end page css -->
@stop

{{-- Page content --}}
@section('content')

<section class="content-header">
                <h1>{{ config('Convert.delete_masters')[$_SESSION['lang']] }}</h1>
                <ol class="breadcrumb">
                    <li>
                        <a href="{{ route('dashboard') }}">
                            <i class="livicon" data-name="home" data-size="14" data-color="#000"></i>
                            {{ config('Convert.dashboard')[$_SESSION['lang']] }}
                        </a>
                    </li>
                    <li><a href="#">{{ config('Convert.masters')[$_SESSION['lang']] }}</a> </li>
                    <li class="active">{{ config('Convert.delete_masters')[$_SESSION['lang']] }}</li>
                </ol>
            </section>
            <!-- Main content -->
         <section class="content paddingleft_right15">
             <div class="row">
                 <div class="panel panel-warning">
                     <div class="panel-heading">
                         <h4 class="panel-title">
                             <i class="livicon" data-name="users-remove" data-size="18" data-c="#ffffff" data-hc="#ffffff"></i>
                             {{ config('Convert.delete_masters')[$_SESSION['lang']] }}
                         </h4>
                     </div>
                     <div class="panel-body">
                         <table class="table table-bordered" id="table">
                             <thead>
                             <tr class="filters">
                                 <th>{{ config('Convert.id')[$_SESSION['lang']] }}</th>
                                 <th>{{ config('Convert.customer_name')[$_SESSION['lang']] }}Customer Name</th>
                                 <th>{{ config('Convert.master_real_name')[$_SESSION['lang']] }}</th>
                                 <th>{{ config('Convert.weixin_id')[$_SESSION['lang']] }}</th>
                                 <th>{{ config('Convert.qq_id')[$_SESSION['lang']] }}</th>
                                 <th>{{ config('Convert.phone')[$_SESSION['lang']] }}</th>
                                 <th>{{ config('Convert.email')[$_SESSION['lang']] }}</th>
                                 <th>{{ config('Convert.gender')[$_SESSION['lang']] }}</th>
                                 <th>{{ config('Convert.birthday')[$_SESSION['lang']] }}</th>
                                 <th>{{ config('Convert.create_date')[$_SESSION['lang']] }}</th>
                                 <th>{{ config('Convert.actions')[$_SESSION['lang']] }}</th>
                             </tr>
                             </thead>
                             <tbody>
                             @foreach ($users as $user)
                                 <tr>
                                     <td>{!! $user->id !!}</td>
                                     <td>{!! $user->username !!}</td>
                                     <td>{!! $user->first_name !!}</td>
                                     <td>{!! $user->weisin_id !!}</td>
                                     <td>{!! $user->qq_id !!}</td>
                                     <td>{!! $user->phone1 !!}</td>
                                     <td>{!! $user->email1 !!}</td>
                                     <td>{!! $user->gender !!}</td>
                                     <td>{!! $user->birthday !!}</td>
                                     <td>{!! $user->created_at->diffForHumans() !!}</td>
                                     <td>
                                         <a href="{{ route('restore/master', $user->id) }}"><i class="livicon" data-name="user-flag" data-c="#6CC66C" data-hc="#6CC66C" data-size="18"></i></a>
                                     </td>
                                 </tr>
                             @endforeach

                             </tbody>
                         </table>
                     </div>
                 </div>
             </div>
</section>

        
    @stop

{{-- page level scripts --}}
@section("footer_scripts")
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/jquery.dataTables.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.bootstrap.js') }}" ></script>
    <script>
        $(document).ready(function() {
            $('#table').DataTable();
        });
    </script>
@stop