@extends('admin/layouts/default')

{{-- Page title --}}
@section('title')
    {{ config('Convert.master_profile')[$_SESSION['lang']] }}
@parent
@stop

{{-- page level styles --}}
@section('header_styles')
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/dataTables.bootstrap.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/buttons.bootstrap.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/colReorder.bootstrap.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/dataTables.bootstrap.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/rowReorder.bootstrap.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/buttons.bootstrap.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/scroller.bootstrap.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/pages/tables.css') }}" />
@stop


{{-- Page content --}}
@section('content')

    <section class="content-header">
        <!--section starts-->
        <h1>{{ config('Convert.master_profile')[$_SESSION['lang']] }}</h1>
        <ol class="breadcrumb">
            <li>
                <a href="">
                    <i class="livicon" data-name="home" data-size="14" data-loop="true"></i>
                    {{ config('Convert.dashboard')[$_SESSION['lang']] }}
                </a>
            </li>
            <li>
                <a href="#">{{ config('Convert.masters')[$_SESSION['lang']] }}</a>
            </li>
            <li class="active">{{ config('Convert.master_profile')[$_SESSION['lang']] }}</li>
        </ol>
    </section>

    <div class="panel-body" style="width:90%;margin-left:5%">
        <ul class="nav  nav-tabs ">
            <li class="active">
                <a href="#tab1" data-toggle="tab">
                    <i class="livicon" data-name="user" data-size="16" data-c="#418BCA" data-hc="#418BCA" data-loop="true"></i>
                    {{ config('Convert.master_profile')[$_SESSION['lang']] }}</a>
            </li>
            <li>
                <a href="#tab2" data-toggle="tab">
                    <i class="livicon" data-name="shopping-cart-in" data-size="16" data-loop="true" data-c="#418BCA" data-hc="#418BCA"></i>
                    {{ config('Convert.payment_transactions')[$_SESSION['lang']] }}</a>
            </li>
            <li>
                <a href="#tab3" data-toggle="tab">
                    <i class="livicon" data-name="shopping-cart" data-size="16" data-loop="true" data-c="#418BCA" data-hc="#418BCA"></i>
                    {{ config('Convert.point_transactions')[$_SESSION['lang']] }}</a>
            </li>
            <li>
            <li>
                <a href="#tab4" data-toggle="tab">
                    <i class="livicon" data-name="address-book" data-size="16" data-loop="true" data-c="#418BCA" data-hc="#418BCA"></i>
                    {{ config('Convert.news')[$_SESSION['lang']] }}</a></a>
            </li>
            <li>
                <a href="#tab5" data-toggle="tab">
                    <i class="livicon" data-name="edit" data-size="16" data-loop="true" data-c="#418BCA" data-hc="#418BCA"></i>
                    {{ config('Convert.comments')[$_SESSION['lang']] }}</a>
            </li>
            <li>
                <a href="#tab6" data-toggle="tab">
                    <i class="livicon" data-name="address-book" data-size="16" data-loop="true" data-c="#418BCA" data-hc="#418BCA"></i>
                    {{ config('Convert.book_contacts')[$_SESSION['lang']] }}</a>
            </li>
            <li>
                <a href="#tab7" data-toggle="tab">
                    <i class="livicon" data-name="edit" data-size="16" data-loop="true" data-c="#418BCA" data-hc="#418BCA"></i>
                    {{ config('Convert.chatting_contacts')[$_SESSION['lang']] }}</a>
            </li>
            <li>
                <a href="#tab8" data-toggle="tab">
                    <i class="livicon" data-name="trophy" data-size="16" data-loop="true" data-c="#418BCA" data-hc="#418BCA"></i>
                    {{ config('Convert.update_to_customer')[$_SESSION['lang']] }}</a>
            </li>

        </ul>
        <div  class="tab-content mar-top">
            <div id="tab1" class="tab-pane fade active in">
                <div class="form-group has-success" style="margin-top:20px;">
                </div>
                <form role="form" action="" method="POST" enctype="multipart/form-data">
                    <input type="hidden" id="_token" name="_token" value="{{ csrf_token() }}"/>

                    <?php
                    if($user->picture == null || $user->picture == ''){
                        $user->picture = 'noimage.png';
                    }
                    if($user->online == 1){
                        $online = '/img/online.png';
                        $alt = 'Online';
                    }else{
                        $online = '/img/offline.png';
                        $alt = 'Offline';
                    }
                    $masters = DB::table('date_user_masters')->where('user_id', $user->id)->where('status', 0)->get();
                    $dancer = '';
                    $url = '/uploads/categories/';
                    $mprices = array();
                    foreach($masters as $master){
                        $cat_id = $master->cat_id;
                        $subcat_id = $master->subcat_id;
                        $price = $master->price;
                        $video = $master->video;
                        $audio = $master->audio;
                        $comment = $master->comment;
                        $actions = $master->action;
                        $high_action = $master->high_action;
                        $strTmp = config('Convert.to_master')[$_SESSION['lang']];
                        $action = '<span style="color:#f6f9f8;background: #3963b7;padding: 3px 7px;cursor: pointer;border-radius: 5px;" data-toggle="modal" data-target="#activeModal" onclick="updateMaster(\'1\', \''.$master->id.'\')">'.$strTmp.'</span>';

                        if($actions == 1){
                            //$action = '<span style="color:#f6f9f8;background: #3963b7;padding: 3px 7px;cursor: pointer;border-radius: 5px;" data-toggle="modal" data-target="#inactiveModal" onclick="updateMaster(\'0\', \''.$master->id.'\')">to Customer</span>';
                            $strTmp = config('Convert.masters')[$_SESSION['lang']];
                            $action = '<span style="color: #f6f9f8;background: #08bd18;padding: 3px 7px;border-radius: 5px;">'.$strTmp.'</span>';
                        }
                        $mname = '';
                        $dancer1 = '';
                        if($subcat_id != 0){
                            $cat = DB::table('date_category')->where('id', $cat_id)->first();
                            if(!empty($cat)){
                                $mname = $cat->name.'∙';
                            }
                            $subcat = DB::table('date_subcategory')->where('cat_id', $cat_id)->where('id', $subcat_id)->first();
                            if(!empty($subcat)){
                                $mname .= $subcat->name;
                                $p = $subcat->icon;
                                if($dancer == '') $dancer = '<img src="'.$url.''.$p.'" style="border-radius:50%;width:30px;height:30px;margin-right:10px;">';
                                else  $dancer .= '<img src="'.$url.''.$p.'" style="border-radius:50%;width:30px;height:30px;margin-right:10px;">';
                                $dancer1 = '<img src="'.$url.''.$p.'" style="border-radius:50%;width:30px;height:30px;margin-right:10px;">';
                            }
                        }
                        $haction = '';
                        if($high_action == 1){
                            $haction = '<span style="color: #f6f9f8;background: #ec39dd;padding: 3px 7px;cursor: pointer;border-radius: 5px;" data-toggle="modal" data-target="#activeHighModal" onclick="updateHighMaster(\'1\', \''.$master->id.'\')">to high Master</span>';
                        }else if($high_action == 2){
                            $strTmp = config('Convert.high_master')[$_SESSION['lang']];
                            $haction = '<span style="color: #f6f9f8;background: #08bd18;padding: 3px 7px;border-radius: 5px;">'.$strTmp.'</span>';
                        }
                        $mprices[] = array($dancer1, $mname, $price, $video, $audio, $comment, $action, $haction);
                    }
                    ?>
                    <div class="form-group has-success row">
                        <div class="col-lg-4" style="margin-top:10px;">
                            <label class="control-label" for="photo" style="font-size: 120%">{{ config('Convert.master_photo')[$_SESSION['lang']] }}&nbsp;&nbsp;
                                <img src ="{{ $online }}" style="width:20px;padding-bottom:10px;" title="{{ $alt }}">&nbsp;&nbsp;{!! $dancer !!}
                            </label><br>
                            <img src="/uploads/users/{{ $user->picture }}" style="max-height:150px;">
                        </div>
                        <div class="col-lg-8" style="margin-top:10px;">
                            <table class="table" style="min-width: 500px !important;max-width: 90% !important;">
                                <thead>
                                    <?php for($i = 0; $i < count($mprices); $i++){ ?>
                                    <tr style="border-bottom:1px #a55e5e solid;">
                                        <td style="text-align: left;">{!! $mprices[$i][0] !!}</td>
                                        <td style="text-align: left;">{!! $mprices[$i][1] !!}</td>
                                        <td style="text-align: left;">{!! $mprices[$i][2] !!} / a hour</td>
                                        <td style="text-align: left;">
                                            {!! $mprices[$i][6] !!}
                                        </td>
                                        <td style="text-align: left;">
                                            {!! $mprices[$i][7] !!}
                                        </td>
                                    </tr>
                                    <?php } ?>
                                </thead>
                            </table>

                            <div class="modal fade" id="activeModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content" style="width: 80%">
                                        <input type="hidden" id="um_id" value="0">
                                        <input type="hidden" id="actionval" value="0">
                                        {{--<input type="hidden" id="activeid" value="0">--}}
                                        <div class="modal-body" id="statecontent">
                                            {{ config('Convert.master_update_message')[$_SESSION['lang']] }}
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default" class="form-control" data-dismiss="modal" onclick="updateUserMaster()">{{ config('Convert.ok')[$_SESSION['lang']] }}</button>
                                            <button type="button" class="btn btn-default" class="form-control" data-dismiss="modal">{{ config('Convert.cancel')[$_SESSION['lang']] }}</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            {{-- inactive product modal--}}
                            <div class="modal fade" id="inactiveModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content" style="width: 80%">
                                        {{--<input type="hidden" id="inactiveid" value="0">--}}
                                        <div class="modal-body" id="statecontent">
                                            {{ config('Convert.master_release_message')[$_SESSION['lang']] }}
                                        </div>

                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default" class="form-control" data-dismiss="modal" onclick="updateUserMaster()">{{ config('Convert.ok')[$_SESSION['lang']] }}</button>
                                            <button type="button" class="btn btn-default" class="form-control" data-dismiss="modal">{{ config('Convert.cancel')[$_SESSION['lang']] }}</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="modal fade" id="activeHighModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content" style="width: 80%">
                                        <input type="hidden" id="um_id" value="0">
                                        <input type="hidden" id="actionval" value="0">
                                        {{--<input type="hidden" id="activeid" value="0">--}}
                                        <div class="modal-body" id="statecontent">
                                            {{ config('Convert.master_update_to_high_message')[$_SESSION['lang']] }}
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default" class="form-control" data-dismiss="modal" onclick="updateHighUserMaster()">{{ config('Convert.ok')[$_SESSION['lang']] }}</button>
                                            <button type="button" class="btn btn-default" class="form-control" data-dismiss="modal">{{ config('Convert.cancel')[$_SESSION['lang']] }}</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group has-success">
                        <div class="col-lg-4">
                            <span  class="control-label" style="margin-top:-5px;">{{ config('Convert.master_id')[$_SESSION['lang']] }}</span>&nbsp;&nbsp; {{ $user->userno }}
                        </div>
                        <div class="col-lg-4">
                            <span  class="control-label" style="margin-top:-5px;">{{ config('Convert.master_user_name')[$_SESSION['lang']] }}</span>&nbsp;&nbsp; {{ $user->username }}
                        </div>
                        <div class="col-lg-4">
                            <span  class="control-label" style="margin-top:-5px;">{{ config('Convert.master_real_name')[$_SESSION['lang']] }}</span>&nbsp;&nbsp; {{ $user->first_name }}
                        </div>
                    </div>
                    <div class="form-group has-success" style="margin-top:50px;">
                        <div class="col-lg-4">
                            <span  class="control-label" style="margin-top:-5px;">{{ config('Convert.weixin_id')[$_SESSION['lang']] }}</span>&nbsp;&nbsp; {{ $user->weisin_id }}
                        </div>
                        <div class="col-lg-4">
                            <span  class="control-label" style="margin-top:-5px;">{{ config('Convert.qq_id')[$_SESSION['lang']] }}</span>&nbsp;&nbsp; {{ $user->qq_id }}
                        </div>
                        <div class="col-lg-4">
                            <span  class="control-label" style="margin-top:-5px;">{{ config('Convert.master_phone')[$_SESSION['lang']] }}</span>&nbsp;&nbsp; {{ $user->phone1 }}
                        </div>
                    </div>
                    <div class="form-group has-success" style="margin-top:80px;">
                        <div class="col-lg-4">
                            <span  class="control-label" style="margin-top:-5px;">{{ config('Convert.master_email')[$_SESSION['lang']] }}</span>&nbsp;&nbsp; {{ $user->email1 }}
                        </div>
                        <div class="col-lg-4">
                            <span  class="control-label" style="margin-top:-5px;">{{ config('Convert.cities')[$_SESSION['lang']] }}</span>&nbsp;&nbsp; {{ $user->city }}
                        </div>
                        <div class="col-lg-4">
                            <span  class="control-label" style="margin-top:-5px;">{{ config('Convert.gender')[$_SESSION['lang']] }}</span>&nbsp;&nbsp; {{ $user->gender }}
                        </div>
                    </div>

                    <div class="form-group has-success" style="margin-top:130px;">
                        <hr style="color:#999;border-top: 1px solid #a55e5e;">
                        <label class="control-label" for="photo" style="font-size: 120%">{{ config('Convert.master_stock')[$_SESSION['lang']] }}</label>
                    </div>
                    <?php
                    $detail = DB::table('date_user_details')->where('user_id', $user->id)->first();
                    $vip_type = '';
                    $user_card = 'nocard.jpg';
                    $car_card = 'nocard.jpg';
                    $lat = 0;
                    $lon = 0;
                    if(!empty($detail)){
                        $vip = DB::table('date_vips')->where('id', $detail->vip_id)->first();
                        if(!empty($vip)){
                            $vip_type = $vip->name;
                        }
                        $user_card = $detail->user_card;
                        $car_card = $detail->car_card;
                        $lat = $detail->lat;
                        $lon = $detail->lon;
                    }
                    $pocket = DB::table('date_user_pockets')->where('user_id', $user->id)->first();
                    $app_amount = 0;$integration_amount = 0;$coupon_amount = 0;$favorite_amount = 0;$stock_amount = 0;
                    if(!empty($pocket)){
                        $app_amount = $pocket->app_amount;
                        $integration_amount = $pocket->integration_amount;
                        $coupon_amount = $pocket->coupon_amount;
                        $favorite_amount = $pocket->favorite_amount;
                    }
                    ?>
                    <div class="form-group has-success" style="margin-top:10px;">
                        <div class="col-lg-4">
                            <span  class="control-label" style="margin-top:-5px;">{{ config('Convert.vip_type')[$_SESSION['lang']] }}</span>&nbsp;&nbsp; {{ $vip_type }}
                        </div>
                        <div class="col-lg-4">
                            <span  class="control-label" style="margin-top:-5px;">{{ config('Convert.stock_amount')[$_SESSION['lang']] }}</span>&nbsp;&nbsp; {{ $stock_amount }} (RMB)
                        </div>
                        <div class="col-lg-4">
                            <span  class="control-label" style="margin-top:-5px;">{{ config('Convert.app_amount')[$_SESSION['lang']] }}</span>&nbsp;&nbsp; {{ $app_amount }} (RMB)
                        </div>
                    </div>
                    <div class="form-group has-success" style="margin-top:50px;">
                        <div class="col-lg-4">
                            <span  class="control-label" style="margin-top:-5px;">{{ config('Convert.coupon_amount')[$_SESSION['lang']] }}</span>&nbsp;&nbsp; {{ $coupon_amount }} (RMB)
                        </div>
                        <div class="col-lg-4">
                            <span  class="control-label" style="margin-top:-5px;">{{ config('Convert.favorite_amount')[$_SESSION['lang']] }}</span>&nbsp;&nbsp; {{ $favorite_amount }} (RMB)
                        </div>
                        <div class="col-lg-4">
                            <span  class="control-label" style="margin-top:-5px;">{{ config('Convert.integration_amount')[$_SESSION['lang']] }}</span>&nbsp;&nbsp; {{ $integration_amount }} (RMB)
                        </div>
                    </div>
                    <div class="form-group has-success" style="margin-top:130px;">
                        <hr style="color:#999;border-top: 1px solid #a55e5e;">
                        <label class="control-label" for="photo" style="font-size: 120%">{{ config('Convert.authorization_photos')[$_SESSION['lang']] }}</label>
                    </div>
                    <div class="form-group has-success" style="margin-top:10px;">
                        <div class="col-lg-6">
                            <label class="control-label" for="photo" style="font-size: 120%">{{ config('Convert.master_card')[$_SESSION['lang']] }}</label><br>
                            <img src="/uploads/auth/usercards/{{ $user_card }}" style="max-height:300px;">
                        </div>
                        <div class="col-lg-6">
                            <label class="control-label" for="photo" style="font-size: 120%">{{ config('Convert.car_card')[$_SESSION['lang']] }}</label><br>
                            <img src="/uploads/auth/carcards/{{ $car_card }}" style="max-height:300px;">
                        </div>
                    </div>
                    <div class="form-group has-success" style="margin-top:390px;">
                        <hr style="color:#999;border-top: 1px solid #a55e5e;">
                        <label class="control-label" for="photo" style="font-size: 120%">{{ config('Convert.master_video_audio')[$_SESSION['lang']] }}</label>
                    </div>
                    <div class="form-group has-success">
                        <label class="control-label" style="font-size: 120%" for="name"></label>
                        <div>
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th style="text-align: left;">{{ config('Convert.category_icon')[$_SESSION['lang']] }}</th>
                                        <th style="text-align: left;">{{ config('Convert.category_name')[$_SESSION['lang']] }}</th>
                                        <th style="text-align: left;">{{ config('Convert.master_video')[$_SESSION['lang']] }}</th>
                                        <th style="text-align: left;">{{ config('Convert.master_audio')[$_SESSION['lang']] }}</th>
                                        <th style="text-align: left;">{{ config('Convert.master_comment')[$_SESSION['lang']] }}</th>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php for($i = 0; $i < count($mprices); $i++){ ?>
                                <tr style="vertical-align: bottom;">
                                    <td style="text-align: left;">{!! $mprices[$i][0] !!}</td>
                                    <td style="text-align: left;">{!! $mprices[$i][1] !!}</td>
                                    <td style="text-align: left;">
                                        <video width="538" height="303" poster="/uploads/auth/uservideos/novideo.jpg" controls>
                                            <source src="/uploads/auth/uservideos/{{ $mprices[$i][3] }}" type="video/mp4">
                                        </video>
                                    </td>
                                    <td style="text-align: left;">
                                        <audio loop="loop" controls="controls" style="">
                                            <source src="/uploads/auth/useraudios/{{ $mprices[$i][4] }}" />
                                        </audio>
                                    </td>
                                    <td style="text-align: left;">
                                        {{ $mprices[$i][5] }}
                                    </td>
                                </tr>
                                <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="form-group has-success">
                        <label class="control-label" style="font-size: 120%" for="name">{{ config('Convert.customer_location')[$_SESSION['lang']] }}</label>
                        <div>
                            <div id="map" style="width: 100%; height: 400px;"></div>
                        </div>
                    </div>
                    <script>
                        var map = new google.maps.Map(document.getElementById('map'), {
                            zoom: 12,
                            center: new google.maps.LatLng({{ $lat }}, {{ $lon }}),
                            mapTypeControl: false,
                            navigationControl: false,
                            zoomControl:false,
                            streetViewControl: false,
                            mapTypeId: google.maps.MapTypeId.ROADMAP
                        });
                        var marker = new google.maps.Marker({
                            position: new google.maps.LatLng({{ $lat }}, {{ $lon }}),
                            draggable:true, //set marker draggable
                            animation: google.maps.Animation.DROP,
                            map: map
                        });
                    </script>

                    <div class="form-group has-success" style="margin-top:40px;">
                        <hr style="color:#999;border-top: 1px solid #a55e5e;">
                        <label class="control-label" for="photo" style="font-size: 120%">{{ config('Convert.others')[$_SESSION['lang']] }}</label>
                    </div>
                    <div class="form-group has-success" style="margin-top:10px;">
                        <div class="col-lg-4">
                            <span  class="control-label" style="margin-top:-5px;">{{ config('Convert.business_type')[$_SESSION['lang']] }}</span>&nbsp;&nbsp; {{ $user->business }}
                        </div>
                        <div class="col-lg-4">
                            <span  class="control-label" style="margin-top:-5px;">{{ config('Convert.school')[$_SESSION['lang']] }}</span>&nbsp;&nbsp; {{ $user->school }}
                        </div>
                        <div class="col-lg-4">
                            <span  class="control-label" style="margin-top:-5px;">{{ config('Convert.tags')[$_SESSION['lang']] }}</span>&nbsp;&nbsp; {{ $user->tags }}
                        </div>
                    </div>
                    <div class="form-group has-success" style="margin-top:70px;">
                        <hr style="color:#999;border-top: 1px solid #a55e5e;">
                        <label class="control-label" for="photo" style="font-size: 120%">{{ config('Convert.customer_comment')[$_SESSION['lang']] }}</label>
                    </div>
                    <div class="form-group has-success" style="margin-top:20px;">
                        <input type="text" class="form-control" id="comment" name="comment" placeholder="no comment" value="{{ $user->comment }}" disabled="disabled">
                    </div>


                    <div class="col-md-12 mar-10" style="padding-top:30px;">
                        <input type="reset" value="{{ config('Convert.back')[$_SESSION['lang']] }}" style="font-size: 120%" class="btn btn-success btn-block btn-md btn-responsive" onclick="onBack()">
                    </div>
                </form>
            </div>
            <div id="tab2" class="tab-pane fade">
                <div class="form-group has-success" style="margin-top:20px;">
                    <div style="padding-top:10px;">
                        <div class="panel panel-primary filterable" style="background-color: transparent !important;">

                            <div class="panel-body table-responsive">
                                <table class="table table-striped table-bordered" id="table2">
                                    <thead>
                                    <tr>
                                        <th>{{ config('Convert.no')[$_SESSION['lang']] }}</th>
                                        <th>{{ config('Convert.payer_id')[$_SESSION['lang']] }}</th>
                                        <th>{{ config('Convert.payer_name')[$_SESSION['lang']] }}</th>
                                        <th>{{ config('Convert.payment_method')[$_SESSION['lang']] }}</th>
                                        <th>{{ config('Convert.transaction_id')[$_SESSION['lang']] }}</th>
                                        <th>{{ config('Convert.subscribe_type')[$_SESSION['lang']] }}</th>
                                        <th>{{ config('Convert.payment_amount')[$_SESSION['lang']] }}</th>
                                        <th>{{ config('Convert.transaction_fee')[$_SESSION['lang']] }}</th>
                                        <th>{{ config('Convert.invoice_number')[$_SESSION['lang']] }}</th>
                                        <th>{{ config('Convert.payment_type')[$_SESSION['lang']] }}</th>
                                        <th>{{ config('Convert.paid_date')[$_SESSION['lang']] }}</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                    //print_r($customers);
                                    $i = 0;
                                    $payments = DB::table('date_payment_tickets as t')
                                            ->leftJoin('date_payment_method as m', 'm.id', '=', 't.pay_method')
                                            ->leftJoin('date_subscribe_prices as s', 's.id', '=', 't.subscribe_id')
                                            ->select('t.*', 'm.name as method_name', 's.name as subscribe_name')
                                            ->where('user_id', $user->id)->orderby('id', 'desc')->get();
                                    foreach($payments as $payment){
                                    $type = "Input";
                                    if($payment->type == 1){
                                        $type = "Output";
                                    }
                                    $i++;
                                    $paiddate = date('M d, Y', strtotime($payment->paid_date));

                                    ?>
                                    <tr>
                                        <td>{{ $i }}</td>
                                        <td>{{ $payment->payer_id }}</td>
                                        <td>{{ $payment->payer_name }}</td>
                                        <td>{{ $payment->method_name }}</td>
                                        <td>{{ $payment->transaction_id }}</td>
                                        <td>{{ $payment->subscribe_name }}</td>
                                        <td>{{ $payment->amount }}</td>
                                        <td>{{ $payment->transaction_fee }}</td>
                                        <td>{{ $payment->invoice_number }}</td>
                                        <td>{{ $type }}</td>
                                        <td>{{ $paiddate }}</td>

                                    </tr>
                                    <?php
                                    }
                                    ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="tab3" class="tab-pane fade">
                <div class="form-group has-success" style="margin-top:20px;">
                    <div style="padding-top:10px;">
                        <div class="panel panel-primary filterable" style="background-color: transparent !important;">

                            <div class="panel-body table-responsive">
                                <table class="table table-striped table-bordered" id="table1">
                                    <thead>
                                    <tr>
                                        <th>{{ config('Convert.no')[$_SESSION['lang']] }}</th>
                                        <th>{{ config('Convert.receiver_sender_id')[$_SESSION['lang']] }}</th>
                                        <th>{{ config('Convert.categories')[$_SESSION['lang']] }}</th>
                                        <th>{{ config('Convert.point_type')[$_SESSION['lang']] }}</th>
                                        <th>{{ config('Convert.point_amount')[$_SESSION['lang']] }}</th>
                                        <th>{{ config('Convert.content')[$_SESSION['lang']] }}</th>
                                        <th>{{ config('Convert.input_output')[$_SESSION['lang']] }}</th>
                                        <th>{{ config('Convert.point_date')[$_SESSION['lang']] }}</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                    //print_r($customers);
                                    $i = 0;
                                    $points = DB::table('date_point_transactions as t')
                                            ->leftJoin('date_point_types as pt', 'pt.id', '=', 't.pointtype_id')
                                            ->leftJoin('date_category as c', 'c.id', '=', 't.cat_id')
                                            ->leftJoin('date_subcategory as sc', 'sc.id', '=', 't.subcat_id')
                                            ->leftJoin('users as u', 'u.id', '=', 't.receive_user_id')
                                            ->select('t.*', 'pt.name as pt_name', 'c.name as cat_name', 'sc.name as subcat_name', 'u.username as username')
                                            ->where('user_id', $user->id)->orderby('id', 'desc')->get();
                                    foreach($points as $point){
                                    $type = "Input";
                                    if($point->type == 1){
                                        $type = "Output";
                                    }
                                    $cat_name = $point->cat_name;
                                    if($point->subcat_id != 0){
                                        $cat_name .= '->'.$point->subcat_name;
                                    }
                                    $i++;
                                    $paiddate = date('M d, Y', strtotime($payment->created_at));

                                    ?>
                                    <tr>
                                        <td>{{ $i }}</td>
                                        <td>{{ $point->username }}</td>
                                        <td>{{ $cat_name }}</td>
                                        <td>{{ $point->pt_name }}</td>
                                        <td>{{ $point->point_amount }}</td>
                                        <td>{{ $point->content }}</td>
                                        <td>{{ $type }}</td>
                                        <td>{{ $paiddate }}</td>

                                    </tr>
                                    <?php
                                    }
                                    ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="tab4" class="tab-pane fade">
                <div class="form-group has-success" style="margin-top:20px;">
                    <div style="padding-top:10px;">
                        <div class="panel panel-primary filterable" style="background-color: transparent !important;">

                            <div class="panel-body table-responsive">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}"/>

                                <table class="table table-striped table-bordered table-hover dataTable no-footer sample_editable"
                                       id="newsdatatable" role="grid">
                                    <thead>
                                    <tr role="row">
                                        <th class="sorting_asc" tabindex="0" aria-controls="occasiontable" rowspan="1"
                                            colspan="1" aria-label="
                                                ID
                                            : activate to sort column ascending" style="width: 30px;">{{ config('Convert.id')[$_SESSION['lang']] }}
                                        </th>
                                        <th class="sorting_asc" tabindex="0" aria-controls="occasiontable" rowspan="1"
                                            colspan="1" aria-label="
                                                Title
                                            : activate to sort column ascending" style="width: 100px;">{{ config('Convert.title')[$_SESSION['lang']] }}
                                        </th>
                                        <th class="sorting_asc" tabindex="0" aria-controls="occasiontable" rowspan="1"
                                            colspan="1" aria-label="
                                                Description
                                            : activate to sort column ascending" style="width: 100px;">{{ config('Convert.description')[$_SESSION['lang']] }}
                                        </th>
                                        <th class="sorting_asc" tabindex="0" aria-controls="occasiontable" rowspan="1"
                                            colspan="1" aria-label="
                                                Photo
                                            : activate to sort column ascending" style="width: 100px;">{{ config('Convert.news_image')[$_SESSION['lang']] }}
                                        </th>
                                        <th class="sorting_asc" tabindex="0" aria-controls="occasiontable" rowspan="1"
                                            colspan="1" aria-label="
                                                Favorites
                                            : activate to sort column ascending" style="width: 100px;">{{ config('Convert.favorites')[$_SESSION['lang']] }}
                                        </th>
                                        <th class="sorting_asc" tabindex="0" aria-controls="occasiontable" rowspan="1"
                                            colspan="1" aria-label="
                                                Comments
                                            : activate to sort column ascending" style="width: 100px;">{{ config('Convert.comments')[$_SESSION['lang']] }}
                                        </th>
                                        <th class="sorting" tabindex="0" aria-controls="occasiontable" rowspan="1"
                                            colspan="1" aria-label="
                                                Created Date
                                            : activate to sort column ascending" style="width: 150px;">{{ config('Convert.create_date')[$_SESSION['lang']] }}
                                        </th>
                                        <th class="sorting" tabindex="0" aria-controls="occasiontable" rowspan="1"
                                            colspan="1" aria-label="
                                                Delete
                                            : activate to sort column ascending" style="width: 50px;">{{ config('Convert.delete')[$_SESSION['lang']] }}
                                        </th>
                                    </tr>
                                    </thead>
                                    <tbody>

                                    </tbody>
                                </table>
                            </div>

                            <!-- delete modal -->
                            <div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content" style="width: 80%">
                                        <input type="hidden" id="deleteid" value="0">
                                        <div class="modal-body">
                                            {{ config('Convert.news_delete_message')[$_SESSION['lang']] }}
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default" class="form-control" data-dismiss="modal" onclick="deleteData()">{{ config('Convert.delete')[$_SESSION['lang']] }}</button>
                                            <button type="button" class="btn btn-default" class="form-control" data-dismiss="modal">{{ config('Convert.cancel')[$_SESSION['lang']] }}</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="tab5" class="tab-pane fade">
                <div class="form-group has-success" style="margin-top:20px;">
                    <div style="padding-top:10px;">
                        <div class="panel panel-primary filterable" style="background-color: transparent !important;">

                            <div class="panel-body table-responsive">

                                <table class="table table-striped table-bordered table-hover dataTable no-footer sample_editable"
                                       id="commentsdatatable" role="grid">
                                    <thead>
                                    <tr role="row">
                                        <th class="sorting_asc" tabindex="0" aria-controls="occasiontable" rowspan="1"
                                            colspan="1" aria-label="
                                                ID
                                            : activate to sort column ascending" style="width: 30px;">{{ config('Convert.id')[$_SESSION['lang']] }}
                                        </th>
                                        <th class="sorting_asc" tabindex="0" aria-controls="occasiontable" rowspan="1"
                                            colspan="1" aria-label="
                                                Title
                                            : activate to sort column ascending" style="width: 100px;">{{ config('Convert.news_title')[$_SESSION['lang']] }}
                                        </th>
                                        <th class="sorting_asc" tabindex="0" aria-controls="occasiontable" rowspan="1"
                                            colspan="1" aria-label="
                                                Comment
                                            : activate to sort column ascending" style="width: 100px;">{{ config('Convert.comments')[$_SESSION['lang']] }}
                                        </th>
                                        <th class="sorting" tabindex="0" aria-controls="occasiontable" rowspan="1"
                                            colspan="1" aria-label="
                                                Created Date
                                            : activate to sort column ascending" style="width: 150px;">{{ config('Convert.create_date')[$_SESSION['lang']] }}
                                        </th>
                                        <th class="sorting" tabindex="0" aria-controls="occasiontable" rowspan="1"
                                            colspan="1" aria-label="
                                                Delete
                                            : activate to sort column ascending" style="width: 50px;">{{ config('Convert.delete')[$_SESSION['lang']] }}
                                        </th>
                                    </tr>
                                    </thead>
                                    <tbody>

                                    </tbody>
                                </table>
                            </div>

                            <!-- delete modal -->
                            <div class="modal fade" id="deleteCommentModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content" style="width: 80%">
                                        <input type="hidden" id="commentdeleteid" value="0">
                                        <div class="modal-body">
                                            {{ config('Convert.comments_delete_message')[$_SESSION['lang']] }}
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default" class="form-control" data-dismiss="modal" onclick="deleteCommentData()">{{ config('Convert.delete')[$_SESSION['lang']] }}</button>
                                            <button type="button" class="btn btn-default" class="form-control" data-dismiss="modal">{{ config('Convert.cancel')[$_SESSION['lang']] }}</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div id="tab6" class="tab-pane fade">
                <div class="form-group has-success" style="margin-top:20px;">
                    <div style="padding-top:10px;">
                        <div class="panel panel-primary filterable" style="background-color: transparent !important;">

                            <div class="panel-body table-responsive">

                                <table class="table table-striped table-bordered table-hover dataTable no-footer sample_editable"
                                       id="bookdatatable" role="grid">
                                    <thead>
                                    <tr role="row">
                                        <th class="sorting_asc" tabindex="0" aria-controls="occasiontable" rowspan="1"
                                            colspan="1" aria-label="
                                                ID
                                            : activate to sort column ascending" style="width: 30px;">{{ config('Convert.id')[$_SESSION['lang']] }}
                                        </th>
                                        <th class="sorting_asc" tabindex="0" aria-controls="occasiontable" rowspan="1"
                                            colspan="1" aria-label="
                                                Title
                                            : activate to sort column ascending" style="width: 100px;">{{ config('Convert.contact_photo')[$_SESSION['lang']] }}
                                        </th>
                                        <th class="sorting_asc" tabindex="0" aria-controls="occasiontable" rowspan="1"
                                            colspan="1" aria-label="
                                                Comment
                                            : activate to sort column ascending" style="width: 100px;">{{ config('Convert.contact_name')[$_SESSION['lang']] }}
                                        </th>
                                        <th class="sorting_asc" tabindex="0" aria-controls="occasiontable" rowspan="1"
                                            colspan="1" aria-label="
                                                Comment
                                            : activate to sort column ascending" style="width: 100px;">{{ config('Convert.contact_info')[$_SESSION['lang']] }}
                                        </th>
                                        <th class="sorting" tabindex="0" aria-controls="occasiontable" rowspan="1"
                                            colspan="1" aria-label="
                                                Created Date
                                            : activate to sort column ascending" style="width: 150px;">{{ config('Convert.create_date')[$_SESSION['lang']] }}
                                        </th>
                                        <th class="sorting" tabindex="0" aria-controls="occasiontable" rowspan="1"
                                            colspan="1" aria-label="
                                                Delete
                                            : activate to sort column ascending" style="width: 50px;">{{ config('Convert.delete')[$_SESSION['lang']] }}
                                        </th>
                                    </tr>
                                    </thead>
                                    <tbody>

                                    </tbody>
                                </table>
                            </div>

                            <!-- delete modal -->
                            <div class="modal fade" id="deleteCommentModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content" style="width: 80%">
                                        <input type="hidden" id="commentdeleteid" value="0">
                                        <div class="modal-body">
                                            {{ config('Convert.book_contact_delete_message')[$_SESSION['lang']] }}
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default" class="form-control" data-dismiss="modal" onclick="deleteBookData()">{{ config('Convert.delete')[$_SESSION['lang']] }}</button>
                                            <button type="button" class="btn btn-default" class="form-control" data-dismiss="modal">{{ config('Convert.cancel')[$_SESSION['lang']] }}</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="tab7" class="tab-pane fade">
                <div class="form-group has-success" style="margin-top:20px;">
                    <div style="padding-top:10px;">
                        <div class="panel panel-primary filterable" style="background-color: transparent !important;">

                            <div class="panel-body table-responsive">

                                <table class="table table-striped table-bordered table-hover dataTable no-footer sample_editable"
                                       id="chatdatatable" role="grid">
                                    <thead>
                                    <tr role="row">
                                        <th class="sorting_asc" tabindex="0" aria-controls="occasiontable" rowspan="1"
                                            colspan="1" aria-label="
                                                ID
                                            : activate to sort column ascending" style="width: 30px;">{{ config('Convert.id')[$_SESSION['lang']] }}
                                        </th>
                                        <th class="sorting_asc" tabindex="0" aria-controls="occasiontable" rowspan="1"
                                            colspan="1" aria-label="
                                                Title
                                            : activate to sort column ascending" style="width: 100px;">{{ config('Convert.contact_photo')[$_SESSION['lang']] }}
                                        </th>
                                        <th class="sorting_asc" tabindex="0" aria-controls="occasiontable" rowspan="1"
                                            colspan="1" aria-label="
                                                Comment
                                            : activate to sort column ascending" style="width: 100px;">{{ config('Convert.contact_name')[$_SESSION['lang']] }}
                                        </th>
                                        <th class="sorting_asc" tabindex="0" aria-controls="occasiontable" rowspan="1"
                                            colspan="1" aria-label="
                                                Comment
                                            : activate to sort column ascending" style="width: 100px;">{{ config('Convert.contact_info')[$_SESSION['lang']] }}
                                        </th>
                                        <th class="sorting" tabindex="0" aria-controls="occasiontable" rowspan="1"
                                            colspan="1" aria-label="
                                                Created Date
                                            : activate to sort column ascending" style="width: 150px;">{{ config('Convert.create_date')[$_SESSION['lang']] }}
                                        </th>
                                        <th class="sorting" tabindex="0" aria-controls="occasiontable" rowspan="1"
                                            colspan="1" aria-label="
                                                Delete
                                            : activate to sort column ascending" style="width: 50px;">{{ config('Convert.delete')[$_SESSION['lang']] }}
                                        </th>
                                    </tr>
                                    </thead>
                                    <tbody>

                                    </tbody>
                                </table>
                            </div>

                            <!-- delete modal -->
                            <div class="modal fade" id="deleteCommentModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content" style="width: 80%">
                                        <input type="hidden" id="commentdeleteid" value="0">
                                        <div class="modal-body">
                                            {{ config('Convert.chat_contact_delete_message')[$_SESSION['lang']] }}
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default" class="form-control" data-dismiss="modal" onclick="deleteChatData()">{{ config('Convert.delete')[$_SESSION['lang']] }}</button>
                                            <button type="button" class="btn btn-default" class="form-control" data-dismiss="modal">{{ config('Convert.cancel')[$_SESSION['lang']] }}</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="tab8" class="tab-pane fade">

                <form id="commentForm" action="{{ route('admin.customers.update1') }}"
                      method="POST" id="wizard-validation" enctype="multipart/form-data" class="form-horizontal">
                    <!-- CSRF Token -->
                    <input type="hidden" name="user_id" value="{{ $user->id }}"/>
                    <input type="hidden" name="_method" value="PATCH"/>
                    <input type="hidden" name="_token" value="{{ csrf_token() }}" />


                    <div class="form-group has-success" style="margin-top:20px;width:50%;margin-left:25%">
                        <label class="control-label" style="font-size: 120%" for="name">{{ config('Convert.master_status')[$_SESSION['lang']] }}</label>
                        <div style="padding-top:10px;">
                            <select class="form-control " title="Select status..." name="groups[]" id="groups" required>
                                <option value="">{{ config('Convert.select')[$_SESSION['lang']] }}</option>
                                @foreach($roles as $role)
                                    <option value="{!! $role->id !!}" {{ (array_key_exists($role->id, $userRoles) ? ' selected="selected"' : '') }}>{{ $role->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="form-group has-success" style="width:50%;margin-left:25%">
                        <label class="control-label" style="font-size: 120%" for="name">{{ config('Convert.activate_user')[$_SESSION['lang']] }}</label>
                        <div>
                            <input id="activate" name="activate" type="checkbox" class="pos-rel p-l-30" value="1" @if($status) checked="checked" @endif  >
                            <span>{{ config('Convert.master_active_message')[$_SESSION['lang']] }}</span>
                        </div>
                    </div>

                    <div class="form-group has-success" style="width:50%;margin-left:25%">
                        <input type="submit" value="Update to Master" style="font-size: 120%" class="btn btn-success btn-block btn-md btn-responsive">

                    </div>
                </form>
            </div>
        </div>

        <!-- Message modal -->
        <div class="modal fade" id="messageModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content" style="width: 80%">
                    <div class="modal-body" id="messagecontent">

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" class="form-control" data-dismiss="modal">{{ config('Convert.ok')[$_SESSION['lang']] }}</button>
                    </div>
                </div>
            </div>
        </div>

        <!-- delete modal -->
        <div class="modal fade" id="deleteBookModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content" style="width: 80%">
                    <input type="hidden" id="bookdeleteid" value="0">
                    <div class="modal-body">
                        {{ config('Convert.book_contact_delete_message')[$_SESSION['lang']] }}
                </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" class="form-control" data-dismiss="modal" onclick="deleteBookData()">{{ config('Convert.delete')[$_SESSION['lang']] }}</button>
                        <button type="button" class="btn btn-default" class="form-control" data-dismiss="modal">{{ config('Convert.cancel')[$_SESSION['lang']] }}</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- delete modal -->
        <div class="modal fade" id="deleteChatModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content" style="width: 80%">
                    <input type="hidden" id="chatdeleteid" value="0">
                    <div class="modal-body">
                        {{ config('Convert.chat_contact_delete_message')[$_SESSION['lang']] }}
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" class="form-control" data-dismiss="modal" onclick="deleteChatData()">{{ config('Convert.delete')[$_SESSION['lang']] }}</button>
                        <button type="button" class="btn btn-default" class="form-control" data-dismiss="modal">{{ config('Convert.cancel')[$_SESSION['lang']] }}</button>
                    </div>
                </div>
            </div>
        </div>
        @stop

        {{-- page level scripts --}}
        @section('footer_scripts')

            <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/jquery.dataTables.js') }}" ></script>
            <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.bootstrap.js') }}" ></script>
            <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.buttons.js') }}" ></script>
            <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.colReorder.js') }}" ></script>
            <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.responsive.js') }}" ></script>
            <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.rowReorder.js') }}" ></script>
            <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/buttons.colVis.js') }}" ></script>
            <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/buttons.html5.js') }}" ></script>
            <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/buttons.print.js') }}" ></script>
            <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/buttons.bootstrap.js') }}" ></script>
            <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/buttons.print.js') }}" ></script>
            <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/pdfmake.js') }}" ></script>
            <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/vfs_fonts.js') }}" ></script>
            <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.scroller.js') }}" ></script>
            <script type="text/javascript" src="{{ asset('assets/js/pages/table-advanced.js') }}" ></script>
            <script>
            </script><script>
                var table = null;
                var commenttable = null;
                var booktable = null;
                var chattable = null;
                $(function () {
                    var nEditing = null;
                    table = $('#newsdatatable').DataTable({
                        processing: true,
                        serverSide: true,
                        ajax: '{!! route("admin.users.news.data", $user->id) !!}',
                        columns: [
                            {data: 'id', name: 'id'},
                            {data: 'title', name: 'title'},
                            {data: 'description', name: 'description'},
                            {data: 'photo', name: 'photo'},
                            {data: 'favo_num', name: 'favo_num'},
                            {data: 'comment_num', name: 'comment_num'},
                            {data: 'created_at', name: 'created_date'},
                            {data: 'delete', name: 'delete', orderable: false, searchable: false}
                        ]
                    });
                    table.on('draw', function () {
                        $('.livicon').each(function () {
                            $(this).updateLivicon();
                        });
                    });

                    commenttable = $('#commentsdatatable').DataTable({
                        processing: true,
                        serverSide: true,
                        ajax: '{!! route("admin.users.comments.data", $user->id) !!}',
                        columns: [
                            {data: 'id', name: 'id'},
                            {data: 'title', name: 'title', searchable: false},
                            {data: 'comment', name: 'comment'},
                            {data: 'created_at', name: 'created_at'},
                            {data: 'delete', name: 'delete', orderable: false, searchable: false}
                        ]
                    });
                    commenttable.on('draw', function () {
                        $('.livicon').each(function () {
                            $(this).updateLivicon();
                        });
                    });
                    booktable = $('#bookdatatable').DataTable({
                        processing: true,
                        serverSide: true,
                        ajax: '{!! route("admin.users.book.data", $user->id) !!}',
                        columns: [
                            {data: 'id', name: 'id'},
                            {data: 'user_photo', name: 'user_photo', searchable: false},
                            {data: 'username', name: 'username'},
                            {data: 'user_info', name: 'user_info'},
                            {data: 'created_at', name: 'created_at'},
                            {data: 'delete', name: 'delete', orderable: false, searchable: false}
                        ]
                    });
                    booktable.on('draw', function () {
                        $('.livicon').each(function () {
                            $(this).updateLivicon();
                        });
                    });
                    chattable = $('#chatdatatable').DataTable({
                        processing: true,
                        serverSide: true,
                        ajax: '{!! route("admin.users.chat.data", $user->id) !!}',
                        columns: [
                            {data: 'id', name: 'id'},
                            {data: 'user_photo', name: 'user_photo', searchable: false},
                            {data: 'username', name: 'username'},
                            {data: 'user_info', name: 'user_info'},
                            {data: 'created_at', name: 'created_at'},
                            {data: 'delete', name: 'delete', orderable: false, searchable: false}
                        ]
                    });
                    chattable.on('draw', function () {
                        $('.livicon').each(function () {
                            $(this).updateLivicon();
                        });
                    });
                });

                function deleteBookData(){
                    var id = $('#bookdeleteid').val();
                    console.log(id);
                    /* delete row in database and datatable  */
                    $.ajax({
                        type: "get",
                        url: '/admin/book/' + id + '/delete',
                        success: function (result) {
                            if(result == ''){
                                console.log('row ' + result + ' deleted');
                                booktable.draw(false);
                                $('#messagecontent').html('Successfully deleted.');
                            }else{
                                $('#messagecontent').html('This is using in '+result);
                            }
                            $('#messageModal').modal('show');
                            //location.reload();
                        },
                        error: function (result) {
                            console.log(result)

                        }
                    });
                }
                function deleteChatData(){
                    var id = $('#chatdeleteid').val();
                    console.log(id);
                    /* delete row in database and datatable  */
                    $.ajax({
                        type: "get",
                        url: '/admin/chat/' + id + '/delete',
                        success: function (result) {
                            if(result == ''){
                                console.log('row ' + result + ' deleted');
                                chattable.draw(false);
                                $('#messagecontent').html('Successfully deleted.');
                            }else{
                                $('#messagecontent').html('This is using in '+result);
                            }
                            $('#messageModal').modal('show');
                            //location.reload();
                        },
                        error: function (result) {
                            console.log(result)

                        }
                    });
                }
                function deleteData(){
                    var id = $('#deleteid').val();
                    /* delete row in database and datatable  */
                    $.ajax({
                        type: "get",
                        url: '/admin/news/' + id + '/delete',
                        success: function (result) {
                            if(result == ''){
                                console.log('row ' + result + ' deleted');
                                table.draw(false);
                                $('#messagecontent').html('Successfully deleted.');
                            }else{
                                $('#messagecontent').html('This is using in '+result);
                            }
                            $('#messageModal').modal('show');
                            //location.reload();
                        },
                        error: function (result) {
                            console.log(result)

                        }
                    });
                }
                //delete occasion
                function deleteCommentData(){
                    var id = $('#commentdeleteid').val();
                    console.log(id);
                    /* delete row in database and datatable  */
                    $.ajax({
                        type: "get",
                        url: '/admin/comments/' + id + '/delete',
                        success: function (result) {
                            if(result == ''){
                                console.log('row ' + result + ' deleted');
                                commenttable.draw(false);
                                $('#messagecontent').html('Successfully deleted.');
                            }else{
                                $('#messagecontent').html('This is using in '+result);
                            }
                            $('#messageModal').modal('show');
                            //location.reload();
                        },
                        error: function (result) {
                            console.log(result)

                        }
                    });
                }
                function deleteCommentItem(id){
                    $('#commentdeleteid').val(id);
                }
                function deleteBookItem(id){
                    $('#bookdeleteid').val(id);
                }
                function deleteChatItem(id){
                    $('#chatdeleteid').val(id);
                }
                function deleteItem(id){
                    $('#deleteid').val(id);
                }
                function updateMaster(actionval, um_id){
                    $('#actionval').val(actionval);
                    $('#um_id').val(um_id);
                }
                function updateHighMaster(actionval, um_id){
                    $('#actionval').val(actionval);
                    $('#um_id').val(um_id);
                }
                function updateUserMaster(){
                    var actionval = $('#actionval').val();
                    var um_id = $('#um_id').val();
                    var data = {
                        um_id:um_id,
                        actionval: actionval,
                        _token: $('#_token').val()
                    };
                    $.ajax({
                        type: "post",
                        url: '{{ route('users.masters.update') }}',
                        data: data,
                        success: function (result) {
                            if(result == 1){
                                $('#messagecontent').html('Successfully updated.');
                                $('#messageModal').modal('show');

                                location.reload();
                            }else{
                                $('#messagecontent').html('Failed. Please try again.');
                                $('#messageModal').modal('show');
                            }
                        },
                        error: function (result) {
                            console.log(result)

                        }
                    });
                }
                function updateHighUserMaster(){
                    var actionval = $('#actionval').val();
                    var um_id = $('#um_id').val();
                    var data = {
                        um_id:um_id,
                        actionval: 2,
                        _token: $('#_token').val()
                    };
                    $.ajax({
                        type: "post",
                        url: '{{ route('users.highmasters.update') }}',
                        data: data,
                        success: function (result) {
                            if(result == 1){
                                $('#messagecontent').html('Successfully updated.');
                                $('#messageModal').modal('show');

                                location.reload();
                            }else{
                                $('#messagecontent').html('Failed. Please try again.');
                                $('#messageModal').modal('show');
                            }
                        },
                        error: function (result) {
                            console.log(result)

                        }
                    });
                }
            </script>

@stop
