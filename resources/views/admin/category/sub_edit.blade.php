@extends('admin/layouts/default')
{{-- Page title --}}
@section('title')
    {{ config('Convert.subedit_category')[$_SESSION['lang']] }}
    @parent
@stop

{{-- page level styles --}}
@section('header_styles')
    <link href="{{ asset('assets/vendors/bootstrap3-wysihtml5-bower/css/bootstrap3-wysihtml5.min.css') }}"  rel="stylesheet" media="screen"/>
    <link href="{{ asset('assets/css/pages/editor.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('assets/vendors/jasny-bootstrap/css/jasny-bootstrap.css') }}" rel="stylesheet" />

    <link href="/vendors/daterangepicker/css/daterangepicker-bs3.css" rel="stylesheet" type="text/css" />
    <link href="/vendors/datetimepicker/css/bootstrap-datetimepicker.min.css" rel="stylesheet" type="text/css" />
    <link href="/vendors/clockface/clockface.css" rel="stylesheet" type="text/css"/>
    <link href="/vendors/jasny-bootstrap/jasny-bootstrap.min.css" rel="stylesheet" type="text/css" />
@stop

{{-- Page content --}}
@section('content')

    <section class="content-header">
        <!--section starts-->
        <h1>{{ config('Convert.subedit_category')[$_SESSION['lang']] }}</h1>
        <ol class="breadcrumb">
            <li>
                <a href="">
                    <i class="livicon" data-name="home" data-size="14" data-loop="true"></i>
                    {{ config('Convert.dashboard')[$_SESSION['lang']] }}
                </a>
            </li>
            <li>
                <a href="#">{{ config('Convert.subcategory')[$_SESSION['lang']] }}</a>
            </li>
            <li class="active">{{ config('Convert.subedit_category')[$_SESSION['lang']] }}</li>
        </ol>
    </section>

    <div class="panel-body" style="width:90%;margin-left:5%">

        <div class="form-group has-success" style="margin-top:20px;">
        </div>
        <form role="form" action="{{ url('/admin/subcategory/'.$subcategory->id.'/update') }}" method="POST" enctype="multipart/form-data">
            <input type="hidden" name="_token" value="{{ csrf_token() }}"/>

            <div class="form-group has-success">
                <label class="control-label" style="font-size: 120%" for="name">{{ config('Convert.category_name')[$_SESSION['lang']] }}</label>
                <div>
                    <input type="text" class="form-control" name="catname" value="{{ $category->name }}" placeholder="Category name" disabled>
                </div>
            </div>
            <div class="form-group has-success">
                <label class="control-label" style="font-size: 120%" for="name">{{ config('Convert.subcategory_name')[$_SESSION['lang']] }}</label>
                <div>
                    <input type="text" class="form-control" name="name" value="{{ $subcategory->name }}" placeholder="Sub category name">
                </div>
            </div>
            <div class="form-group has-success">
                <label class="control-label" style="font-size: 120%" for="name">{{ config('Convert.subcategory_icon')[$_SESSION['lang']] }}</label>
                <div>
                    <div class="form-group">
                        <div class="form-group">
                            <div class="fileinput fileinput-exists" data-provides="fileinput">
                                <div class="fileinput-new thumbnail" style="width: 200px; min-height: 200px;">
                                    <img data-src="holder.js/100%x100%"></div>
                                <div class="fileinput-preview thumbnail" style="max-width: 200px; min-height: 200px; line-height: 210px;">
                                    <img src="/uploads/categories/{{ $subcategory->icon }}" style="max-width: 200px; min-height: 200px;">
                                </div>
                                <div>
                                                <span class="btn btn-default btn-file">
                                                    <span class="fileinput-new">{{ config('Convert.select_image')[$_SESSION['lang']] }}</span>
                                                    <span class="fileinput-exists">{{ config('Convert.change')[$_SESSION['lang']] }}</span>
                                                    <input type="hidden" value="" name=""><input type="file" name="photo"></span>
                                    <a href="#" class="btn btn-default fileinput-exists" data-dismiss="fileinput">{{ config('Convert.remove')[$_SESSION['lang']] }}</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-12 mar-10" style="padding-top:30px;">
                <input type="submit" value="{{ config('Convert.add_subcategory')[$_SESSION['lang']] }}" style="font-size: 120%" class="btn btn-success btn-block btn-md btn-responsive">
            </div>
        </form>
    </div>

@stop

{{-- page level scripts --}}
@section('footer_scripts')

    <script src="{{asset('assets/vendors/tinymce/tinymce.min.js')}}" type="text/javascript"></script>
    <script  src="{{ asset('assets/vendors/ckeditor/js/ckeditor.js') }}"  type="text/javascript"></script>
    <script  src="{{ asset('assets/vendors/ckeditor/js/jquery.js') }}"  type="text/javascript" ></script>
    <script  src="{{ asset('assets/vendors/ckeditor/js/config.js') }}"  type="text/javascript"></script>
    <script  src="{{ asset('assets/js/pages/editor.js') }}"  type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/jasny-bootstrap/js/jasny-bootstrap.js') }}" ></script>

@stop
