@extends('admin/layouts/default')

{{-- Page title --}}
@section('title')
    {{ config('Convert.category_groups')[$_SESSION['lang']] }}
    @parent
@stop

{{-- page level styles --}}
@section('header_styles')
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/dataTables.bootstrap.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/buttons.bootstrap.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/colReorder.bootstrap.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/dataTables.bootstrap.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/rowReorder.bootstrap.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/buttons.bootstrap.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/scroller.bootstrap.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/pages/tables.css') }}" />
@stop


{{-- Page content --}}
@section('content')
    <section class="content-header">
        <h1>{{ config('Convert.category_groups')[$_SESSION['lang']] }}</h1>
        <ol class="breadcrumb">
            <li>
                <a href="{{ route('dashboard') }}">
                    <i class="livicon" data-name="home" data-size="14" data-color="#000"></i>
                    {{ config('Convert.dashboard')[$_SESSION['lang']] }}
                </a>
            </li>
            <li><a href="#"> {{ config('Convert.groups')[$_SESSION['lang']] }}</a></li>
            <li class="active">{{ config('Convert.category_groups')[$_SESSION['lang']] }}</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content paddingleft_right15">
        <div class="row">
            <div class="panel panel-primary ">
                <div class="panel-heading">
                    <h4 class="panel-title"> <i class="livicon" data-name="user" data-size="16" data-loop="true" data-c="#fff" data-hc="white"></i>
                        {{ config('Convert.category_groups')[$_SESSION['lang']] }}
                    </h4>
                </div>
                <br />
                <div class="panel-body">
                    <table class="table table-striped table-bordered table-hover dataTable no-footer sample_editable"
                           id="grouptable" role="grid">
                        <thead>
                        <tr role="row">
                            <th class="sorting_asc"  tabindex="0" aria-controls="paidtable" rowspan="1" te
                                colspan="1"  aria-label="
                                                   ID
                                            : activate to sort column ascending" style="width: 30px;">{{ config('Convert.id')[$_SESSION['lang']] }}
                            </th>
                            <th class="sorting_asc" tabindex="0" aria-controls="paidtable" rowspan="1"
                                colspan="1"  aria-label="
                                                   master
                                            : activate to sort column ascending" style="width: 100px;">{{ config('Convert.group_master')[$_SESSION['lang']] }}
                            </th>
                            <th class="sorting_asc" tabindex="0" aria-controls="paidtable" rowspan="1"
                                colspan="1"  aria-label="
                                                   Title
                                            : activate to sort column ascending" style="width: 100px;">{{ config('Convert.group_title')[$_SESSION['lang']] }}
                            </th>
                            <th class="sorting_asc" tabindex="0" aria-controls="paidtable" rowspan="1"
                                colspan="1"  aria-label="
                                                   Description
                                            : activate to sort column ascending" style="width: 100px;">{{ config('Convert.group_description')[$_SESSION['lang']] }}
                            </th>
                            <th class="sorting_asc" tabindex="0" aria-controls="paidtable" rowspan="1"
                                colspan="1"  aria-label="
                                                   Staffs
                                            : activate to sort column ascending" style="width: 100px;">{{ config('Convert.group_staffs')[$_SESSION['lang']] }}
                            </th>
                            <th class="sorting" tabindex="0" aria-controls="paidtable" rowspan="1"
                                colspan="1" aria-label="
                                                Created At
                                            : activate to sort column ascending" style="width: 80px;">{{ config('Convert.create_date')[$_SESSION['lang']] }}
                            </th>
                            <th class="sorting_asc" tabindex="0" aria-controls="paidtable" rowspan="1"
                                colspan="1"  aria-label="
                                                   Actions
                                            : activate to sort column ascending" style="width:100px;">{{ config('Convert.actions')[$_SESSION['lang']] }}
                            </th>
                        </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>    <!-- row-->
    </section>
@stop

{{-- page level scripts --}}
@section('footer_scripts')
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/jquery.dataTables.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.bootstrap.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.buttons.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.colReorder.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.responsive.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.rowReorder.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/buttons.colVis.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/buttons.html5.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/buttons.print.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/buttons.bootstrap.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/buttons.print.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/pdfmake.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/vfs_fonts.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.scroller.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/select2/js/select2.js') }}"></script>

    <script>
        var table = null;
        $(function() {
            var nEditing = null;
            table = $('#grouptable').DataTable({
                processing: true,
                serverSide: true,
                ajax: '{!! route('admin.catgroups.data') !!}',
                columns: [
                    { data: 'id', name: 'id' },
                    { data: 'username', name: 'username' , searchable: false},
                    { data: 'title', name: 'title' },
                    { data: 'description', name: 'description' },
                    { data: 'user_info', name: 'user_info' , orderable: false, searchable: false},
                    { data: 'created_at', name:'created_at', searchable: false},
                    { data: 'delete', name: 'delete', orderable: false, searchable: false }
                ]
            });
            table.on( 'draw', function () {
                $('.livicon').each(function(){
                    $(this).updateLivicon();
                });
            } );
        });

    </script>


    <!-- delete modal -->
    <div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content" style="width: 80%">
                <input type="hidden" id="deleteid" value="0">
                <div class="modal-body">
                    {{ config('Convert.category_group_delete_message')[$_SESSION['lang']] }}
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" class="form-control" data-dismiss="modal" onclick="deleteData()">{{ config('Convert.delete')[$_SESSION['lang']] }}</button>
                    <button type="button" class="btn btn-default" class="form-control" data-dismiss="modal">{{ config('Convert.cancel')[$_SESSION['lang']] }}</button>
                </div>
            </div>
        </div>
    </div>

    <script>
        $(function () {
            $('body').on('hidden.bs.modal', '.modal', function () {
                $(this).removeData('bs.modal');
            });
        });

        function deleteData(){
            var id = $('#deleteid').val();
            /* delete row in database and datatable  */
            $.ajax({
                type: "get",
                url: '/admin/catgroups/' + id + '/delete',
                success: function (result) {
                    if(result == ''){
                        $('#messagecontent').html('Successfully deleted.');
                    }else{
                        $('#messagecontent').html('This is using in '+result);
                    }
                    $('#messageModal').modal('show');
                    //location.reload();
                    table.draw(false);
                },
                error: function (result) {
                    console.log(result)

                }
            });
        }
        function deleteItem(id){
            $('#deleteid').val(id);
        }
    </script>
@stop
