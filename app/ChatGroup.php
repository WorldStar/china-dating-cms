<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class ChatGroup extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'date_chatting_groups';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];

}