<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class GroupMaster extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'date_chatting_group_masters';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];

}