<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Business extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'date_business';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];

}