<?php

namespace App\Http\Controllers;

use App\City;
use App\Http\Requests;
use Illuminate\Http\Request;
use Datatables;
use DB;
use App\Http\Controllers\sweetAlert;
use App\SubPrice;

class SubPriceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */

    public function getlist()
    {
        $tables = SubPrice::orderBy('id', 'asc')->get();
        return View('admin/vip/subprice', compact('tables'));
    }

    public function getData()
    {
        $tables = SubPrice::select(['id', 'name',  'status',  'created_at'])->orderby('name', 'asc');

        return Datatables::of($tables)
            ->edit_column('status', function ($data) {
                if ($data->status == 1) {
                    $status = config('Convert.active')[$_SESSION['lang']];
                    return '<a style="color: #ca0002" class="active" href="javascript:;">' . $status . '</a>';
                    //return '<a onmouseover="this.style.color=\'#0618d8\" onMouseOut="this.style.color=\'#d80b06\'"> '. $status .'</span>';
                } else {
                    $status = config('Convert.inactive')[$_SESSION['lang']];
                    return '<a class="inactive" href="javascript:;">' . $status . '</a>';
                    //return '<a onmouseover="this.style.color=\'#0618d8\'" onMouseOut="this.style.color=\'#d80b06\'" class="active" href="javascript:;">' . $status . '</a>';
                }
            })
            ->add_column('edit', '<a class="edit" href="javascript:;">'.config('Convert.edit')[$_SESSION['lang']].'</a>')
            ->add_column('delete', '<a class="delete" href="javascript:;">'.config('Convert.delete')[$_SESSION['lang']].'</a>')
            ->make(true);
    }

    public function updateData(Request $request, $id)
    {
        $table = SubPrice::find($id);

        $table->update($request->except('_token'));
        return $table->id;
    }

    public function deleteData($id)
    {

        DB::table('date_sbuscribe_prices')->where('id', $id)->delete();

    }

    public function addData(Request $request)
    {
        $tables = array();
        if ($request->ajax()) {

            $tables = SubPrice::create($request->except('_token'));
        }
        return $tables;
    }

    public function activeData($id = 0)
    {
        DB::table('date_subscribe_prices')->where('id', $id)->update(['status'=>0]);
        $data = array("status"=>0);
        return $data;
    }

    public function inactiveData($id = 0)
    {
        DB::table('date_subscribe_prices')->where('id', $id)->update(['status'=>1]);
        $data = array("status"=>1);
        return $data;
    }
}

