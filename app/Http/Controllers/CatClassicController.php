<?php

namespace App\Http\Controllers;

use App\City;
use App\Http\Requests;
use Illuminate\Http\Request;
use Datatables;
use DB;
use App\Http\Controllers\sweetAlert;
use App\Category;
use App\SubCategory;
use App\CatClassic;

class CatClassicController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */


    public function getsublist(Request $request)
    {
        $cat_id = $request->get('cat_id', 0);
        $subcat_id = $request->get('subcat_id', 0);
        //if($cat_id)
        //$tables = SubCategory::orderBy('id', 'asc')->get();

        if($cat_id == 0){
            $cat = DB::table('date_category')->orderby('id', 'asc')->first();
            if(!empty($cat)){
                $cat_id = $cat->id;
            }
        }
        $categories = Category::orderBy('id', 'asc')->lists('name','id');
        if($cat_id != 0){
            $subcategories = SubCategory::orderBy('id', 'asc')->where('cat_id', $cat_id)->lists('name','id');
        }else{
            $subcategories = SubCategory::orderBy('id', 'asc')->lists('name','id');
        }
        if($cat_id != 0){
            $subcat = DB::table('date_subcategory')->where('cat_id', $cat_id)->orderby('id', 'asc')->first();
            if(!empty($subcat)){
                $subcat_id = $subcat->id;
            }
        }
        return View('admin/category/classic', compact('cat_id', 'subcat_id', 'categories', 'subcategories'));
    }
    public function getClassicList($cat_id = 0, $subcat_id = 0, Request $request)
    {
        //if($cat_id)
        //$tables = SubCategory::orderBy('id', 'asc')->get();
        $categories = Category::orderBy('id', 'asc')->lists('name','id');
        if($cat_id != 0){
            $subcategories = SubCategory::orderBy('id', 'asc')->where('cat_id', $cat_id)->lists('name','id');
        }else{
            $subcategories = SubCategory::orderBy('id', 'asc')->lists('name','id');
        }
        return View('admin/category/classic', compact('cat_id', 'subcat_id', 'categories', 'subcategories'));
    }
    public function getClassicItem($classic_id = 0){
        $classic = CatClassic::find($classic_id);
        return json_encode($classic);
    }
    public function getsubData($cat_id = 0, $subcat_id = 0, Request $request)
    {
        if($cat_id != 0 && $subcat_id == 0){
            $subcat = DB::table('date_subcategory')->where('cat_id', $cat_id)->orderby('id', 'asc')->first();
            if(!empty($subcat)){
                $subcat_id = $subcat->id;
            }
        }
        if($cat_id == 0 && $subcat_id == 0) {
            $tables = CatClassic::select(\DB::raw('date_cat_classics.*, date_category.name as cat_name, date_subcategory.name as subcat_name'))
                ->leftJoin('date_category', 'date_category.id', '=', 'date_cat_classics.cat_id')
                ->leftJoin('date_subcategory', 'date_subcategory.id', '=', 'date_cat_classics.subcat_id')
                ->orderby('date_cat_classics.cat_id', 'asc');
        }else{
            $tables = CatClassic::select(\DB::raw('date_cat_classics.*, date_category.name as cat_name, date_subcategory.name as subcat_name'))
                ->leftJoin('date_category', 'date_category.id', '=', 'date_cat_classics.cat_id')
                ->leftJoin('date_subcategory', 'date_subcategory.id', '=', 'date_cat_classics.subcat_id')
                ->where('date_cat_classics.cat_id', $cat_id)
                ->where('date_cat_classics.subcat_id', $subcat_id)
                ->orderby('date_cat_classics.id', 'asc');
        }
        return Datatables::of($tables)
            ->edit_column('status', function ($data) {
                if ($data->status == 1) {
                    $status = config('Convert.active')[$_SESSION['lang']];
                    return '<a style="color: #ca0002" class="active" href="javascript:;">' . $status . '</a>';
                    //return '<a onmouseover="this.style.color=\'#0618d8\" onMouseOut="this.style.color=\'#d80b06\'"> '. $status .'</span>';
                } else {
                    $status = config('Convert.inactive')[$_SESSION['lang']];
                    return '<a class="inactive" href="javascript:;">' . $status . '</a>';
                    //return '<a onmouseover="this.style.color=\'#0618d8\'" onMouseOut="this.style.color=\'#d80b06\'" class="active" href="javascript:;">' . $status . '</a>';
                }
            })
            ->add_column('edit', '<a class="edit" href="javascript:;">'.config('Convert.edit')[$_SESSION['lang']].'</a>')
            ->add_column('delete', '<a class="delete" href="javascript:;">'.config('Convert.delete')[$_SESSION['lang']].'</a>')
            ->make(true);
    }

    public function updatesubData(Request $request, $id)
    {
        $table = CatClassic::find($id);

        $table->update($request->except('_token'));
        return $table->id;
    }

    public function deletesubData($id)
    {

        DB::table('date_cat_classics')->where('id', $id)->delete();

    }

    public function addsubData(Request $request)
    {
        $tables = array();
        if ($request->ajax()) {

            $tables = CatClassic::create($request->except('_token'));
        }
        return $tables;
    }

    public function activesubData($id = 0, Request $request)
    {
        DB::table('date_cat_classics')->where('id', $id)->update(['status'=>0]);
        $data = array("status"=>0);
        return $data;
    }

    public function inactivesubData($id = 0, Request $request)
    {
        DB::table('date_cat_classics')->where('id', $id)->update(['status'=>1]);
        $data = array("status"=>1);
        return $data;
    }
}

