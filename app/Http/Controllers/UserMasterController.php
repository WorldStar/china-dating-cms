<?php

namespace App\Http\Controllers;

use App\City;
use App\Http\Requests;
use Illuminate\Http\Request;
use Datatables;
use DB;
use App\Http\Controllers\sweetAlert;
use App\UserMaster;

class UserMasterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */

    public function getlist()
    {
        return View('admin/masters/usermaster');
    }

    public function getData()
    {
        $tables = UserMaster::select(['date_user_masters.id', 'users.username as username', 'date_category.name as cat_name', 'date_subcategory.name as subcat_name', 'date_subcategory.icon as cat_icon', 'date_user_masters.user_id', 'date_user_masters.price', 'date_user_masters.action', 'date_user_masters.status', 'date_user_masters.created_at'])
            ->leftJoin('users', 'users.id', '=', 'date_user_masters.user_id')
            ->leftJoin('date_subcategory', 'date_subcategory.id', '=', 'date_user_masters.subcat_id')
            ->leftJoin('date_category', 'date_category.id', '=', 'date_user_masters.cat_id')
            ->where('date_user_masters.action', 0)
            ->orderby('date_user_masters.id', 'desc');

        return Datatables::of($tables)
            ->edit_column('status', function ($data) {
                if ($data->status == 1) {
                    $status = config('Convert.active')[$_SESSION['lang']];
                    return '<a style="color: #ca0002" class="active" href="javascript:;">' . $status . '</a>';
                    //return '<a onmouseover="this.style.color=\'#0618d8\" onMouseOut="this.style.color=\'#d80b06\'"> '. $status .'</span>';
                } else {
                    $status = config('Convert.inactive')[$_SESSION['lang']];
                    return '<a class="inactive" href="javascript:;">' . $status . '</a>';
                    //return '<a onmouseover="this.style.color=\'#0618d8\'" onMouseOut="this.style.color=\'#d80b06\'" class="active" href="javascript:;">' . $status . '</a>';
                }
            })
            ->edit_column('cat_icon', function ($data) {
                $url = '/uploads/categories/noimage.png';
                if($data->cat_icon != ''){
                    $url = '/uploads/categories/'.$data->cat_icon;
                }
                return '<img src="'.$url.'" style="width:40px;height:40px;border-radius:50%">';
            })
            ->edit_column('price', function ($data) {

                return $data->price;
            })
            ->edit_column('subcat_name', function ($data) {
                return ''.$data->cat_name.'∙'.$data->subcat_name;
            })
            ->edit_column('action', function ($data) {
                $url = url('/admin/customers/book/'.$data->user_id);
                return '<a href="'.$url.'">'.config('Convert.to_master')[$_SESSION['lang']].'</a>';
            })
            ->edit_column('username', function ($data) {
                $url = '';
                $url = url('/admin/customers/book/'.$data->user_id);
                return '<a href="'.$url.'">'.$data->username.'</a>';
            })
            ->add_column('delete', function ($data) {

                return '<a class="delete" href = "javascript:;" ><i class="livicon" data-name = "trash" data-size = "18" data-loop = "true" data-c = "#f56954" data-hc = "#f56954" title = "inactive news" ></i ></a > ';
            })
            ->make(true);
    }

    public function deleteData($id)
    {

        DB::table('date_user_masters')->where('id', $id)->delete();

    }

    public function activeData($id = 0)
    {
        DB::table('date_user_masters')->where('id', $id)->update(['status'=>0]);
        $data = array("status"=>0);
        return $data;
    }

    public function inactiveData($id = 0)
    {
        DB::table('date_user_masters')->where('id', $id)->update(['status'=>1]);
        $data = array("status"=>1);
        return $data;
    }
}

