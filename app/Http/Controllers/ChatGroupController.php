<?php

namespace App\Http\Controllers;

use App\City;
use App\Http\Requests;
use Illuminate\Http\Request;
use Datatables;
use DB;
use App\Http\Controllers\sweetAlert;
use App\Category;
use App\GroupMaster;
use App\ChatGroup;
use Illuminate\Support\Facades\Redirect;
use Validator;
use URL;

class ChatGroupController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */


    public function getcatlist(Request $request)
    {
        $today = date('Y-m-d');
        return View('admin/groups/catgroups');
    }
    public function getchatlist(Request $request)
    {
        $today = date('Y-m-d');
        return View('admin/groups/chatgroups');
    }
    public function getCatData(Request $request)
    {

        $tables = GroupMaster::select(['role_users.role_id', 'date_chatting_group_masters.id', 'date_chatting_group_masters.user_id', 'date_chatting_group_masters.title', 'date_chatting_group_masters.description', 'date_chatting_group_masters.cat_id', 'date_chatting_group_masters.subcat_id', 'date_chatting_group_masters.status', 'date_user_details.vip_id', 'date_chatting_group_masters.created_at', 'users.username'])
            ->leftJoin('users', 'users.id', '=', 'date_chatting_group_masters.user_id')
            ->leftJoin('date_user_details', 'date_user_details.user_id', '=', 'date_chatting_group_masters.user_id')
            ->leftJoin('role_users', 'role_users.user_id', '=', 'date_chatting_group_masters.user_id')
            ->where('date_chatting_group_masters.cat_id', '!=', 0)
            ->where('date_chatting_group_masters.subcat_id', '!=', 0)
            ->orderby('date_chatting_group_masters.id', 'desc');

        return Datatables::of($tables)
            ->edit_column('username', function ($data) {
                $url1 = '';
                if($data->role_id == 3){
                    $url1 = url('/admin/masters/'.$data->user_id);
                }else if($data->role_id == 4){
                    $url1 = url('/admin/customers/'.$data->user_id);
                }
                $vip_name = '';
                $dancer = '';
                $detail = DB::table('date_vips')->where('id', $data->vip_id)->first();
                if(!empty($detail)) $vip_name = '<span style="padding:3px 5px;background:#999;margin-right:10px;border-radius:3px;">'.$detail->name.'</span>';
                if($data->role_id == 3){
                    $masters = DB::table('date_user_masters')->where('user_id', $data->user_id)->where('status', 0)->get();

                    $url = '/uploads/categories/';
                    foreach($masters as $master){
                        $subcat = DB::table('date_subcategory')->where('cat_id', $master->cat_id)->where('id', $master->subcat_id)->first();
                        if(!empty($subcat)){
                            $p = $subcat->icon;
                            if($dancer == '') $dancer = '<img src="'.$url.''.$p.'" style="border-radius:50%;width:25px;height:25px;margin-right:10px;">';
                            else  $dancer .= '<img src="'.$url.''.$p.'" style="border-radius:50%;width:25px;height:25px;margin-right:10px;">';

                        }
                    }
                }

                $name =  '<a href="'.$url1.'">'.$data->username.'</a><br>'.$vip_name.' '.$dancer;
                return $name;
            })
            ->add_column('user_info', function ($data) {
                $staffs = ChatGroup::select(['role_users.role_id', 'date_chatting_groups.id', 'date_chatting_groups.user_id', 'date_user_details.vip_id', 'users.username'])
                    ->leftJoin('users', 'users.id', '=', 'date_chatting_groups.user_id')
                    ->leftJoin('date_user_details', 'date_user_details.user_id', '=', 'date_chatting_groups.user_id')
                    ->leftJoin('role_users', 'role_users.user_id', '=', 'date_chatting_groups.user_id')
                    ->where('date_chatting_groups.group_id',  $data->id)
                    ->get();
                $name = '';
                foreach($staffs as $staff){
                    $url = '';
                    $vip_name = '';
                    $dancer = '';
                    $detail = DB::table('date_vips')->where('id', $staff->vip_id)->first();
                    if(!empty($detail)) $vip_name = '<span style="padding:3px 5px;background:#999;margin-right:10px;border-radius:3px;">'.$detail->name.'</span>';
                    if($data->role_id == 3){
                        $masters = DB::table('date_user_masters')->where('user_id', $staff->user_id)->where('status', 0)->get();

                        $url = '/uploads/categories/';
                        foreach($masters as $master){
                            $subcat = DB::table('date_subcategory')->where('cat_id', $master->cat_id)->where('id', $master->subcat_id)->first();
                            if(!empty($subcat)){
                                $p = $subcat->icon;
                                if($dancer == '') $dancer = '<img src="'.$url.''.$p.'" style="border-radius:50%;width:25px;height:25px;margin-right:10px;">';
                                else  $dancer .= '<img src="'.$url.''.$p.'" style="border-radius:50%;width:25px;height:25px;margin-right:10px;">';

                            }
                        }
                    }
                    if($staff->role_id == 3){
                        $url = url('/admin/masters/'.$staff->user_id);
                    }else if($staff->role_id == 4){
                        $url = url('/admin/customers/'.$staff->user_id);
                    }
                    $name .=  '<a href="'.$url.'">'.$staff->username.'</a><br>'.$vip_name.' '.$dancer.'<br>';

                }

                return $name;
            })
            ->add_column('delete', function ($data) {

                return '<a class="delete" href = ""  data-toggle="modal" data-target="#deleteModal" onclick="deleteItem('.$data->id.')"><i class="livicon" data-name = "trash" data-size = "18" data-loop = "true" data-c = "#f56954" data-hc = "#f56954" title = "delete group" ></i ></a > ';
            })
            ->make(true);
    }

    public function deleteCatData($id)
    {

        DB::table('date_chatting_groups')->where('group_id', $id)->delete();
        DB::table('date_chatting_group_masters')->where('id', $id)->delete();

    }
    public function getChatData(Request $request)
    {

        $tables = GroupMaster::select(['role_users.role_id', 'date_chatting_group_masters.id', 'date_chatting_group_masters.type', 'date_group_types.name as type_name', 'date_chatting_group_masters.user_id', 'date_chatting_group_masters.title', 'date_chatting_group_masters.description', 'date_chatting_group_masters.cat_id', 'date_chatting_group_masters.subcat_id', 'date_chatting_group_masters.status', 'date_user_details.vip_id', 'date_chatting_group_masters.created_at', 'users.username'])
            ->leftJoin('users', 'users.id', '=', 'date_chatting_group_masters.user_id')
            ->leftJoin('date_user_details', 'date_user_details.user_id', '=', 'date_chatting_group_masters.user_id')
            ->leftJoin('role_users', 'role_users.user_id', '=', 'date_chatting_group_masters.user_id')
            ->leftJoin('date_group_types', 'date_group_types.id', '=', 'date_chatting_group_masters.type')
            ->where('date_chatting_group_masters.cat_id', 0)
            ->where('date_chatting_group_masters.subcat_id', 0)
            ->orderby('date_chatting_group_masters.id', 'desc');

        return Datatables::of($tables)
            ->edit_column('username', function ($data) {
                $url1 = '';
                if($data->role_id == 3){
                    $url1 = url('/admin/masters/'.$data->user_id);
                }else if($data->role_id == 4){
                    $url1 = url('/admin/customers/'.$data->user_id);
                }
                $vip_name = '';
                $dancer = '';
                $detail = DB::table('date_vips')->where('id', $data->vip_id)->first();
                if(!empty($detail)) $vip_name = '<span style="padding:3px 5px;background:#999;margin-right:10px;border-radius:3px;">'.$detail->name.'</span>';
                if($data->role_id == 3){
                    $masters = DB::table('date_user_masters')->where('user_id', $data->user_id)->where('status', 0)->get();

                    $url = '/uploads/categories/';
                    foreach($masters as $master){
                        $subcat = DB::table('date_subcategory')->where('cat_id', $master->cat_id)->where('id', $master->subcat_id)->first();
                        if(!empty($subcat)){
                            $p = $subcat->icon;
                            if($dancer == '') $dancer = '<img src="'.$url.''.$p.'" style="border-radius:50%;width:25px;height:25px;margin-right:10px;">';
                            else  $dancer .= '<img src="'.$url.''.$p.'" style="border-radius:50%;width:25px;height:25px;margin-right:10px;">';

                        }
                    }
                }

                $name =  '<a href="'.$url1.'">'.$data->username.'</a><br>'.$vip_name.' '.$dancer;
                return $name;
            })
            ->add_column('user_info', function ($data) {
                $staffs = ChatGroup::select(['role_users.role_id', 'date_chatting_groups.id', 'date_chatting_groups.user_id', 'date_user_details.vip_id', 'users.username'])
                    ->leftJoin('users', 'users.id', '=', 'date_chatting_groups.user_id')
                    ->leftJoin('date_user_details', 'date_user_details.user_id', '=', 'date_chatting_groups.user_id')
                    ->leftJoin('role_users', 'role_users.user_id', '=', 'date_chatting_groups.user_id')
                    ->where('date_chatting_groups.group_id',  $data->id)
                    ->get();
                $name = '';
                foreach($staffs as $staff){
                    $url = '';
                    $vip_name = '';
                    $dancer = '';
                    $detail = DB::table('date_vips')->where('id', $staff->vip_id)->first();
                    if(!empty($detail)) $vip_name = '<span style="padding:3px 5px;background:#999;margin-right:10px;border-radius:3px;">'.$detail->name.'</span>';
                    if($data->role_id == 3){
                        $masters = DB::table('date_user_masters')->where('user_id', $staff->user_id)->where('status', 0)->get();

                        $url = '/uploads/categories/';
                        foreach($masters as $master){
                            $subcat = DB::table('date_subcategory')->where('cat_id', $master->cat_id)->where('id', $master->subcat_id)->first();
                            if(!empty($subcat)){
                                $p = $subcat->icon;
                                if($dancer == '') $dancer = '<img src="'.$url.''.$p.'" style="border-radius:50%;width:25px;height:25px;margin-right:10px;">';
                                else  $dancer .= '<img src="'.$url.''.$p.'" style="border-radius:50%;width:25px;height:25px;margin-right:10px;">';

                            }
                        }
                    }
                    if($staff->role_id == 3){
                        $url = url('/admin/masters/'.$staff->user_id);
                    }else if($staff->role_id == 4){
                        $url = url('/admin/customers/'.$staff->user_id);
                    }
                    $name .=  '<a href="'.$url.'">'.$staff->username.'</a><br>'.$vip_name.' '.$dancer.'<br>';

                }

                return $name;
            })
            ->add_column('delete', function ($data) {

                return '<a class="delete" href = ""  data-toggle="modal" data-target="#deleteModal" onclick="deleteItem('.$data->id.')"><i class="livicon" data-name = "trash" data-size = "18" data-loop = "true" data-c = "#f56954" data-hc = "#f56954" title = "delete group" ></i ></a > ';
            })
            ->make(true);
    }

    public function deleteChatData($id)
    {

        DB::table('date_chatting_groups')->where('group_id', $id)->delete();
        DB::table('date_chatting_group_masters')->where('id', $id)->delete();

    }

}

