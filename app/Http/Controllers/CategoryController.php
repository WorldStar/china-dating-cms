<?php

namespace App\Http\Controllers;

use App\City;
use App\Http\Requests;
use Illuminate\Http\Request;
use Datatables;
use DB;
use App\Http\Controllers\sweetAlert;
use App\Category;
use App\SubCategory;
use Validator;
use Redirect;
use App\UserMaster;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */

    public function getlist()
    {
        $tables = Category::orderBy('id', 'asc')->get();

        return View('admin/category/category', compact('tables'));
    }

    public function getData()
    {
        $tables = Category::get(['id', 'name',  'status',  'created_at']);

        $tables = Category::select(\DB::raw('date_category.*, SUM(date_cat_favorites.favorite) as favorite'))
            ->leftJoin('date_cat_favorites', 'date_cat_favorites.cat_id', '=', 'date_category.id')
            ->groupBy('date_category.id')
            ->orderby('date_category.id', 'desc');
        return Datatables::of($tables)
            ->edit_column('status', function ($data) {
                if ($data->status == 1) {
                    $status = config('Convert.active')[$_SESSION['lang']];
                    return '<a style="color: #ca0002" class="active" href="javascript:;">' . $status . '</a>';
                    //return '<a onmouseover="this.style.color=\'#0618d8\" onMouseOut="this.style.color=\'#d80b06\'"> '. $status .'</span>';
                } else {
                    $status = config('Convert.inactive')[$_SESSION['lang']];
                    return '<a class="inactive" href="javascript:;">' . $status . '</a>';
                    //return '<a onmouseover="this.style.color=\'#0618d8\'" onMouseOut="this.style.color=\'#d80b06\'" class="active" href="javascript:;">' . $status . '</a>';
                }
            })
            ->edit_column('icon', function ($data) {
                $url = '/uploads/categories/noimage.png';
                if($data->icon != ''){
                    $url = '/uploads/categories/'.$data->icon;
                }
                return '<img src="'.$url.'" style="width:40px;height:auto;height:40px">';
            })
            ->add_column('edit', function ($data) {
                $status = config('Convert.edit')[$_SESSION['lang']];
                return '<a class="edit" href="/admin/category/edit/'.$data->id.'">'.$status.'</a>';
            })
            ->add_column('delete', '<a class="delete" href="javascript:;">'.config('Convert.delete')[$_SESSION['lang']].'</a>')
            ->make(true);
    }

    public function updateData(Request $request, $id= 0)
    {
        $rules = array(
            'name' => 'required',
            'photo' => 'image|mimes:jpg,jpeg,bmp,png',
        );
        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            // Ooops.. something went wrong
            return Redirect::to(URL::previous())->withInput()->withErrors($validator);
        }
        $safeName = '';
        if ($file = $request->file('photo')) {
            $fileName = $file->getClientOriginalName();
            $extension = $file->getClientOriginalExtension() ?: 'png';
            $folderName = '/uploads/categories';
            $destinationPath = public_path() . $folderName;
            $safeName = str_random(10) . '.' . $extension;
            $file->move($destinationPath, $safeName);
        }
        $category = Category::find($id);
        $category->name = $request->get('name', '');
        if($safeName != '')
            $category->icon = $safeName;
        $category->save();
        return Redirect::to("admin/category")->with('success', 'Successfully Updated.');
    }

    public function deleteData($id)
    {

        DB::table('date_category')->where('id', $id)->delete();

    }

    public function addData(Request $request)
    {
        $rules = array(
            'name' => 'required',
            'photo' => 'image|mimes:jpg,jpeg,bmp,png',
        );
        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            // Ooops.. something went wrong
            return Redirect::to(URL::previous())->withInput()->withErrors($validator);
        }
        $safeName = '';
        if ($file = $request->file('photo')) {
            $fileName = $file->getClientOriginalName();
            $extension = $file->getClientOriginalExtension() ?: 'png';
            $folderName = '/uploads/categories';
            $destinationPath = public_path() . $folderName;
            $safeName = str_random(10) . '.' . $extension;
            $file->move($destinationPath, $safeName);
        }
        $category = new Category();
        $category->name = $request->get('name', '');
        $category->icon = $safeName;
        $category->save();
        return Redirect::to("admin/category")->with('success', 'Successfully Added.');
    }
    public function create(Request $request)
    {
        return View('admin/category/create');
    }
    public function edit($id = 0, Request $request)
    {
        $category = Category::find($id);
        return View('admin/category/edit', compact('category'));
    }

    public function activeData($id = 0)
    {
        DB::table('date_category')->where('id', $id)->update(['status'=>0]);
        $data = array("status"=>0);
        return $data;
    }

    public function inactiveData($id = 0)
    {
        DB::table('date_category')->where('id', $id)->update(['status'=>1]);
        $data = array("status"=>1);
        return $data;
    }
    public function getsublist(Request $request)
    {
        $cat_id = $request->get('cat_id', 0);
        //if($cat_id)
        //$tables = SubCategory::orderBy('id', 'asc')->get();
        $categories = Category::orderBy('id', 'asc')->lists('name','id');
        if($cat_id == 0){
            $category = Category::orderby('id', 'asc')->first();
            if(!empty($category)) $cat_id = $category->id;
        }
        return View('admin/category/subcategory', compact('cat_id', 'categories'));
    }
    public function getcategoryList($cat_id = 0, Request $request)
    {
        //if($cat_id)
        //$tables = SubCategory::orderBy('id', 'asc')->get();
        $categories = Category::orderBy('id', 'asc')->lists('name','id');
        return View('admin/category/subcategory', compact('cat_id', 'categories'));
    }
    public function getsubcategoryItem($subcat_id = 0){
        $subcategory = SubCategory::find($subcat_id);
        return json_encode($subcategory);
    }
    public function getsubData($cat_id = 0, Request $request)
    {
        if($cat_id == 0) {
            $tables = SubCategory::select(\DB::raw('date_subcategory.*, date_category.name as cat_name, SUM(date_cat_favorites.favorite) as favorite'))
                ->leftJoin('date_cat_favorites', 'date_cat_favorites.subcat_id', '=', 'date_subcategory.id')
                ->leftJoin('date_category', 'date_category.id', '=', 'date_subcategory.cat_id')
                ->groupBy('date_subcategory.id')
                ->orderby('date_subcategory.cat_id', 'asc');
        }else{
            $tables = SubCategory::select(\DB::raw('date_subcategory.*, date_category.name as cat_name, SUM(date_cat_favorites.favorite) as favorite'))
                ->leftJoin('date_cat_favorites', 'date_cat_favorites.subcat_id', '=', 'date_subcategory.id')
                ->leftJoin('date_category', 'date_category.id', '=', 'date_subcategory.cat_id')
                ->groupBy('date_subcategory.id')
                ->where('date_subcategory.cat_id', $cat_id)
                ->orderby('date_subcategory.id', 'asc');
        }
        return Datatables::of($tables)
            ->edit_column('status', function ($data) {
                if ($data->status == 1) {
                    $status = config('Convert.active')[$_SESSION['lang']];
                    return '<a style="color: #ca0002" class="active" href="javascript:;">' . $status . '</a>';
                    //return '<a onmouseover="this.style.color=\'#0618d8\" onMouseOut="this.style.color=\'#d80b06\'"> '. $status .'</span>';
                } else {
                    $status = config('Convert.inactive')[$_SESSION['lang']];
                    return '<a class="inactive" href="javascript:;">' . $status . '</a>';
                    //return '<a onmouseover="this.style.color=\'#0618d8\'" onMouseOut="this.style.color=\'#d80b06\'" class="active" href="javascript:;">' . $status . '</a>';
                }
            })
            ->edit_column('icon', function ($data) {
                $url = '/uploads/categories/noimage.png';
                if($data->icon != ''){
                    $url = '/uploads/categories/'.$data->icon;
                }
                return '<img src="'.$url.'"  style="width:40px;height:auto;height:40px">';
            })
            ->add_column('edit', function ($data) {
                $strTmp = config('Convert.edit')[$_SESSION['lang']];
                return '<a class="edit" href="/admin/subcategory/edit/'.$data->id.'">'.$strTmp.'</a>';
            })
            ->add_column('delete', '<a class="delete" href="javascript:;">'.config('Convert.delete')[$_SESSION['lang']].'</a>')
            ->make(true);
    }

    public function updatesubData(Request $request, $id = 0)
    {
        $rules = array(
            'name' => 'required',
            'photo' => 'image|mimes:jpg,jpeg,bmp,png',
        );
        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            // Ooops.. something went wrong
            return Redirect::to(URL::previous())->withInput()->withErrors($validator);
        }
        $safeName = '';
        if ($file = $request->file('photo')) {
            $fileName = $file->getClientOriginalName();
            $extension = $file->getClientOriginalExtension() ?: 'png';
            $folderName = '/uploads/categories';
            $destinationPath = public_path() . $folderName;
            $safeName = str_random(10) . '.' . $extension;
            $file->move($destinationPath, $safeName);
        }
        $subcategory = SubCategory::find($id);
        $subcategory->name = $request->get('name', '');
        if($safeName != '')
            $subcategory->icon = $safeName;
        $subcategory->save();
        return Redirect::to("admin/subcategorylist/".$subcategory->cat_id)->with('success', 'Successfully Updated.');
    }

    public function deletesubData($id)
    {

        DB::table('date_subcategory')->where('id', $id)->delete();

    }
    public function subcreate($cat_id = 0, Request $request)
    {
        $category = Category::find($cat_id);
        return View('admin/category/sub_create', compact('category'));
    }
    public function subedit($id = 0, Request $request)
    {
        $subcategory = SubCategory::find($id);
        $category = Category::find($subcategory->cat_id);
        return View('admin/category/sub_edit', compact('category', 'subcategory'));
    }

    public function addsubData(Request $request)
    {
        $rules = array(
            'cat_id' => 'required',
            'name' => 'required',
            'photo' => 'image|mimes:jpg,jpeg,bmp,png',
        );
        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            // Ooops.. something went wrong
            return Redirect::to(URL::previous())->withInput()->withErrors($validator);
        }
        $safeName = '';
        if ($file = $request->file('photo')) {
            $fileName = $file->getClientOriginalName();
            $extension = $file->getClientOriginalExtension() ?: 'png';
            $folderName = '/uploads/categories';
            $destinationPath = public_path() . $folderName;
            $safeName = str_random(10) . '.' . $extension;
            $file->move($destinationPath, $safeName);
        }
        $subcategory = new SubCategory();
        $subcategory->cat_id = $request->get('cat_id', 0);
        $subcategory->name = $request->get('name', '');
        $subcategory->icon = $safeName;
        $subcategory->save();
        return Redirect::to("admin/subcategorylist/".$request->get('cat_id', 0))->with('success', 'Successfully Added.');
    }

    public function activesubData($id = 0, Request $request)
    {
        DB::table('date_subcategory')->where('id', $id)->update(['status'=>0]);
        $data = array("status"=>0);
        return $data;
    }

    public function inactivesubData($id = 0, Request $request)
    {
        DB::table('date_subcategory')->where('id', $id)->update(['status'=>1]);
        $data = array("status"=>1);
        return $data;
    }
    public function updateMaster(Request $request)
    {
        $rules = array(
            'um_id' => 'required',
            'actionval' => 'required',
        );
        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            // Ooops.. something went wrong
            return 0;//Redirect::to(URL::previous())->withInput()->withErrors($validator);
        }
        $um_id = $request->get('um_id', 0);
        $usermaster = UserMaster::find($um_id);
        $usermaster->action = $request->get('actionval', 0);
        $usermaster->save();
        if($request->get('actionval', 0) == 1){
            DB::table('role_users')->where('user_id', $usermaster->user_id)->update(['role_id'=>3]);
        }
        return 1;
        //return Redirect::to(URL::previous())->withInput()->with('success', 'Successfully Added.');
    }
    public function updateHighMaster(Request $request)
    {
        $rules = array(
            'um_id' => 'required',
            'actionval' => 'required',
        );
        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            // Ooops.. something went wrong
            return 0;//Redirect::to(URL::previous())->withInput()->withErrors($validator);
        }
        $um_id = $request->get('um_id', 0);
        $usermaster = UserMaster::find($um_id);
        $usermaster->high_action = $request->get('actionval', 0);
        $usermaster->save();
        return 1;
        //return Redirect::to(URL::previous())->withInput()->with('success', 'Successfully Added.');
    }
}

