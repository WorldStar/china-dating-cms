<?php

namespace App\Http\Controllers;

use App\City;
use App\Http\Requests;
use Illuminate\Http\Request;
use Datatables;
use DB;
use App\Http\Controllers\sweetAlert;
use App\Category;
use App\PaymentMethod;
use Illuminate\Support\Facades\Redirect;
use Validator;
use URL;

class FilterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */


    public function getlist($num = 0, Request $request)
    {
        $cat_id = $request->get('cat_id', 0);
        $subcat_id = $request->get('subcat_id', 0);
        if($cat_id == 0) $subcat_id = 0;
        $vip_id = $request->get('vip_id', 0);
        $option_id = $request->get('option_id', 0);
        $type_id = $request->get('type_id', 0);
        $categories = DB::table('date_category')->orderby('id', 'asc')->lists('name', 'id');
        $categories = array('0'=>'Select Category') + $categories;
        $subcategories = DB::table('date_subcategory')->where('cat_id', $cat_id)->orderby('id', 'asc')->lists('name', 'id');
        $subcategories = array('0'=>'Select Sub Category') + $subcategories;
        $vips = DB::table('date_vips')->orderby('id', 'asc')->lists('name', 'id');
        $vips = array('0'=>'Select VIP') + $vips;
        $options = array('0'=>'Select', '3'=>'Master', '4'=>'Customer');
        $types = DB::table('date_point_types')->orderby('id', 'asc')->lists('name', 'id');
        $types = array('0'=>'Select Type') + $types;
        switch($num){
            case 1:
                return View('admin/filter/master', compact('num', 'categories', 'subcategories', 'vips', 'cat_id', 'subcat_id', 'vip_id'));
                break;
            case 2:
                return View('admin/filter/customer', compact('num', 'categories', 'subcategories', 'vips', 'cat_id', 'subcat_id', 'vip_id'));
                break;
            case 3:
                return View('admin/filter/payment', compact('num', 'categories', 'subcategories', 'vips', 'cat_id', 'subcat_id', 'vip_id', 'option_id', 'options'));
                break;
            case 4:
                return View('admin/filter/point', compact('num', 'categories', 'subcategories', 'vips', 'cat_id', 'subcat_id', 'vip_id', 'option_id', 'type_id', 'options', 'types'));
                break;
            case 5:
                $types = array('0'=>'Select', '1'=>'Now', '2'=>'Past');
                return View('admin/filter/live', compact('num', 'categories', 'subcategories', 'vips', 'cat_id', 'subcat_id', 'vip_id', 'type_id', 'types'));
                break;
            case 6:
                $types = array('0'=>'Select', '1'=>'Point', '2'=>'Audio', '3'=>'Video', '4'=>'Card', '5'=>'Photo', '6'=>'Text', '7'=>'Other');
                return View('admin/filter/chat', compact('num', 'categories', 'subcategories', 'vips', 'cat_id', 'subcat_id', 'vip_id', 'option_id', 'type_id', 'options', 'types'));
                break;
        }
        return Redirect::back();
    }
    public function getActivitylist(Request $request)
    {
        $cat_id = $request->get('cat_id', 0);
        $subcat_id = $request->get('subcat_id', 0);
        if($cat_id == 0) $subcat_id = 0;
        $vip_id = $request->get('vip_id', 0);
        $option_id = $request->get('option_id', 0);
        $type_id = $request->get('type_id', 0);
        $categories = DB::table('date_category')->orderby('id', 'asc')->lists('name', 'id');
        $categories = array('0'=>'Select Category') + $categories;
        $subcategories = DB::table('date_subcategory')->where('cat_id', $cat_id)->orderby('id', 'asc')->lists('name', 'id');
        $subcategories = array('0'=>'Select Sub Category') + $subcategories;
        $vips = DB::table('date_vips')->orderby('id', 'asc')->lists('name', 'id');
        $vips = array('0'=>'Select VIP') + $vips;
        $options = array('0'=>'Select', '3'=>'Master', '4'=>'Customer');
        $types = DB::table('date_point_types')->orderby('id', 'asc')->lists('name', 'id');
        $types = array('0'=>'Select Type') + $types;

        $types = array('0'=>'Select', '1'=>'Point', '2'=>'Audio', '3'=>'Video', '4'=>'Card', '5'=>'Photo', '6'=>'Text', '7'=>'Other');
        return View('admin/filter/activity', compact('num', 'categories', 'subcategories', 'vips', 'cat_id', 'subcat_id', 'vip_id', 'option_id', 'type_id', 'options', 'types'));
    }

}

