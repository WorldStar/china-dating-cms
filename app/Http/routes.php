<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

/**
 * Model binding into route
 */
Route::model('file', 'App\File');
Route::model('task', 'App\Task');
Route::model('users', 'App\User');

Route::pattern('slug', '[a-z0-9- _]+');

Route::group(array('prefix' => 'admin'), function () {

	# Error pages should be shown without requiring login
	Route::get('404', function () {
		return View('admin/404');
	});
	Route::get('500', function () {
		return View::make('admin/500');
	});

    Route::post('secureImage', array('as' => 'secureImage','uses' => 'JoshController@secureImage'));
    
	# Lock screen
	Route::get('{id}/lockscreen', array('as' => 'lockscreen', 'uses' =>'UsersController@lockscreen'));

	# All basic routes defined here
	Route::get('signin', array('as' => 'signin', 'uses' => 'AuthController@getSignin'));
	Route::post('signin', 'AuthController@postSignin');
	Route::post('signup', array('as' => 'signup', 'uses' => 'AuthController@postSignup'));
	Route::post('forgot-password', array('as' => 'forgot-password', 'uses' => 'AuthController@postForgotPassword'));
	Route::get('login2', function () {
		return View::make('admin/login2');
	});

	# Register2
	Route::get('register2', function () {
		return View::make('admin/register2');
	});
	Route::post('register2', array('as' => 'register2', 'uses' => 'AuthController@postRegister2'));

	# Forgot Password Confirmation
	Route::get('forgot-password/{userId}/{passwordResetCode}', array('as' => 'forgot-password-confirm', 'uses' => 'AuthController@getForgotPasswordConfirm'));
	Route::post('forgot-password/{userId}/{passwordResetCode}', 'AuthController@postForgotPasswordConfirm');

	# Logout
	Route::get('logout', array('as' => 'logout', 'uses' => 'AuthController@getLogout'));

	# Account Activation
	Route::get('activate/{userId}/{activationCode}', array('as' => 'activate', 'uses' => 'AuthController@getActivate'));
});

Route::group(array('prefix' => 'admin', 'middleware' => 'SentinelUser'), function () {
    # Dashboard / Index
	Route::get('/', array('as' => 'dashboard','uses' => 'JoshController@showHome'));
	Route::get('/home', array('as' => 'home','uses' => 'JoshController@showHome'));
	Route::get('/setlang/{lang}', array('as' => 'home','uses' => 'JoshController@setlang'));


    # User Management
    Route::group(array('prefix' => 'users'), function () {
        Route::get('/', array('as' => 'users', 'uses' => 'UsersController@index'));
        Route::get('data',['as' => 'users.data', 'uses' =>'UsersController@data']);
        Route::get('create', 'UsersController@create');
        Route::post('create', 'UsersController@store');
        Route::get('{userId}/delete', array('as' => 'delete/user', 'uses' => 'UsersController@destroy'));
        Route::get('{userId}/confirm-delete', array('as' => 'confirm-delete/user', 'uses' => 'UsersController@getModalDelete'));
        Route::get('{userId}/restore', array('as' => 'restore/user', 'uses' => 'UsersController@getRestore'));
        Route::get('{userId}', array('as' => 'users.show', 'uses' => 'UsersController@show'));
        Route::post('{userId}/passwordreset', array('as' => 'passwordreset', 'uses' => 'UsersController@passwordreset'));
    });
    Route::resource('users', 'UsersController');

	Route::get('deleted_users',array('as' => 'deleted_users','before' => 'Sentinel', 'uses' => 'UsersController@getDeletedUsers'));
	Route::post('remove-noti',array('as' => 'remove-noti', 'uses' => 'MainController@getDelNoti'));


	# Master Management
	Route::group(array('prefix' => 'masters'), function () {
		Route::get('/', array('as' => 'masters', 'uses' => 'MastersController@index'));
		Route::get('data',['as' => 'masters.data', 'uses' =>'MastersController@data']);
		Route::get('create', 'MastersController@create');
		Route::post('create', 'MastersController@store');
		Route::get('{userId}/delete', array('as' => 'delete/master', 'uses' => 'MastersController@destroy'));
		Route::post('users/update', array('as' => 'users.masters.update', 'uses' => 'CategoryController@updateMaster'));
		Route::post('users/high/update', array('as' => 'users.highmasters.update', 'uses' => 'CategoryController@updateHighMaster'));
		Route::get('{userId}/confirm-delete', array('as' => 'confirm-delete/master', 'uses' => 'MastersController@getModalDelete'));
		Route::get('{userId}/restore', array('as' => 'restore/master', 'uses' => 'MastersController@getRestore'));
		Route::get('{userId}', array('as' => 'masters.show', 'uses' => 'MastersController@show'));
		Route::get('{userId}/edit', array('as' => 'admin.masters.edit1', 'uses' => 'MastersController@edit'));
		Route::post('update1', array('as' => 'admin.masters.update1', 'uses' => 'MastersController@edit'));
		Route::post('{userId}/passwordreset', array('as' => 'master.passwordreset', 'uses' => 'MastersController@passwordreset'));
	});
	Route::resource('masters', 'MastersController');

	Route::get('deleted_masters',array('as' => 'deleted_masters','before' => 'Sentinel', 'uses' => 'MastersController@getDeletedUsers'));

	# Customers Management
	Route::group(array('prefix' => 'customers'), function () {
		Route::get('/', array('as' => 'customers', 'uses' => 'CustomersController@index'));
		Route::get('data',['as' => 'customers.data', 'uses' =>'CustomersController@data']);
		Route::get('create', 'CustomersController@create');
		Route::post('create', 'CustomersController@store');
		Route::get('{userId}/delete', array('as' => 'delete/customer', 'uses' => 'CustomersController@destroy'));
		Route::get('{userId}/confirm-delete', array('as' => 'confirm-delete/customer', 'uses' => 'CustomersController@getModalDelete'));
		Route::get('{userId}/restore', array('as' => 'restore/customer', 'uses' => 'CustomersController@getRestore'));
		Route::get('{userId}', array('as' => 'customers.show', 'uses' => 'CustomersController@show'));
		Route::get('{userId}/edit', array('as' => 'admin.customers.edit1', 'uses' => 'CustomersController@edit'));
		Route::post('update1', array('as' => 'admin.customers.update1', 'uses' => 'CustomersController@edit'));
		Route::post('{userId}/passwordreset', array('as' => 'customer.passwordreset', 'uses' => 'CustomersController@passwordreset'));
		Route::get('/book/{userId}', array('as' => 'book.masters.show', 'uses' => 'MastersController@bookshow'));
	});
	Route::resource('customers', 'CustomersController');

	Route::get('deleted_customers',array('as' => 'deleted_customers','before' => 'Sentinel', 'uses' => 'CustomersController@getDeletedUsers'));


	/*routes for file*/
	Route::group(array('prefix' => 'file'), function () {
        Route::post('create', 'FileController@store');
		Route::post('createmulti', 'FileController@postFilesCreate');
		Route::delete('delete', 'FileController@delete');
	});

	Route::get('gethome', 'AdminGetController@getHome');
	Route::get('gethomeview/{uid}', 'AdminGetController@getHomeView');
	Route::get('getabout', 'AdminGetController@getAbout');
	Route::get('getaboutview/{uid}', 'AdminGetController@getAboutView');
	Route::get('getservice1', 'AdminGetController@getService1');
	Route::get('getserviceview1/{uid}', 'AdminGetController@getServiceView1');
	Route::get('getservice2', 'AdminGetController@getService2');
	Route::get('getserviceview2/{uid}', 'AdminGetController@getServiceView2');
	Route::get('getoption', 'AdminGetController@getOption');
	Route::get('getoptionview/{uid}', 'AdminGetController@getOptionView');
    //tasks section
    Route::post('task/create', 'TaskController@store');
    Route::get('task/data', 'TaskController@data');
    Route::post('task/{task}/edit', 'TaskController@update');
    Route::post('task/{task}/delete', 'TaskController@delete');


	/*****cities*****/

	Route::get('city', 'CityController@getlist');
	Route::get('city/data', array('as' => 'admin.city.data', 'uses' => 'CityController@cityData'));
	Route::post('city/addcity', array('as' => 'admin.city.add', 'uses' => 'CityController@addCity'));
	Route::post('city/{id}/update', 'CityController@updateCity');
	Route::get('city/{id}/delete', array('as' => 'admin.city.delete', 'uses' => 'CityController@deleteCity'));
	Route::get('city/{id}/inactivecity', 'CityController@inactiveCity');
	Route::get('city/{id}/activecity', 'CityController@activeCity');
	/****************/
	/*****Tags*****/
	Route::get('tag', 'TagController@getlist');
	Route::get('tag/data', array('as' => 'admin.tag.data', 'uses' => 'TagController@tagData'));
	Route::post('tag/addtag', array('as' => 'admin.tag.add', 'uses' => 'TagController@addTag'));
	Route::post('tag/{id}/update', 'TagController@updateTag');
	Route::get('tag/{id}/delete', array('as' => 'admin.tag.delete', 'uses' => 'TagController@deleteTag'));
	Route::get('tag/{id}/inactivetag', 'TagController@inactiveTag');
	Route::get('tag/{id}/activetag', 'TagController@activeTag');
	/****************/
	/*****Categories*****/
	Route::get('category', 'CategoryController@getlist');
	Route::get('category/data', array('as' => 'admin.category.data', 'uses' => 'CategoryController@getData'));
	Route::get('category/create', array('as' => 'admin.category.create', 'uses' => 'CategoryController@create'));
	Route::get('category/edit/{id}', array('as' => 'admin.category.edit', 'uses' => 'CategoryController@edit'));
	Route::post('category/adddata', array('as' => 'admin.category.add', 'uses' => 'CategoryController@addData'));
	Route::post('category/{id}/update', 'CategoryController@updateData');
	Route::get('category/{id}/delete', array('as' => 'admin.category.delete', 'uses' => 'CategoryController@deleteData'));
	Route::get('category/{id}/inactivedata', 'CategoryController@inactiveData');
	Route::get('category/{id}/activedata', 'CategoryController@activeData');
	/****************/
	/*****Sub Categories*****/
	Route::get('subcategory', 'CategoryController@getsublist');
	Route::get('subcategory/{cat_id}/create', array('as' => 'admin.subcategory.create', 'uses' => 'CategoryController@subcreate'));
	Route::get('subcategory/edit/{id}', array('as' => 'admin.subcategory.edit', 'uses' => 'CategoryController@subedit'));
	Route::get('subcategorylist/{cat_id}', 'CategoryController@getcategoryList');
	Route::get('subcategoryitem/{subcat_id}', 'CategoryController@getsubcategoryItem');
	Route::get('subcategory/subdata/{cat_id}', array('as' => 'admin.subcategory.data', 'uses' => 'CategoryController@getsubData'));
	Route::post('subcategory/addsubdata', array('as' => 'admin.subcategory.add', 'uses' => 'CategoryController@addsubData'));
	Route::post('subcategory/{id}/update', 'CategoryController@updatesubData');
	Route::get('subcategory/{id}/delete', array('as' => 'admin.subcategory.delete', 'uses' => 'CategoryController@deletesubData'));
	Route::get('subcategory/{id}/inactivesubdata', 'CategoryController@inactivesubData');
	Route::get('subcategory/{id}/activesubdata', 'CategoryController@activesubData');
	/****************/
	/*****Category Classics*****/
	Route::get('classic', 'CatClassicController@getsublist');
	Route::get('classiclist/{cat_id}/{subcat_id}', 'CatClassicController@getClassicList');
	Route::get('classicitem/{classic_id}', 'CatClassicController@getClassicItem');
	Route::get('classic/subdata/{cat_id}/{subcat_id}', array('as' => 'admin.classic.data', 'uses' => 'CatClassicController@getsubData'));
	Route::post('classic/addsubdata', array('as' => 'admin.classic.add', 'uses' => 'CatClassicController@addsubData'));
	Route::post('classic/{id}/update', 'CatClassicController@updatesubData');
	Route::get('classic/{id}/delete', array('as' => 'admin.classic.delete', 'uses' => 'CatClassicController@deletesubData'));
	Route::get('classic/{id}/inactivesubdata', 'CatClassicController@inactivesubData');
	Route::get('classic/{id}/activesubdata', 'CatClassicController@activesubData');
	/****************/
	/*****Category Prices*****/
	Route::get('price', 'CatPriceController@getsublist');
	Route::get('pricelist/{cat_id}/{subcat_id}', 'CatPriceController@getPriceList');
	Route::get('priceitem/{classic_id}', 'CatPriceController@getPriceItem');
	Route::get('price/subdata/{cat_id}/{subcat_id}', array('as' => 'admin.price.data', 'uses' => 'CatPriceController@getsubData'));
	Route::post('price/addsubdata', array('as' => 'admin.price.add', 'uses' => 'CatPriceController@addsubData'));
	Route::post('price/{id}/update', 'CatPriceController@updatesubData');
	Route::get('price/{id}/delete', array('as' => 'admin.price.delete', 'uses' => 'CatPriceController@deletesubData'));
	Route::get('price/{id}/inactivesubdata', 'CatPriceController@inactivesubData');
	Route::get('price/{id}/activesubdata', 'CatPriceController@activesubData');
	/****************/
	/*****VIPs*****/
	Route::get('vip', 'VIPController@getlist');
	Route::get('vip/data', array('as' => 'admin.vip.data', 'uses' => 'VIPController@getData'));
	Route::post('vip/adddata', array('as' => 'admin.vip.add', 'uses' => 'VIPController@addData'));
	Route::post('vip/{id}/update', 'VIPController@updateData');
	Route::get('vip/{id}/delete', array('as' => 'admin.vip.delete', 'uses' => 'VIPController@deleteData'));
	Route::get('vip/{id}/inactivedata', 'VIPController@inactiveData');
	Route::get('vip/{id}/activedata', 'VIPController@activeData');
	/****************/
	/*****Subscribe Prices*****/
	Route::get('subprice', 'SubPriceController@getlist');
	Route::get('subprice/data', array('as' => 'admin.subprice.data', 'uses' => 'SubPriceController@getData'));
	Route::post('subprice/adddata', array('as' => 'admin.subprice.add', 'uses' => 'SubPriceController@addData'));
	Route::post('subprice/{id}/update', 'SubPriceController@updateData');
	Route::get('subprice/{id}/delete', array('as' => 'admin.subprice.delete', 'uses' => 'SubPriceController@deleteData'));
	Route::get('subprice/{id}/inactivedata', 'SubPriceController@inactiveData');
	Route::get('subprice/{id}/activedata', 'SubPriceController@activeData');
	/****************/
	/*****Business*****/
	Route::get('business', 'BusinessController@getlist');
	Route::get('business/data', array('as' => 'admin.business.data', 'uses' => 'BusinessController@getData'));
	Route::post('business/adddata', array('as' => 'admin.business.add', 'uses' => 'BusinessController@addData'));
	Route::post('business/{id}/update', 'BusinessController@updateData');
	Route::get('business/{id}/delete', array('as' => 'admin.business.delete', 'uses' => 'BusinessController@deleteData'));
	Route::get('business/{id}/inactivedata', 'BusinessController@inactiveData');
	Route::get('business/{id}/activedata', 'BusinessController@activeData');
	/****************/
	/*****Advertises*****/
	Route::get('advertise/{type}', 'AdvertiseController@getlist');
	Route::get('advertise/data/{type}', array('as' => 'admin.advertise.data', 'uses' => 'AdvertiseController@getData'));
	Route::get('advertise/create/{type}', array('as' => 'admin.advertise.add', 'uses' => 'AdvertiseController@create'));
	Route::post('advertise/adddata', array('as' => 'admin.advertise.adddata', 'uses' => 'AdvertiseController@addData'));
	Route::post('advertise/{id}/update', 'AdvertiseController@updateData');
	Route::get('advertise/{id}/edit', 'AdvertiseController@editData');
	Route::get('advertise/{id}/show', 'AdvertiseController@updateShow');
	Route::get('advertise/{id}/delete', array('as' => 'admin.advertise.delete', 'uses' => 'AdvertiseController@deleteData'));
	Route::get('advertise/{id}/inactivedata', 'AdvertiseController@inactiveData');
	Route::get('advertise/{id}/activedata', 'AdvertiseController@activeData');
	/****************/
	/*****Third party*****/
	Route::get('thirdparty', 'ThirdPartyController@getlist');
	Route::post('thirdparty/{id}/update', 'ThirdPartyController@updateData');
	Route::get('thirdparty/{id}/edit', 'ThirdPartyController@editData');
	Route::get('thirdparty/{id}/show', 'ThirdPartyController@updateShow');
	/****************/
	/*****Filter*****/
	Route::any('filter/{id}', 'FilterController@getList');
	Route::any('activities', 'FilterController@getActivityList');
	Route::post('thirdparty/{id}/update', 'ThirdPartyController@updateData');
	Route::get('thirdparty/{id}/edit', 'ThirdPartyController@editData');
	Route::get('thirdparty/{id}/show', 'ThirdPartyController@updateShow');
	/****************/
	/*****news*****/
	Route::get('news', 'NewsController@getNewslist');
	Route::get('news/data', array('as' => 'admin.news.data', 'uses' => 'NewsController@getData'));
	Route::get('news/users/data/{user_id}', array('as' => 'admin.users.news.data', 'uses' => 'NewsController@getUserNewsData'));
	Route::get('news/{id}/show', array('as' => 'admin.news.show', 'uses' => 'NewsController@showData'));
	Route::get('news/{id}/delete', array('as' => 'admin.news.delete', 'uses' => 'NewsController@deleteData'));
	Route::get('news/{id}/inactivedata', 'NewsController@inactiveData');
	Route::get('news/{id}/activedata', 'NewsController@activeData');
	/****************/
	/*****comments*****/
	Route::get('comments/{news_id}', 'NewsController@getCommentslist');
	Route::get('comments/data/{news_id}', array('as' => 'admin.comments.data', 'uses' => 'NewsController@getCommentsData'));
	Route::get('comments/users/data/{user_id}', array('as' => 'admin.users.comments.data', 'uses' => 'NewsController@getUsersCommentsData'));
	Route::get('comments/{id}/delete', array('as' => 'admin.comments.delete', 'uses' => 'NewsController@deleteCommentsData'));
	/****************/
	/*****news*****/
	Route::get('usermaster', 'UserMasterController@getlist');
	Route::get('usermaster/data', array('as' => 'admin.usermaster.data', 'uses' => 'UserMasterController@getData'));
	Route::get('usermaster/{id}/delete', array('as' => 'admin.usermaster.delete', 'uses' => 'UserMasterController@deleteData'));
	Route::get('usermaster/{id}/inactivedata', 'UserMasterController@inactiveData');
	Route::get('usermaster/{id}/activedata', 'UserMasterController@activeData');
	/****************/
	/*****book contacts*****/
	Route::get('book/users/data/{user_id}', array('as' => 'admin.users.book.data', 'uses' => 'NewsController@getUsersBookData'));
	Route::get('book/{id}/delete', array('as' => 'admin.book.delete', 'uses' => 'NewsController@deleteBookData'));
	/****************/
	/*****chat contacts*****/
	Route::get('chat/users/data/{user_id}', array('as' => 'admin.users.chat.data', 'uses' => 'NewsController@getUsersChatData'));
	Route::get('chat/{id}/delete', array('as' => 'admin.chat.delete', 'uses' => 'NewsController@deleteChatData'));
	/****************/
	/*****site notifications*****/
	Route::get('notification', 'NotificationController@getlist');
	Route::get('notification/data', array('as' => 'admin.notification.data', 'uses' => 'NotificationController@getData'));
	Route::get('notification/create', array('as' => 'admin.notification.add', 'uses' => 'NotificationController@create'));
	Route::post('notification/adddata', array('as' => 'admin.notification.adddata', 'uses' => 'NotificationController@addData'));
	Route::post('notification/{id}/update', 'NotificationController@updateData');
	Route::get('notification/{id}/edit', 'NotificationController@editData');
	Route::get('notification/{id}/show', 'NotificationController@updateShow');
	Route::get('notification/{id}/delete', array('as' => 'admin.notification.delete', 'uses' => 'NotificationController@deleteData'));
	Route::get('notification/{id}/inactivedata', 'NotificationController@inactiveData');
	Route::get('notification/{id}/activedata', 'NotificationController@activeData');
	/****************/
	/*****category groups*****/
	Route::get('catgroups', 'ChatGroupController@getcatlist');
	Route::get('catgroups/data', array('as' => 'admin.catgroups.data', 'uses' => 'ChatGroupController@getCatData'));
	Route::get('catgroups/{id}/delete', array('as' => 'admin.catgroups.delete', 'uses' => 'ChatGroupController@deleteCatData'));
	Route::get('catgroups/{id}/inactivedata', 'ChatGroupController@inactiveCatData');
	Route::get('catgroups/{id}/activedata', 'ChatGroupController@activeCatData');
	/****************/
	/*****chat groups*****/
	Route::get('chatgroups', 'ChatGroupController@getchatlist');
	Route::get('chatgroups/data', array('as' => 'admin.chatgroups.data', 'uses' => 'ChatGroupController@getChatData'));
	Route::get('chatgroups/{id}/delete', array('as' => 'admin.chatgroups.delete', 'uses' => 'ChatGroupController@deleteChatData'));
	Route::get('chatgroups/{id}/inactivedata', 'ChatGroupController@inactiveChatData');
	Route::get('chatgroups/{id}/activedata', 'ChatGroupController@activeChatData');
	/****************/


	/*******reports***************/
	Route::group(array('prefix' => 'reports'), function () {
		Route::get('charts', function () {
			return View::make('admin/reports/charts');
		});
		Route::get('piecharts', function () {
			return View::make('admin/reports/piecharts');
		});
		Route::get('charts_animation', function () {
			return View::make('admin/reports/charts_animation');
		});
	});

	# Remaining pages will be called from below controller method
	# in real world scenario, you may be required to define all routes manually
	Route::get('{name?}', 'JoshController@showView');

});

Route::get('/', function () {
	return Redirect::to('/admin');
});

Route::get('login', array('as' => 'login','uses' => 'FrontEndController@getLogin'));
Route::post('login','FrontEndController@postLogin');
Route::get('register', array('as' => 'register','uses' => 'FrontEndController@getRegister'));
Route::post('register','FrontEndController@postRegister');
Route::get('activate/{userId}/{activationCode}',array('as' =>'activate','uses'=>'FrontEndController@getActivate'));
Route::get('forgot-password',array('as' => 'forgot-password','uses' => 'FrontEndController@getForgotPassword'));
Route::post('forgot-password','FrontEndController@postForgotPassword');
# Forgot Password Confirmation
Route::get('forgot-password/{userId}/{passwordResetCode}', array('as' => 'forgot-password-confirm', 'uses' => 'FrontEndController@getForgotPasswordConfirm'));
Route::post('forgot-password/{userId}/{passwordResetCode}', 'FrontEndController@postForgotPasswordConfirm');
# My account display and update details
Route::group(array('middleware' => 'SentinelUser'), function () {
	Route::get('my-account', array('as' => 'my-account', 'uses' => 'FrontEndController@myAccount'));
	Route::put('my-account', 'FrontEndController@update');
});
Route::get('logout', array('as' => 'logout','uses' => 'FrontEndController@getLogout'));

# My account display and update details
/*Route::group(array('middleware' => 'SentinelUser'), function () {
	Route::get('my-account', array('as' => 'my-account', 'uses' => 'FrontEndController@myAccount'));
    Route::put('my-account', 'FrontEndController@update');
});*/
# End of frontend views

