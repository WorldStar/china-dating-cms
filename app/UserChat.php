<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class UserChat extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'date_user_chats';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];

}