<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class PointType extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'date_point_types';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];

}