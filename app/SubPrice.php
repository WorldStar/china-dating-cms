<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class SubPrice extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'date_subscribe_prices';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];

}