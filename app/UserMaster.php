<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class UserMaster extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'date_user_masters';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];

}