<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class CatPrice extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'date_cat_prices';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];

}