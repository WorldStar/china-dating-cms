<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class CatClassic extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'date_cat_classics';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];

}